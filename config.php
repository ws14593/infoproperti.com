<?php

date_default_timezone_set(date_default_timezone_get());

define("INSTALL_DONE", true);
define("SHOW_CONFIG_READ_ERROR", false);

define("SITE_SUBFOLDER", 'ip-15022016/');
define("SITE_PATH", 'G:/xampp/htdocs/');
define("SITE_PHYSICAL_PATH", SITE_PATH . SITE_SUBFOLDER );
//define("SITE_SERVER", 'http://202.158.138.131/'); original
#2nd method mod for https
define("SITE_SERVER", 'http://'.$_SERVER['HTTP_HOST'].'/');
#end of https#
//define("SITE_SERVER", 'http://infoproperti.com/'); edited to allow https
define("COOKIE_SITE_SERVER", 'infoproperti.com');

define("SITE_VIRTUAL_PATH", SITE_SERVER . SITE_SUBFOLDER );

define("DB_HOSTNAME", 'localhost');
define("DB_USERNAME", 'root');
//define("DB_PASSWORD", '2015mydbaIP'); //'ipDBA2014');
define("DB_PASSWORD", '');
define("DB_DATABASE", 'ipdb3');
define("DB_PREFIX", 'pg_');
define("DB_DRIVER", "mysql");

define("UPLOAD_DIR", "uploads/");
define("DEFAULT_DIR", "default/");
define("DATASOURCE_ICONS_DIR", "datasource_icons/");

define("FRONTEND_PATH", SITE_PHYSICAL_PATH . UPLOAD_DIR);
define("FRONTEND_URL", SITE_VIRTUAL_PATH . UPLOAD_DIR);

define("GENERATE_BACKTRACE", false);
define("USE_PROFILING", false);
define("DISPLAY_ERRORS", false);
define("ADD_LANG_MODE", false);
define("DEMO_MODE", false);

/**
 * Set to true, if you use .htaccess rule to remove $config['index_page'] file 
 * from the site URLs
 */
define("HIDE_INDEX_PAGE", true);
