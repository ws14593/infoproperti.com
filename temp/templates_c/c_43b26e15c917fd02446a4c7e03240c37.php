<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-09 17:51:50 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_export_menu'), $this);?>
<div class="actions">
	&nbsp;
</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first"><?php echo l('field_driver_name', 'export', '', 'text', array()); ?></th>
	<th class="w50"><?php echo l('field_driver_status', 'export', '', 'text', array()); ?></th>
	<th class="w50"><?php echo l('field_driver_link', 'export', '', 'text', array()); ?></th>
	<th class="w70">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['drivers']) and count((array)$this->_vars['drivers'])): foreach ((array)$this->_vars['drivers'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td class="center">
		<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/activate_driver/<?php echo $this->_vars['item']['gid']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_driver', 'export', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_driver', 'export', '', 'button', array()); ?>">
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/activate_driver/<?php echo $this->_vars['item']['gid']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_driver', 'export', '', 'button', array()); ?>" title="<?php echo l('link_activate_driver', 'export', '', 'button', array()); ?>"></a>
		<?php endif; ?>
	</td>
	<td class="center">
		<a href="<?php echo $this->_vars['item']['link']; ?>
" target="_blank">
			<?php if ($this->_vars['item']['need_regkey']): ?>
				<?php echo l('driver_registration', 'export', '', 'text', array()); ?>
			<?php else: ?>
				<?php echo l('driver_info', 'export', '', 'text', array()); ?>
			<?php endif; ?>
		</a>
	</td>
	<td class="icons">
		<?php if ($this->_vars['item']['editable']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/driver_fields/<?php echo $this->_vars['item']['gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-list.png" width="16" height="16" border="0" alt="<?php echo l('link_driver_fields', 'export', '', 'button', array()); ?>" title="<?php echo l('link_driver_fields', 'export', '', 'button', array()); ?>"></a>
		<?php if ($this->_run_modifier($this->_vars['item']['settings'], 'count', 'PHP', 1)): ?><a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit_driver/<?php echo $this->_vars['item']['gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_driver', 'export', '', 'button', array()); ?>" title="<?php echo l('link_edit_driver', 'export', '', 'button', array()); ?>"></a><?php endif; ?>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="5" class="center"><?php echo l('no_drivers', 'export', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
