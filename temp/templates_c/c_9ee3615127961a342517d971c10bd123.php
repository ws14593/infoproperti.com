<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-05-06 09:30:56 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_subscription_edit', 'subscriptions', '', 'text', array()); ?></div>

		<div class="row zebra">
            <div class="h"><?php echo l('field_subscription_name', 'subscriptions', '', 'text', array()); ?>:&nbsp;* </div>
            <div class="v">
				<?php $this->assign('lang_gid', $this->_vars['data']['lang_gid']); ?>
				<?php $this->assign('name_i', $this->_vars['data']['name_i']); ?>
				<input type="text" value="<?php if ($this->_vars['validate_lang_var']):  echo $this->_vars['validate_lang_var'][$this->_vars['cur_lang']];  else:  echo l($this->_vars['data']['name_i'], 'subscriptions', $this->_vars['cur_lang'], 'text', array());  endif; ?>" name="langs[<?php echo $this->_vars['cur_lang']; ?>
]">
				<?php if ($this->_vars['languages_count'] > 1): ?>
                    &nbsp;&nbsp;<a href="#" onclick="showLangs('name_langs'); return false;"><?php echo l('others_languages', 'subscriptions', '', 'text', array()); ?></a><br>
                    <div id="name_langs" class="hide p-top2">
						<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']):  if ($this->_vars['lang_id'] != $this->_vars['cur_lang']): ?>
								<input type="text" value="<?php if ($this->_vars['validate_lang']):  echo $this->_vars['validate_lang'][$this->_vars['lang_id']];  else:  echo l($this->_vars['data']['name_i'], 'subscriptions', $this->_vars['lang_id'], 'text', array());  endif; ?>" name="langs[<?php echo $this->_vars['lang_id']; ?>
]">&nbsp;|&nbsp;<?php echo $this->_vars['item']['name']; ?>
<br>
						<?php endif;  endforeach; endif; ?>
                    </div>
				<?php endif; ?>
            </div>
		</div>

		<div class="row">
            <div class="h"><?php echo l('field_subscribe_type', 'subscriptions', '', 'text', array()); ?>:&nbsp;* </div>
            <div class="v">
				<select name="subscribe_type">
                    <option value="auto" <?php if ($this->_vars['data']['subscribe_type'] == 'auto'): ?>selected<?php endif; ?>><?php echo l('field_subscribe_type_auto', 'subscriptions', '', 'text', array()); ?></option>
                    <option value="user" <?php if ($this->_vars['data']['subscribe_type'] == 'user'): ?>selected<?php endif; ?>><?php echo l('field_subscribe_type_user', 'subscriptions', '', 'text', array()); ?></option>
				</select>
            </div>
		</div>
		
		<div class="row">
			<div class="h"><?php echo l('field_subscriber_type', 'subscriptions', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="subscriber_type">
					<option value="all"><?php echo l('field_subscriber_type_all', 'subscriptions', '', 'text', array()); ?></option>
					<?php if (is_array($this->_vars['subscriber_types']) and count((array)$this->_vars['subscriber_types'])): foreach ((array)$this->_vars['subscriber_types'] as $this->_vars['t_gid']): ?>
					<option value="<?php echo $this->_vars['t_gid']; ?>
"<?php if ($this->_vars['data']['subscriber_type'] == $this->_vars['t_gid']): ?> selected="selected"<?php endif; ?>><?php echo l($this->_vars['t_gid'], 'users', '', 'text', array()); ?></option>
					<?php endforeach; endif; ?>
				</select>
			</div>
		</div>

		<div class="row zebra">
            <div class="h"><?php echo l('field_id_template', 'subscriptions', '', 'text', array()); ?>:&nbsp;* </div>
            <div class="v">
				<select name="id_template">
			<?php if (is_array($this->_vars['templates']) and count((array)$this->_vars['templates'])): foreach ((array)$this->_vars['templates'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['data']['id_template'] == $this->_vars['item']['id']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?>
		</select>
	</div>
</div>

<div class="row">
	<div class="h"><?php echo l('field_id_content_type', 'subscriptions', '', 'text', array()); ?>:&nbsp;* </div>
	<div class="v">
		<select name="id_content_type">
		<?php if (is_array($this->_vars['content_types']) and count((array)$this->_vars['content_types'])): foreach ((array)$this->_vars['content_types'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['data']['id_content_type'] == $this->_vars['item']['id']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?>
		</select>
	</div>
</div>
<?php echo tpl_function_js(array('file' => 'jquery-ui.custom.min.js'), $this);?>
<LINK href='<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css' media='screen'>

<script type='text/javascript'><?php echo '
   $(function(){
	   $( "#datepicker1, #datepicker2" ).datepicker({dateFormat :\'yy-mm-dd\'});
   });
'; ?>
</script>
<div class="row zebra">
	<div class="h"><?php echo l('field_scheduler', 'subscriptions', '', 'text', array()); ?>:&nbsp;* </div>
	<div class="v">
		<p><input type="radio" name="scheduler_type" value="1" <?php if ($this->_vars['data']['scheduler']['type'] == 1): ?>checked<?php endif; ?>><?php echo l('manual', 'subscriptions', '', 'text', array()); ?></p>
		<p>
			<input type="radio" name="scheduler_type" value="2" <?php if ($this->_vars['data']['scheduler']['type'] == 2): ?>checked<?php endif; ?>><?php echo l('in_time', 'subscriptions', '', 'text', array()); ?>
			<input type='text' value='<?php if ($this->_vars['data']['scheduler']['type'] == 2):  echo $this->_vars['data']['scheduler']['date'];  endif; ?>' name="scheduler_date2" id="datepicker1" maxlength="10" class="short">
			<select name="scheduler_hours2" class="short">
			<?php if (is_array($this->_vars['hours']) and count((array)$this->_vars['hours'])): foreach ((array)$this->_vars['hours'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']; ?>
" <?php if ($this->_vars['data']['scheduler']['hours'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 2): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
			</select>
			<select name="scheduler_minutes2" class="short">
			<?php if (is_array($this->_vars['minutes']) and count((array)$this->_vars['minutes'])): foreach ((array)$this->_vars['minutes'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']; ?>
" <?php if ($this->_vars['data']['scheduler']['minutes'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 2): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
			</select>
		</p>
		<p>
			<input type="radio" name="scheduler_type" value="3" <?php if ($this->_vars['data']['scheduler']['type'] == 3): ?>checked<?php endif; ?>><?php echo l('every_time', 'subscriptions', '', 'text', array()); ?>
			<select name="scheduler_period" class="middle">
				<option value="day" <?php if ($this->_vars['data']['scheduler']['period'] == 'day'): ?>selected<?php endif; ?>><?php echo l('day', 'subscriptions', '', 'text', array()); ?> </option>
				<option value="week" <?php if ($this->_vars['data']['scheduler']['period'] == 'week'): ?>selected<?php endif; ?>><?php echo l('week', 'subscriptions', '', 'text', array()); ?> </option>
				<option value="month" <?php if ($this->_vars['data']['scheduler']['period'] == 'month'): ?>selected<?php endif; ?>><?php echo l('month', 'subscriptions', '', 'text', array()); ?> </option>
			</select>
			<?php echo l('since', 'subscriptions', '', 'text', array()); ?>
			<input type='text' value='<?php if ($this->_vars['data']['scheduler']['type'] == 3):  echo $this->_vars['data']['scheduler']['date'];  endif; ?>' name="scheduler_date3" id="datepicker2" maxlength="10" class="short">
			<select name="scheduler_hours3" class="short">
			<?php if (is_array($this->_vars['hours']) and count((array)$this->_vars['hours'])): foreach ((array)$this->_vars['hours'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']; ?>
" <?php if ($this->_vars['data']['scheduler']['hours'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 3): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
			</select>
			<select name="scheduler_minutes3" class="short">
			<?php if (is_array($this->_vars['minutes']) and count((array)$this->_vars['minutes'])): foreach ((array)$this->_vars['minutes'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']; ?>
" <?php if ($this->_vars['data']['scheduler']['minutes'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 3): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
			</select>
		</p>
	</div>
</div>

<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/subscriptions/index"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</div>
</form>
<script>
	<?php echo '
function showLangs(divId){
	$(\'#\'+divId).slideToggle();
}

function openTab(id, object){
	$(\'#edit_divs > div.tab\').hide();
	$(\'#\'+id).show();
	$(\'#edit_tabs > li\').removeClass(\'active\');
	$(object).addClass(\'active\');
}

	'; ?>

</script>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
