<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-23 23:35:03 KRAT */ ?>

<?php if (! $this->_vars['hide_poll']): ?>
<div id="poll_block_<?php echo $this->_vars['poll_data']['id']; ?>
" class="poll_block">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_vars['site_root']; ?>
application/modules/polls/views/default/css/style-<?php echo $this->_vars['_LANG']['rtl']; ?>
.css" />
	<?php echo tpl_function_js(array('module' => polls,'file' => 'polls.js'), $this);?>
	<h2><?php if ($this->_vars['language']):  echo $this->_vars['poll_data']['question'][$this->_vars['language']];  else:  echo $this->_vars['poll_data']['question'][$this->_vars['cur_lang']];  endif; ?></h2>
	<div class="poll">
		<?php echo $this->_vars['poll_block']; ?>

	</div>
	<script type="text/javascript">
		<?php echo '
			$(function() {
				new Polls({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					poll_id: \'';  echo $this->_vars['poll_data']['id'];  echo '\'
				});
			});
		'; ?>

	</script>
</div>
<?php endif; ?>
