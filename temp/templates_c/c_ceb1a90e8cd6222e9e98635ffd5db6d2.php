<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-09 17:31:45 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_seo_menu'), $this);?>
<div class="actions">&nbsp;</div>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_robots_txt_editing', 'seo', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_robots_file', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><textarea name="content" style="height: 170px"><?php echo $this->_vars['content']; ?>
</textarea></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save_robots" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/seo/robots"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_site_xml_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_sitemap_txt_editing', 'seo', '', 'text', array()); ?></div>
		<?php if ($this->_vars['sitemap_data']['mtime']): ?>
		<div class="row zebra">
			<div class="h"><?php echo l('field_last_sitemap_generating', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_run_modifier($this->_vars['sitemap_data']['mtime'], 'date_format', 'plugin', 1, $this->_vars['date_format']); ?>
</div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="h"><?php echo l('field_frequency', 'seo', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="changefreq"><?php if (is_array($this->_vars['frequency_lang']['option']) and count((array)$this->_vars['frequency_lang']['option'])): foreach ((array)$this->_vars['frequency_lang']['option'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_vars['key']; ?>
"><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?></select>
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_last_modified', 'seo', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
			<input type="radio" name="lastmod" value="0" id="lastmod0"> <label for="lastmod0"><?php echo l('field_last_modified_0', 'seo', '', 'text', array()); ?></label><br>
			<input type="radio" name="lastmod" value="1" id="lastmod1" checked> <label for="lastmod1"><?php echo l('field_last_modified_1', 'seo', '', 'text', array()); ?></label><br>
			<input type="radio" name="lastmod" value="2" id="lastmod2"> <label for="lastmod2"><?php echo l('field_last_modified_2', 'seo', '', 'text', array()); ?></label><br>
			<input type="text" name="lastmod_date" value="<?php echo $this->_vars['sitemap_data']['current_date']; ?>
">
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_priority', 'seo', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="priority">
				<option value="0"><?php echo l('field_priority_none', 'seo', '', 'text', array()); ?></option>
				<option value="1"><?php echo l('field_priority_auto', 'seo', '', 'text', array()); ?></option>
				</select>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save_sitexml" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/seo/robots"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
