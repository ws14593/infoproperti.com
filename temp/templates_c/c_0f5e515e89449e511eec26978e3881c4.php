<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:13:48 KRAT */ ?>

<?php if ($this->_vars['news']): ?>
<div id="news-n-fb" class="clearfix">
	<div id="news-section">
		<h2 class="head-title"><?php echo l('header_latest_added_news', 'news', '', 'text', array()); ?></h2>
		<!--<div class="latest_added_news_block">-->
				<div class="latestnews clearfix">
					<?php if (is_array($this->_vars['news']) and count((array)$this->_vars['news'])): foreach ((array)$this->_vars['news'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<div class="news-entry clearfix">
						<?php if ($this->_vars['item']['img']): ?><div class="image"><a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['small']; ?>
" align="left" /></a></div>
						<div class="body"><?php endif; ?>
							<b><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</b>
							<?php echo $this->_run_modifier($this->_vars['item']['annotation'], 'truncate', 'plugin', 1, 180); ?>
<br />
							<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_view_more', 'news', '', 'text', array()); ?></a>
						<?php if ($this->_vars['item']['img']): ?>
						</div>
						<?php endif; ?>		
					</div>
				<?php endforeach; endif; ?>
				</div>
		<!-- </div>-->
	</div>

	<!--<p><a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'index'), $this);?>"><?php echo l('link_read_more', 'news', '', 'text', array()); ?></a></p>-->
</div>

<?php endif; ?>