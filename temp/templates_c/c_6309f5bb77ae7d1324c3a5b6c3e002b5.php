<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-23 22:57:53 KRAT */ ?>

<div class="tabs tab-size-15 noPrint">
	<ul id="user_sections">
		<?php if (is_array($this->_vars['display_sections']) and count((array)$this->_vars['display_sections'])): foreach ((array)$this->_vars['display_sections'] as $this->_vars['sgid'] => $this->_vars['item']): ?>
		<?php $this->assign('sheadline', 'filter_section_'.$this->_vars['sgid']); ?>
		<?php if ($this->_vars['item']): ?><li id="m_<?php echo $this->_vars['sgid']; ?>
" sgid="<?php echo $this->_vars['sgid']; ?>
" class="<?php if ($this->_vars['section_gid'] == $this->_vars['sgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
users/view/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l($this->_vars['sheadline'], 'users', '', 'text', array()); ?> <?php if ($this->_vars['sgid'] == 'reviews'): ?>(<?php echo $this->_vars['user']['review_count']; ?>
)<?php endif; ?></a></li><?php endif; ?>
		<?php endforeach; endif; ?>
	</ul>
</div>
