<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-02-27 14:32:52 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<form method="post" action="" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_page_change', 'content', '', 'text', array());  else:  echo l('admin_header_page_add', 'content', '', 'text', array());  endif; ?></div>
		<?php if ($this->_vars['data']['id']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_view_link', 'content', '', 'text', array()); ?>: </div>
			<div class="v"><a href="<?php echo $this->_vars['site_url']; ?>
content/view/<?php echo $this->_vars['data']['gid']; ?>
"><?php echo $this->_vars['site_url']; ?>
content/view/<?php echo $this->_vars['data']['gid']; ?>
</a>&nbsp;</div>
		</div>
		<?php endif; ?>
		<div class="row zebra">
			<div class="h"><?php echo l('field_lang', 'content', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['languages'][$this->_vars['current_lang']]['name']; ?>
</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_gid', 'content', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="gid"></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_title', 'content', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['title']; ?>
" name="title" class="long"></div>
		</div>
		<div class="row content">
			<?php echo $this->_vars['data']['content_fck']; ?>

		</div>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/content/index/<?php echo $this->_vars['current_lang']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
