<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-09 12:42:39 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_listings_menu'), $this);?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit_wish_list"><?php echo l('link_add_wish_list', 'listings', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<form id="wish_lists_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="w150"><?php echo l('field_wish_list_name', 'listings', '', 'text', array()); ?></th>
	
	<th class="w150"><a href="<?php echo $this->_vars['sort_links']['date_created']; ?>
"<?php if ($this->_vars['order'] == 'date_created'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_date_created', 'listings', '', 'text', array()); ?></a></th>
	<th class="w70">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['wish_lists']) and count((array)$this->_vars['wish_lists'])): foreach ((array)$this->_vars['wish_lists'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>				
	<td><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</td>
	
	<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
	<td class="icons">
		<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/activate_wish_list/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_wish_list', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_wish_list', 'listings', '', 'button', array()); ?>"></a>
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/activate_wish_list/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_wish_list', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_activate_wish_list', 'listings', '', 'button', array()); ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/wish_list_content/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-settings.png" width="16" height="16" border="0" alt="<?php echo l('link_wish_list_content', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_wish_list_content', 'listings', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit_wish_list/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_wish_list', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_edit_wish_list', 'listings', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/delete_wish_list/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_wish_list', 'listings', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_wish_list', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_delete_wish_list', 'listings', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="4" class="center"><?php echo l('no_wish_lists', 'listings', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php echo tpl_function_js(array('file' => 'easyTooltip.min.js'), $this);?>
<script><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/listings/wish_lists<?php echo '";
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(".tooltip").each(function(){
		$(this).easyTooltip({
			useElement: \'span_\'+$(this).attr(\'id\')
		});
	});
});
function reload_this_page(){
	$(\'form[name="save_form"]\').submit();
}
function reload_this_page(value){
	var link = reload_link + \'/\' + value + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
