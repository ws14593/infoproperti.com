<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-09-10 16:36:04 KRAT */ ?>

	<form id="reviews_form_<?php echo $this->_vars['rand']; ?>
" action="" method="POST">
	<div class="r">
		<?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'type_gid' => $this->_vars['type']['gid'],'template' => 'extended','show_label' => 'true'), $this);?>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_reviews_message', 'reviews', '', 'text', array()); ?>&nbsp;*:</div>
		<div class="v"><textarea name="data[message]" rows="5" cols="23"></textarea></div>
	</div>
	<div class="r">
		<input type="checkbox" name="agree" value="1" id="review_agree" /> <label for="review_agree"><?php echo l('field_reviews_agree', 'reviews', '', 'text', array()); ?></label>
	</div>
	<div class="r"><input type="submit" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" id="close_btn" /></div>
	<input type="hidden" name="type_gid" value="<?php echo $this->_vars['type']['gid']; ?>
">
	<input type="hidden" name="object_id" value="<?php echo $this->_vars['object_id']; ?>
">
	<input type="hidden" name="responder_id" value="<?php echo $this->_vars['responder_id']; ?>
">
	<?php if ($this->_vars['is_review_owner']): ?><input type="hidden" name="is_owner" value="1"><?php endif; ?>
	</form>
