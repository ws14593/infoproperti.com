<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-09 17:51:47 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'editable|ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_js(array('file' => 'admin-multilevel-sorter.js'), $this);?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit_driver_field/<?php echo $this->_vars['driver_data']['gid']; ?>
"><?php echo l('btn_driver_field_create', 'export', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first v150"><?php echo l('field_custom_name', 'export', '', 'text', array()); ?></th>
	<th class="w150"><?php echo l('field_custom_type', 'export', '', 'text', array()); ?></th>
	<th class="w70">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['driver_data']['elements']) and count((array)$this->_vars['driver_data']['elements'])): foreach ((array)$this->_vars['driver_data']['elements'] as $this->_vars['key'] => $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['type'], 'truncate', 'plugin', 1, 30); ?>
</td>
	<td class="icons">
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit_driver_field/<?php echo $this->_vars['driver_data']['gid']; ?>
/<?php echo $this->_vars['key']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_custom_field_edit', 'export', '', 'button', array()); ?>" title="<?php echo l('link_custom_field_edit', 'export', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/delete_driver_field/<?php echo $this->_vars['driver_data']['gid']; ?>
/<?php echo $this->_vars['key']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_custom_field_delete', 'export', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_custom_field_delete', 'export', '', 'button', array()); ?>" title="<?php echo l('link_custom_field_delete', 'export', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="3" class="center"><?php echo l('no_driver_fields', 'export', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script><?php echo '
	var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/export/edit_driver/<?php echo $this->_vars['driver_data']['gid']; ?>
/<?php echo '";
	var filter = \'';  echo $this->_vars['filter'];  echo '\';
	var order = \'';  echo $this->_vars['order'];  echo '\';
	var loading_content;
	var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
	$(function(){
		$(\'#grouping_all\').bind(\'click\', function(){
			var checked = $(this).is(\':checked\');
			if(checked){
				$(\'input.grouping\').attr(\'checked\', \'checked\');
			}else{
				$(\'input.grouping\').removeAttr(\'checked\');
			}
		});
		
		$(\'#grouping_all\').bind(\'click\', function(){
			var checked = $(this).is(\':checked\');
			if(checked){
				$(\'input[type=checkbox].grouping\').attr(\'checked\', \'checked\');
			}else{
				$(\'input[type=checkbox].grouping\').removeAttr(\'checked\');
			}
		});
	
		$(\'#all\').bind(\'click\', function(){
			if(!$(\'input[type=checkbox].grouping\').is(\':checked\')) return false; 
			if(this.id == \'delete_all\' && !confirm(\'';  echo l('note_custom_field_delete_all', 'export', '', 'js', array());  echo '\')) return false;
			$(\'#alerts_form\').attr(\'action\', $(this).find(\'a\').attr(\'href\')).submit();		
			return false;
		});
	});
	function reload_this_page(value){
		var link = reload_link + filter + \'/\' + value + \'/\' + order + \'/\' + order_direction;
		location.href=link;
	}
'; ?>
</script>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
