<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-15 17:49:17 KRAT */ ?>

<div class="load_content_controller">
	<h1><?php if ($this->_vars['data']['id']):  echo l('header_edit_photo', 'listings', '', 'text', array());  else:  echo l('header_add_photo', 'listings', '', 'text', array());  endif; ?></h1>
	<div class="inside">
	<form action="" method="post" enctype="multipart/form-data">
	<div class="edit-form">
		<div class="row">
			<div class="h"><?php echo l('field_photo', 'listings', '', 'text', array()); ?>:</div>
			<div class="v"><input type="file" name="photo_file" id="photo_file"><br><i><?php echo $this->_vars['upload_config']['requirements_str']; ?>
</i><?php if ($this->_vars['data']['media']['thumbs']['60_60']): ?><br><img src="<?php echo $this->_vars['data']['media']['thumbs']['60_60']; ?>
" /><?php endif; ?></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_photo_comment', 'listings', '', 'text', array()); ?>: </div>
			<div class="v"><textarea name="comment" id="photo_comment"><?php echo $this->_run_modifier($this->_vars['data']['comment'], 'escape', 'plugin', 1); ?>
</textarea></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="button" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" onclick="javascript: gUpload.upload_photo(<?php if ($this->_vars['data']['id']):  echo $this->_vars['data']['id'];  else: ?>0<?php endif; ?>);"></div></div>
	<a class="cancel" href="#" onclick="javascript: gUpload.close_open_form(); return false;"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
	</form>
	</div>
</div>
