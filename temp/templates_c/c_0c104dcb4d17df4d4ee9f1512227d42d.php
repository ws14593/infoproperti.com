<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-02-17 10:47:41 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_payment_system_change', 'payments', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_system_name', 'payments', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['data']['name']; ?>
</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_system_use', 'payments', '', 'text', array()); ?>: </div>
			<div class="v"><?php if ($this->_vars['data']['in_use']):  echo l('system_in_use', 'payments', '', 'text', array());  else:  echo l('system_not_in_use', 'payments', '', 'text', array());  endif; ?></div>
		</div>
		<?php if (is_array($this->_vars['data']['map']) and count((array)$this->_vars['data']['map'])): foreach ((array)$this->_vars['data']['map'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo $this->_vars['item']['name']; ?>
: </div>
			<div class="v">
								<?php switch($this->_vars['item']['type']): case 'text':  ?>
				<input type="text" name="map[<?php echo $this->_vars['key']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item']['size'] == 'small'): ?>class="short"<?php elseif ($this->_vars['item']['size'] == 'big'): ?>class="long"<?php endif; ?>>
				<?php break; case 'textarea':  ?>
				<textarea name="map[<?php echo $this->_vars['key']; ?>
]" rows="10" cols="80" <?php if ($this->_vars['item']['size'] == 'small'): ?>class="short"<?php elseif ($this->_vars['item']['size'] == 'big'): ?>class="long"<?php endif; ?>><?php echo $this->_vars['item']['value']; ?>
</textarea>
				<?php break; endswitch; ?>
			</div>
		</div>
		<?php endforeach; endif; ?>
		<div class="row">
			<div class="h"><?php echo l('field_info_data', 'payments', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if (is_array($this->_vars['langs']) and count((array)$this->_vars['langs'])): foreach ((array)$this->_vars['langs'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
				<?php $this->assign('name', 'info_data_'.$this->_vars['lang_id']); ?>
				<?php if ($this->_vars['lang_id'] == $this->_vars['current_lang_id']): ?>
				<textarea name="info[<?php echo $this->_vars['name']; ?>
]" rows="10" cols="80" lang-editor="value" lang-editor-type="data-name" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
"><?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['name']], 'escape', 'plugin', 1); ?>
</textarea>
				<?php else: ?>
				<input type="hidden" name="info[<?php echo $this->_vars['name']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['name']], 'escape', 'plugin', 1); ?>
" lang-editor="value" lang-editor-type="data-name" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
" />
				<?php endif; ?>
				<?php endforeach; endif; ?>
				<a href="#" lang-editor="button" lang-editor-type="data-name"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16"></a>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/payments/systems"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start,'textarea' => 1), $this);?>
<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
