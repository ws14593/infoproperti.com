<?php

/*
 * Template Lite plugin converted from Smarty
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     date_format
 * Purpose:  format datestamps via strftime
 * Input:    string: input date string
 *           format: strftime format for output
 *           default_date: default date if $string is empty
 * -------------------------------------------------------------
 */

function tpl_modifier_date_format($string, $format="%b %e, %Y", $default_date=null, $default='')
{
	$CI = &get_instance();
	$CI->load->helper('date_format');
	return tpl_date_format($string, $format, $default_date, $default);
}
