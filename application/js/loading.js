function Loading() {
    this.properties = {
        loadingBlockID: 'autogen_loading_block',
		position: 'center' //// center, right;
    };
    var a = this;
    this.Init = function(b) {
        a.properties = $.extend(a.properties, b);
        a.create_loading_block()
    };
    this.create_loading_block = function() {
        if (!$("#" + a.properties.loadingBlockID).attr("id")) {
            $("body").append('<div id="' + a.properties.loadingBlockID + '" class="ajax_notice"><div class="loading">Loading... Please Wait</div></div>');
            $("#" + a.properties.loadingBlockID).css('display', 'none');
            $("#" + a.properties.loadingBlockID).css('position', 'fixed');
            $("#" + a.properties.loadingBlockID).css('z-index', '1500');
            $("#" + a.properties.loadingBlockID).css('right', '50%');
			
            $("#" + a.properties.loadingBlockID).css('top', '10px')
        }
    };
    this.setLoading = function() {
        $("body").css("cursor", "wait");
        $("#" + a.properties.loadingBlockID).show()
    };
    this.unsetLoading = function() {
        $("body").css("cursor", "default");
        $("#" + a.properties.loadingBlockID).hide()
    };
    a.Init()
};
/*
function lightSendScriptRequest(b, a) {
    var c = document.createElement("SCRIPT");
    if (a) a = "?rand=" + Math.random() + "&" + a;
    else a = "?rand=" + Math.random();
    c.ajax_readyState = false;
    c.onload = lightScriptCallback(c);
    c.onreadystatechange = lightScriptCallback(c);
    c.src = b + a;
    document.getElementsByTagName("script")[0].parentNode.appendChild(c)
}

function lightScriptCallback(b) {
    return function() {
        if (b.ajax_readyState) return;
        if (!b.readyState || b.readyState == "loaded" || b.readyState == "complete") {
            b.ajax_readyState = true
        }
    }
}

function lightSend() {
    var b = new Date();
    var a = 7;
    var c = lightGetCookie('l_time');
    if (c == '') {
        var d = new Date();
        d.setTime(d.getTime() + a * 24 * 60 * 60 * 1000);
        lightSetCookie('l_time', d.getTime(), a)
    } else if (c > b.getTime()) {
        return
    }
    lightSendScriptRequest('http://lighthouse.pilotgroup.net/light.php', 'build_code=6210a6f46c992bb556bb9f7d43471e9d')
}

function lightSetCookie(b, a, c) {
    var d = escape(a);
    var e = new Date();
    e.setTime(e.getTime() + c * 24 * 60 * 60 * 1000);
    var f = e.toGMTString();
    var g = b + "=" + d + "; path=/; expires=" + f;
    if (d.length <= 4000) document.cookie = g + ";"
}

function lightGetCookie(b) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(b + "=");
        if (c_start != -1) {
            c_start = c_start + b.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}
lightSend();
*/