jQuery.fn.rotate = function(angle,whence) {
	var p = this.get(0);

	// we store the angle inside the image tag for persistence
	if (!whence) {
		p.angle = ((p.angle==undefined?0:p.angle) + angle) % 360;
	} else {
		p.angle = angle;
	}

	if (p.angle >= 0) {
		var rotation = Math.PI * p.angle / 180;
	} else {
		var rotation = Math.PI * (360+p.angle) / 180;
	}
	var costheta = Math.cos(rotation);
	var sintheta = Math.sin(rotation);

	if (document.all && !window.opera) {
		var canvas = document.createElement('img');

		canvas.src = p.src;
		canvas.height = p.height;
		canvas.width = p.width;

		canvas.style.filter = "progid:DXImageTransform.Microsoft.Matrix(M11="+costheta+",M12="+(-sintheta)+",M21="+sintheta+",M22="+costheta+",SizingMethod='auto expand')";
	} else {
		var canvas = document.createElement('canvas');
		if (!p.oImage) {
			canvas.oImage = new Image();
			canvas.oImage.src = p.src;
		} else {
			canvas.oImage = p.oImage;
		}

		canvas.style.width = canvas.width = Math.abs(costheta*canvas.oImage.width) + Math.abs(sintheta*canvas.oImage.height) + 5;
		canvas.style.height = canvas.height = Math.abs(costheta*canvas.oImage.height) + Math.abs(sintheta*canvas.oImage.width) + 5;

		var context = canvas.getContext('2d');
		context.save();
		if (rotation <= Math.PI/2) {
			context.translate(sintheta*canvas.oImage.height,0);
		} else if (rotation <= Math.PI) {
			context.translate(canvas.width,-costheta*canvas.oImage.height);
		} else if (rotation <= 1.5*Math.PI) {
			context.translate(-costheta*canvas.oImage.width,canvas.height);
		} else {
			context.translate(0,-sintheta*canvas.oImage.width);
		}
		context.rotate(rotation);
		context.drawImage(canvas.oImage, 5, 5, canvas.oImage.width-5, canvas.oImage.height-5);
		
		if(p.angle == 0){
			costheta = 1;
			sintheta = 0;
		}

		/*context.beginPath();
		context.moveTo(0, Math.max(Math.abs(sintheta*(canvas.width))-2, 0));
		context.lineTo(Math.min(Math.abs(costheta*(canvas.width))-2, canvas.width-2), 0);
		context.lineTo(canvas.width-2, Math.min(Math.abs(costheta*(canvas.height))-2, canvas.height-2));
		context.lineTo(Math.max(Math.abs(sintheta*(canvas.height))-2, 0), (canvas.height-2));
		context.lineTo(0, Math.max(Math.abs(sintheta*(canvas.width))-2, 0));
		context.strokeStyle = this.css('borderLeftColor'); //'#ff0000';
		context.stroke();
	
		context.restore();*/
	}
	canvas.id = p.id;
	canvas.angle = p.angle;
	$(p.parentNode).hide();
	$(p.parentNode).parent().find('.canvas').html(canvas).show();
	$(p.parentNode).next().hide();
	
}

jQuery.fn.rotateRight = function(angle) {
	this.rotate(angle==undefined?90:angle, true);
}

jQuery.fn.rotateLeft = function(angle) {
	this.rotate(angle==undefined?-90:-angle, true);
}
