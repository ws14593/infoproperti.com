<?php

$install_lang["users_contact_service"]["contact_count"] = "Count(contacts)";
$install_lang["users_contact_service"]["service_period"] = "Service activity period(days)";

$install_lang["users_post_service"]["post_count"] = "Count(publications)";
$install_lang["users_post_service"]["period"] = "Publication period(days)";

$install_lang["users_combined_service"]["combined_count"] = "Count";
$install_lang["users_combined_service"]["period"] = "Publication period(days)";
$install_lang["users_combined_service"]["service_period"] = "Service activity period(days)";
$install_lang["users_combined_service"]["contact_count"] = "Count(contacts)";
$install_lang["users_combined_service"]["post_count"] = "Count(publications)";

