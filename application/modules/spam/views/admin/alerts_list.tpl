{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_spam_menu'}

<div class="actions">
	&nbsp;
</div>
{if $spam_types_count}
<div class="menu-level3">
	<ul>
		{foreach item=item from=$spam_types}
		{assign var=stat_header value="stat_header_spam_`$item.gid`"}
		{if $filter eq $item.gid}{assign var=form_type value=$item.form_type}{/if}
		<li class="{if $filter eq $item.gid}active{/if}{if !$item.obj_count} hide{/if}"><a href="{$site_url}admin/spam/index/{$item.gid}">{l i=$stat_header gid='spam'} ({$item.obj_count})</a></li>
		{/foreach}		
	</ul>
	&nbsp;
</div>
{/if}

<form id="alerts_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w100"><a href="{$sort_links.poster}"{if $order eq 'poster'} class="{$order_direction|lower}"{/if}>{l i='field_alert_poster' gid='spam'}</a></th>
		<th class="w100">{l i='field_alert_content' gid='spam'}</th>
		{if $form_type eq 'select_text'}
		<th class="w100">{l i='field_spam_reason' gid='spam'}</th>
		{/if}
		<th class="w50"><a href="{$sort_links.date_add}"{if $order eq 'date_add'} class="{$order_direction|lower}"{/if}>{l i='field_alert_date_add' gid='spam'}</a></th>
		<th class="w50">&nbsp;</th>
	</tr>
	{foreach item=item from=$alerts}
		{counter print=false assign=counter}
		{assign var=spam_status_name value='alert_status_'`$item.spam_status`}
		<tr{if $counter is div by 2} class="zebra"{/if}>				
			<td>{if !$item.mark}<b>{/if}{$item.poster.output_name}{if !$item.mark}</b>{/if}</td>
			<td>{if !$item.mark}<b>{/if}{$item.content|truncate:24}{if !$item.mark}</b>{/if}</td>
			{if $form_type eq 'select_text'}
			<td>{if !$item.mark}<b>{/if}{$item.reason}{if !$item.mark}</b>{/if}</td>
			{/if}
			<td class="center">{if !$item.mark}<b>{/if}{$item.date_add|date_format:$page_data.date_format}{if !$item.mark}</b>{/if}</td>
			<td class="icons">	
				<a href="{$site_url}admin/spam/alerts_show/{$item.id}"><img src="{$site_root}{$img_folder}icon-view.png" width="16" height="16" border="0" alt="{l i='link_alerts_show' gid='spam' type='button'}" title="{l i='link_alerts_show' gid='spam' type='button'}"></a>
				<a href="{$site_url}admin/spam/alerts_delete/without_object/{$item.id}" onclick="javascript: if(!confirm('{l i='note_alerts_delete' gid='spam' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_alerts_delete' gid='spam' type='button'}" title="{l i='link_alerts_delete' gid='spam' type='button'}"></a>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="{if $form_type eq 'select_text'}6{else}5{/if}" class="center">{l i='no_alerts' gid='spam'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}

<script>{literal}
var reload_link = "{/literal}{$site_url}admin/spam/index/{literal}";
var filter = '{/literal}{$filter}{literal}';
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input.grouping').attr('checked', 'checked');
		}else{
			$('input.grouping').removeAttr('checked');
		}
	});
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input[type=checkbox].grouping').attr('checked', 'checked');
		}else{
			$('input[type=checkbox].grouping').removeAttr('checked');
		}
	});
	$('#ban_all,#unban_all,#delete_object_all').bind('click', function(){
		if(!$('input[type=checkbox].grouping').is(':checked')) return false; 
		if(this.id == 'delete_object_all' && !confirm('{/literal}{l i='note_alerts_delete_object_all' gid='spam' type='js'}{literal}')) return false;
		if(this.id == 'delete_all' && !confirm('{/literal}{l i='note_alerts_delete_all' gid='spam' type='js'}{literal}')) return false;
		$('#alerts_form').attr('action', $(this).find('a').attr('href')).submit();		
		return false;
	});
});
function reload_this_page(value){
	var link = reload_link + filter + '/' + value + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}
