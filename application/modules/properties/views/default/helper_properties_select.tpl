<select name="{$properties_helper_data.name}" {if $properties_helper_data.id}id="{$properties_helper_data.id}"{/if}>

    {if !$properties_helper_data.id_category}
	<option value="0">{l i='select_empty_option' gid='properties'}</option>

	{foreach item=item from=$properties_helper_data.data}
	    {if $properties_helper_data.cat_select_available}
		<option value="{$item.value}"{if $item.value eq $properties_helper_data.selected}selected{/if}>{$item.name}</option>
		{foreach item=option from=$item.options}<option value="{$option.value}"{if $option.value eq $properties_helper_data.selected}selected{/if}>&nbsp;&nbsp;--&nbsp;&nbsp;{$option.name}</option>{/foreach}
	    {else}
		<optgroup label="{$item.name}">
		{foreach item=option from=$item.options}<option value="{$option.value}"{if $option.value eq $properties_helper_data.selected}selected{/if}>{$option.name}</option>{/foreach}
		</optgroup>
	    {/if}
	{/foreach}
    {else}
	<option value="0">{$properties_helper_data.category_str}</option>

	{foreach item=item from=$properties_helper_data.data}
		{foreach item=option from=$item.options}<option value="{$option.value}" {if $option.value eq $properties_helper_data.selected}selected{/if}>{$option.name}</option>{/foreach}
	{/foreach}
    {/if}

</select>
