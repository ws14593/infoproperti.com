<?php

$install_lang["admin_contact_menu_contact_list_item"] = "Contacts";
$install_lang["admin_contact_menu_contact_list_item_tooltip"] = "";
$install_lang["admin_contact_menu_contact_settings_item"] = "Settings";
$install_lang["admin_contact_menu_contact_settings_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items_contact_menu_item"] = "Guest contacts";
$install_lang["admin_menu_settings_items_feedbacks-items_contact_menu_item_tooltip"] = "Manage guest contacts with listing providers";

