function Contact(optionArr){
	this.properties = {
		siteUrl: '',
		cFormId: 'contact_form',
		urlCheckCode: 'contact/ajax_check_code/',
		urlSendMessage: 'contact/ajax_send_message/',
		errorObj: new Errors,
		emptyUser: 'Empty user',
		emptySender: 'Empty sender',
		emptyPhone: 'Empty phone',
		emptyEmail: 'Empty email',
		emptyMessage: 'Empty message',
		emptyCode: 'Empty code',
		invalidEmail: 'Invalid email',
		invalidCode: 'Invalid code',
	};

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);		
		
		$('#'+_self.properties.cFormId).bind('submit', function(){
			_self.send_message();
			return false;
		});
	}

	this.send_message = function(){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlSendMessage,
			type: 'POST',
			data: $('#'+_self.properties.cFormId).serialize(),
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					$('#'+_self.properties.cFormId).find('textarea[name="message"]').val('');
				}
			}
		});		
		return false;
	}

	_self.Init(optionArr);
}
