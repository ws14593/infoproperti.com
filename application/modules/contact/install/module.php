<?php
$module["module"] = "contact";
$module["install_name"] = "Contact";
$module["install_descr"] = "Contact forms for site guests";
$module["version"] = "1.01";
$module["files"] = array(
	array("file", "read", "application/modules/contact/controllers/admin_contact.php"),
	array("file", "read", "application/modules/contact/controllers/api_contact.php"),
	array("file", "read", "application/modules/contact/controllers/contact.php"),
	array("file", "read", "application/modules/contact/helpers/contact_helper.php"),
	array("file", "read", "application/modules/contact/install/module.php"),
	array("file", "read", "application/modules/contact/install/permissions.php"),
	array("file", "read", "application/modules/contact/install/settings.php"),
	array("file", "read", "application/modules/contact/install/structure_deinstall.sql"),
	array("file", "read", "application/modules/contact/install/structure_install.sql"),
	array("file", "read", "application/modules/contact/js/contact.js"),
	array("file", "read", "application/modules/contact/models/contact_install_model.php"),
	array("file", "read", "application/modules/contact/models/contact_model.php"),
	array("file", "read", "application/modules/contact/views/admin/helper_home_contact_block.tpl"),
	array("file", "read", "application/modules/contact/views/admin/list.tpl"),
	array("file", "read", "application/modules/contact/views/admin/settings.tpl"),
	array("file", "read", "application/modules/contact/views/admin/view.tpl"),
	array("file", "read", "application/modules/contact/views/default/helper_contact_form.tpl"),
	
	array("dir", "read", "application/modules/contact/langs"),
);

$module["dependencies"] = array(
	"start" => array("version"=>"1.01"),
	"menu" => array("version"=>"1.01"),
	"moderation" 	=> array("version"=>"1.01"),
	"notifications" => array("version"=>"1.03"),
	"users" => array("version"=>"3.01"),
);

$module["linked_modules"] = array(
	"install" => array(
		"menu"			=> "install_menu",
		"notifications"	=> "install_notifications",
		"moderation" 	=> "install_moderation",
		"ausers"		=> "install_ausers",
	),
	"deinstall" => array(
		"menu"			=> "deinstall_menu",
		"notifications"	=> "deinstall_notifications",
		"moderation" 	=> "deinstall_moderation",
		"ausers"		=> "install_ausers",
	)
);

