{include file="header.tpl"}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{if $data.id}{l i='admin_header_news_change' gid='news'}{else}{l i='admin_header_news_add' gid='news'}{/if}</div>
		<div class="row">
			<div class="h">{l i='field_name' gid='news'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.name}" name="name" class="long"></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_gid' gid='news'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.gid}" name="gid" class="long"></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_news_lang' gid='news'}: </div>
			<div class="v"><select name="id_lang">{foreach item=item from=$languages}<option value="{$item.id}"{if $item.id eq $data.id_lang} selected{/if}>{$item.name}</option>{/foreach}</select></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_icon' gid='news'}: </div>
			<div class="v">
				<input type="file" name="news_icon">
				{if $data.img}
				<br><img src="{$data.media.img.thumbs.small}"  hspace="2" vspace="2" />
				<br><input type="checkbox" name="news_icon_delete" value="1" id="uichb"><label for="uichb">{l i='field_icon_delete' gid='news'}</label>
				{/if}
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_video' gid='news'}: </div>
			<div class="v">
				<input type="file" name="news_video">
				{if $data.video}
					<br>{l i='field_video_status' gid='news'}: 
					{if $data.video_data.status eq 'end' && $data.video_data.errors}
						<font color="red">{foreach item=item from=$data.video_data.errors}{$item}<br>{/foreach}</font>
					{elseif $data.video_data.status eq 'end'}	<font color="green">{l i='field_video_status_end' gid='news'}</font><br>
					{elseif $data.video_data.status eq 'images'}	<font color="yellow">{l i='field_video_status_images' gid='news'}</font><br>
					{elseif $data.video_data.status eq 'waiting'} <font color="yellow">{l i='field_video_status_waiting' gid='news'}</font><br>
					{elseif $data.video_data.status eq 'start'} <font color="yellow">{l i='field_video_status_start' gid='news'}</font><br>
					{/if}
					{if $data.video_content.thumbs.small}
					<br><img src="{$data.video_content.thumbs.small}"  hspace="2" vspace="2" />
					{/if}
					<br><input type="checkbox" name="news_video_delete" value="1" id="uvchb"><label for="uvchb">{l i='field_video_delete' gid='news'}</label>
				{/if}
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_annotation' gid='news'}: </div>
			<div class="v"><textarea name="annotation">{$data.annotation}</textarea><br><i>{l i='field_annotation_text' gid='news'}</i></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_content' gid='news'}: </div>
			<div class="v">{$data.content_fck}&nbsp;</div>
		</div>
	
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/news">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>

{include file="footer.tpl"}
