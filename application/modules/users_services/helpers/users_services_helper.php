<?php  if(!defined("BASEPATH")) exit("No direct script access allowed");

if(!function_exists("user_featured_services")){	
	/**
	 * Check featured status of user
	 */
	function user_featured_services(){
		$CI = &get_instance();
		$CI->load->model("users/model/users_model", "Users_model", true);		

		$user_type = $CI->session->userdata("user_type");
		$user_id = $CI->session->userdata("user_id");
		$CI->load->model("Services_model");
		//  ...and if the service is activated
		if(!$CI->Services_model->is_service_active($user_type."_featured_services")) {
			return false;
		}
		$CI->load->model("Users_services_model");
		$is_featured = $CI->Users_services_model->is_user_featured($user_id);
		$CI->template_lite->assign("is_user_featured",	$is_featured);
		
		$CI->config->load("date_formats", TRUE);
		$page_data["date_format"] = $CI->config->item("st_format_date_time_literal", "date_formats");
		$CI->template_lite->assign("page_data", $page_data);
		
		return $CI->template_lite->fetch("helper_".$user_type."_featured", "user", "users_services");
	}
}

if(!function_exists("user_show_logo_services")){	
	/**
	 * Check featured status of user
	 */
	function user_show_logo_services(){
		$CI = &get_instance();
		$CI->load->model("Users_model");		

		$user_type = $CI->session->userdata("user_type");
		$user_id = $CI->session->userdata("user_id");
		$CI->load->model("Services_model");
		//  ...and if the service is activated
		if(!$CI->Services_model->is_service_active($user_type."_show_logo_services")) {
			return false;
		}
		$CI->load->model("Users_services_model");
		$is_show_logo = $CI->Users_services_model->is_user_show_logo($user_id);
		$CI->template_lite->assign("is_show_logo",	$is_show_logo);
		
		$CI->config->load("date_formats", TRUE);
		$page_data["date_format"] = $CI->config->item("st_format_date_time_literal", "date_formats");
		$CI->template_lite->assign("page_data", $page_data);
		
		return $CI->template_lite->fetch("helper_".$user_type."_show_logo", "user", "users_services");
	}
}
