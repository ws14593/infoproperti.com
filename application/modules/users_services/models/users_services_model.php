<?php
/**
* Users services model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2012-09-20 09:47:07 +0300 (Пт, 20 сент 2012) $ $Author: abatukhtin $
**/

if (!defined("BASEPATH")) exit("No direct script access allowed");

if (!defined("USER_SERVICES_TABLE")) define("USER_SERVICES_TABLE", DB_PREFIX . "user_services");

class Users_services_model extends Model{
	
	/**
	 * Link to code igniter object
	 * @var object
	 */
	var $CI;
	
	/**
	 * Link to database object
	 */
	var $DB;
	
	/**
	 * Services fields
	 * @var array
	 */
	var $services_fields = array(
		"id",
		"id_user",
		"service_name",
		"service_data",
		"post_count",
		"post_period",
		"contact_count",
		"contact_service_end_date",
		"date_created",
		"date_modified",
		"contact_status",
		"post_status",
	);

	/**
	 * Constructor
	 *
	 * @return users object
	 */
	function Users_services_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return number of services
	 * @param integer $user_id
	 * @param string $count_name
	 * @return integer
	 */
	public function get_count($user_id=0, $count_name){
		$count = 0;
		$services = $this->get_services_list(null, array("id_user" => $user_id, "status" => "1"));
		foreach($services as $id => $value){
			$data = @unserialize($value["service_data"]);
			if(isset($data[$count_name])) $count = $count + $data[$count_name];
		}
		return $count;
	}

	/**
	 * Save service data
	 * @param integer $service_id
	 * @param array $attrs
	 * @return integer
	 */
	public function save_service($service_id=null, $attrs=array()){
		if(isset($attrs["service_data"]) && is_array($attrs["service_data"])){
			$attrs["service_data"] = serialize($attrs["service_data"]);
		}
		if(is_null($service_id)){
			$attrs["date_created"] = $attrs["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(USER_SERVICES_TABLE, $attrs);
			$service_id = $this->DB->insert_id();
		}else{
			$attrs["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->where("id", $service_id);
			$this->DB->update(USER_SERVICES_TABLE, $attrs);
		}
		return $service_id;
	}

	/**
	 * Return services as array
	 * @param array $order_by
	 * @param array $params
	 * @param array $filter_object_ids
	 * @return array
	 */
	public function get_services_list($order_by=null, $params=array(), $filter_object_ids=null){
		$data = array();
		
		$select_attrs = $this->services_fields;

		$this->DB->select(implode(", ", $select_attrs))->from(USER_SERVICES_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field => $value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field => $value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value, null, false);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->services_fields)){
					$this->DB->order_by($field . " " . $dir);
				}
			}
		}

		$results = $this->DB->get()->result_array();

		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[$r["id"]] = $this->format_service($r);
			}
		}
		return $data;
	}

	/**
	 * Format service data
	 * @param array $data
	 * @return array
	 */
	public function format_service($data){
		if(!empty($data["service_data"])){
			$data["service_data"] = @unserialize($data["service_data"]);
		}
		return $data;
	}

	/**
	 * Return service data by id
	 * @param integer $id
	 * @return array
	 */
	public function get_service($id){
		$data = array();
		$this->DB->select(implode(", ", $this->services_fields))->from(USER_SERVICES_TABLE)->where("id", $id);
		$results = $this->DB->get()->result_array();

		if(!empty($results) && is_array($results)){
			$data = $this->format_service($results[0]);
		}
		return $data;
	}

	/**
	 * Acivate service
	 * @param string $service_name
	 * @param integer $user_id
	 * @param float $price
	 * @param array $service_admin_data
	 * @param array $service_user_data
	 */
	private function _service_activate($service_name, $user_id, $price, $service_admin_data, $service_user_data){
		$this->CI->load->model("Users_model");
		
		$service_data = array(
			"id_user" => $user_id,
			"service_name" => $service_name,
		);

		$user_types = $this->CI->Users_model->get_user_types();
		foreach($user_types as $user_type){
			unset($service_admin_data[$user_type]);
		}
		
		$service_data["service_data"] = @serialize($service_admin_data);
		$service_data["contact_count"] = intval($service_admin_data["contact_count"]);
		if($service_admin_data["service_period"]){
			$service_data["contact_service_end_date"] = date("Y-m-d H:i:s", time()+$service_admin_data["service_period"]*24*60*60);
		}
		$service_data["post_count"] = intval($service_admin_data["post_count"]);
		$service_data["post_period"] = intval($service_admin_data["period"]);

		$service_data["contact_status"] = isset($service_admin_data["contact_count"]) ? 1 : 0;
		$service_data["post_status"] = isset($service_admin_data["post_count"]) ? 1 : 0;
		$service_id = $this->save_service(null, $service_data);
		
		// send notification
		$this->config->load("date_formats", TRUE);
		$date_format = $this->config->item("date_format_date_time_literal", "date_formats");
		
		$user = $this->CI->Users_model->get_user_by_id($user_id);
		
		$mail_data = array(
			'user'=>$user['output_name'],
			'name'=>l('service_string_name_'.$service_name, 'users_services', $user['lang_id']), 
		);
		
		$this->CI->load->model("Notifications_model");	
		$this->CI->Notifications_model->send_notification($user['email'], "user_package_enabled", $mail_data);
	}

	/**
	 * Contact service activate
	 * @param integer $user_id
	 * @param float $price
	 * @param array $service_admin_data
	 * @param array $service_user_data
	 */
	public function service_contact_activate($user_id, $price, $service_admin_data, $service_user_data){
		$this->_service_activate("contact", $user_id, $price, $service_admin_data, $service_user_data);
		return;
	}

	/**
	 * Contact service validate
	 * @param integer $user_id
	 * @param array $data
	 * @param array $service_data
	 * @param float $price
	 */
	public function service_contact_validate($user_id, $data, $service_data=array(), $price=""){
		$return = array("errors" => array(), "data" => $data);
		return $return;
	}

	/**
	 * Post service activate
	 * @param integer $user_id
	 * @param float $price
	 * @param array $service_admin_data
	 * @param array $service_user_data
	 */
	public function service_post_activate($user_id, $price, $service_admin_data, $service_user_data){
		$this->_service_activate("post", $user_id, $price, $service_admin_data, $service_user_data);
		return;
	}

	/**
	 * Post service validate
	 * @param integer $user_id
	 * @param array $data
	 * @param array $service_data
	 * @param float $price
	 */
	public function service_post_validate($user_id, $data, $service_data=array(), $price=""){
		$return = array("errors" => array(), "data" => $data);
		return $return;
	}

	/**
	 * Combine service activate
	 * @param integer $user_id
	 * @param float $price
	 * @param array $service_admin_data
	 * @param array $service_user_data
	 */
	public function service_combined_activate($user_id, $price, $service_admin_data, $service_user_data){
		$this->_service_activate("combined", $user_id, $price, $service_admin_data, $service_user_data);
		return;
	}

	/**
	 * Combine service validate
	 * @param integer $user_id
	 * @param array $data
	 * @param array $service_data
	 * @param float $price
	 */
	public function service_combined_validate($user_id, $data, $service_data=array(), $price=""){
		$return = array("errors" => array(), "data" => $data);
		return $return;
	}
	
	/*
	 * Featured
	 */
	
	/**
	 * Validate featured service
	 * @param integer $user_id
	 * @param array $data
	 * @param array $service_data
	 * @param float $price
	 * @return array
	 */
	public function service_featured_validate($user_id, $data, $service_data=array(), $price=""){
		$return = array("errors"=>array(), "data"=>$data);
		return $return;
	}

	/**
	 * Activate featured service
	 * @param integer $user_id
	 * @param float $price
	 * @param array $service_admin_data
	 * @param array $service_user_data
	 */
	public function service_featured_activate($user_id, $price, $service_admin_data, $service_user_data){
		$this->CI->load->model("Users_model");
		
		$id_user = $service_user_data["id_user"];
		$period = $service_admin_data["period"];

		$user = $this->CI->Users_model->get_user_by_id($id_user);
		if(strtotime($user["featured_end_date"]) > 86400){
			$uts = strtotime($user["featured_end_date"]);
		}else{
			$uts = time();
		}

		$data = array("featured_end_date" => date("Y-m-d H:i:s", $uts+$period*60*60*24));
		$this->CI->Users_model->save_user($id_user, $data);
		
		// send notification
		$this->config->load("date_formats", TRUE);
		$date_format = $this->config->item("date_format_date_time_literal", "date_formats");
		
		$mail_data = array(
			'user'=>$user['output_name'],
			'name'=>l('featured_user_services', 'users_services', $user['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model("Notifications_model");	
		$this->CI->Notifications_model->send_notification($user['email'], "user_service_enabled", $mail_data);
	}

	/**
	 * Cron method for featured service
	 */
	public function service_featured_cron(){
		$this->CI->load->model("Users_model");
		$clean = $this->CI->Users_model->clear_end_date("featured_end_date", 'featured_user_services');
		echo "Make clean (user featured): ".$clean."Featured users was removed";
	}
	
	/**
	 * Activate show logo service
	 * @param integer $user_id
	 * @param float $price
	 * @param array $service_admin_data
	 * @param array $service_user_data
	 */
	public function service_show_logo_activate($user_id, $price, $service_admin_data, $service_user_data){
		$this->CI->load->model("Users_model");
		
		$id_user = $service_user_data["id_user"];
		$period = $service_admin_data["period"];

		$user = $this->CI->Users_model->get_user_by_id($id_user);
		if(strtotime($user["show_logo_end_date"]) > 86400){
			$uts = strtotime($user["show_logo_end_date"]);
		}else{
			$uts = time();
		}

		$data = array("show_logo_end_date" => date("Y-m-d H:i:s", $uts+$period*60*60*24));
		$this->CI->Users_model->save_user($id_user, $data);
		
		// send notification
		$this->config->load("date_formats", TRUE);
		$date_format = $this->config->item("date_format_date_time_literal", "date_formats");
		
		$mail_data = array(
			'user'=>$user['output_name'],
			'name'=>l('show_logo_services', 'users_services', $user['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model("Notifications_model");	
		$this->CI->Notifications_model->send_notification($user['email'], "user_service_enabled", $mail_data);
	}
	
	/**
	 * Show logo service validate
	 * @param integer $user_id
	 * @param array $data
	 * @param array $service_data
	 * @param float $price
	 */
	public function service_show_logo_validate($user_id, $data, $service_data=array(), $price=""){
		$return = array("errors" => array(), "data" => $data);
		return $return;
	}
	
	/**
	 * Cron method for show logo service
	 */
	public function service_show_logo_cron(){
		$this->CI->load->model("Users_model");
		$clean = $this->CI->Users_model->clear_end_date("show_logo_end_date", 'show_logo_services');
		echo "Make clean(Users show logo): ".$clean." Users was removed";
	}	

	/**
	 * Update services
	 */
	public function update_services(){
		$this->DB->select("COUNT(*) AS cnt")->from(USER_SERVICES_TABLE)->where("contact_service_end_date <", date("Y-m-d H:i:s"))->where("UNIX_TIMESTAMP(contact_service_end_date) >", "86400")->where("contact_status", "1");
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results) && $results[0]["cnt"] > 0){
			$data["contact_service_end_date"] = "0000-00-00 00:00:00";
			$data["contact_status"] = 0;
			$this->DB->where("contact_service_end_date <", date("Y-m-d H:i:s"));
			$this->DB->where("UNIX_TIMESTAMP(contact_service_end_date) >", "86400");
			$this->DB->where("contact_status", "1");
			$this->DB->update(USER_SERVICES_TABLE, $data);
			$clean = $results[0]["cnt"];

		}
		echo "User services updating: ".$clean." services was deactivated";
	}

	/**
	 * Return available contact
	 * @return array
	 */
	public function available_contact_action($id_user, $id_contact){
		$is_contact = $this->is_available_contact($id_user, $id_contact);
		
		$return["available"] = 0;
		$return["content"] = "";
		$return["content_buy_block"] = false;
		$this->CI->load->model("Users_model");
		$user_type = $this->CI->Users_model->get_user_type_by_id($id_user);
		// если уже есть контакт - то пропускаем
		if($is_contact || $id_user == $id_contact){
			$return["available"] = 1;
		// если контакта нет:
		}else{
			$this->CI->load->model("Services_model");
			$services_params = array();
			$services_params["where_in"]["template_gid"] = array($user_type."_contact_template", $user_type."_combined_template");
			$services_params["where"]["status"] = 1;
			$services_count = $this->CI->Services_model->get_service_count($services_params);
			if($services_count){
				// если сервисы есть, и есть что списать
				// если сервисы есть, но нет с чего списывать
				$return["available"] = 0;
				$return["content_buy_block"] = true;			
			}else{
				// если сервисов нет - то бесплатно
				//$this->add_contact($id_user, $id_contact);
				$return["available"] = 1;
			}
		}
		return $return;
	}

	/**
	 * Check contact is available
	 * @param integer $id_user
	 * @param integer $id_contact
	 * @param array
	 */
	public function is_available_contact($id_user, $id_contact){
		if(!$this->CI->pg_module->is_module_installed("linker")) return false;
		
		static $contacts = null;
		
		if(is_null($contacts)){
			$this->CI->load->model("Linker_model");
			$results = $this->CI->Linker_model->get_links_simple_arr_count('users_contacts', array($id_user));
			if($results){
				$contacts = $results[$id_user];
			}
		}
	
		return isset($contacts[$id_contact]) && $contacts[$id_contact] > 0;
		//return $this->CI->Linker_model->get_links_simple_count("users_contacts", $id_user, $id_contact);
	}

	/**
	 * Check user is featured
	 * @return array
	 */
	public function is_user_featured($user_id){
		$this->CI->load->model("users/models/users_model", "Users_model", true);
		$return = array("is_featured" => 0);
		$featured_end_date = $this->CI->Users_model->is_end_date("featured_end_date", $user_id);
		if ($featured_end_date){
			$return["is_featured"] = 1;
			$return["featured_end_date"] = $featured_end_date;
		}
		return $return;
	}
	
	/**
	 * Check user is showed logo
	 * @return array
	 */
	public function is_user_show_logo($user_id){
		$this->CI->load->model("users/models/users_model", "Users_model", true);
		$return = array("is_show_logo" => 0);
		$show_logo_end_date = $this->CI->Users_model->is_end_date("show_logo_end_date", $user_id);
		if ($show_logo_end_date){
			$return["is_show_logo"] = 1;
			$return["show_logo_end_date"] = $show_logo_end_date;
		}
		return $return;
	}

	/**
	 * Return seo settings
	 * @param string $method
	 * @param integer $lang_id
	 * @return array
	 */
	function get_seo_settings($method="", $lang_id=""){
		if (!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array("my_services");
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return seo settings (internal)
	 * @param string $method
	 * @param integer $lang_id
	 * @return array
	 */
	function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"title" => l('seo_tags_index_title', 'users_services', $lang_id, 'seo'),
				"keyword" => l('seo_tags_index_keyword', 'users_services', $lang_id, 'seo'),
				"description" => l('seo_tags_index_description', 'users_services', $lang_id, 'seo'),
				"templates" => array()
			);
		}elseif($method == "my_services"){
			return array(
				"title" => l('seo_tags_my_services_title', 'users_services', $lang_id, 'seo'),
				"keyword" => l('seo_tags_my_services_keyword', 'users_services', $lang_id, 'seo'),
				"description" => l('seo_tags_my_services_description', 'users_services', $lang_id, 'seo'),
				"templates" => array()
			);
		}
	}
	
	/**
	 * Return urls for site map
	 */
	function get_sitemap_xml_urls(){
		$this->CI->load->helper('seo');
		$return = array();
		return $return;
	}

	/**
     * Return urls for site map
	 */
	function get_sitemap_urls(){
		$this->CI->load->helper("seo");
		$auth = $this->CI->session->userdata("auth_type");
		$user_type = $this->CI->session->userdata("user_type");
		$block = array();
		return $block;
	}
	
	/**
	 * Return available contact
	 * @return array
	 */
	public function is_active_contact_services($user_type=null){
	    $services_params = array();	    
	    if($user_type){
			$services_params["where_in"]["template_gid"] = array($user_type."_contact_template", $user_type."_combined_template");
		}else{
			$contact_template = $this->DB->escape('%_contact_template%');
			$combined_template = $this->DB->escape('%_combined_template%');
			$services_params["where_sql"][]  = "(template_gid LIKE ".$contact_template." OR template_gid LIKE ".$combined_template.")";
	    }
	    $this->CI->load->model("Services_model");
	    $services_params["where"]["status"] = 1;
	    $services_count = $this->CI->Services_model->get_service_count($services_params);
	    if($services_count){
			return true;
	    }else{
			return false;
	    }
	}
	
	/**
	 * Return available show logo services
	 * @return array
	 */
	public function is_active_show_logo_services($user_type=null){
		$services_params = array();
		if($user_type){
			$services_params["where_in"]["template_gid"] = array($user_type."_show_logo_template");
		}else{
			$show_logo_template = $this->DB->escape('%_show_logo_template%');
			$services_params["where_sql"][]  = "template_gid LIKE ".$show_logo_template;
	    }
	    $this->CI->load->model("Services_model");
	    $services_params["where"]["status"] = 1;
	    $services_count = $this->CI->Services_model->get_service_count($services_params);
	    if($services_count){
			return true;
	    }else{
			return false;
	    }
	}
	
	/**
	 * Return available services
	 * @param string user_type type of user
	 * @return array
	 */
	public function get_active_services($user_type){
		$this->CI->load->model("Services_model");
	    $services_params = array();
		$services_params["where_in"]["template_gid"] = array();
		$services_params["where_in"]["template_gid"][] = $user_type."_featured_template";
		$services_params["where_in"]["template_gid"][] = $user_type."_show_logo_template";
	    $services_params["where"]["status"] = 1;
	    $services = $this->CI->Services_model->get_service_list($services_params);
	    return $services;
	}
}
