<?php

$install_lang["actions"] = "Действия";
$install_lang["admin_header_notification_edit"] = "Настройки уведомлений";
$install_lang["admin_header_notifications_list"] = "Настройки уведомлений";
$install_lang["admin_header_settings_edit"] = "Настройки уведомлений";
$install_lang["admin_header_settings_editing"] = "Настройки";
$install_lang["admin_header_settings_testing"] = "Тестирование параметров";
$install_lang["admin_header_template_content"] = "Содержание письма";
$install_lang["admin_header_template_edit"] = "Редактирование шаблона";
$install_lang["admin_header_templates_list"] = "Настройки уведомлений";
$install_lang["delete_pools"] = "Удалить выбранное";
$install_lang["empty_variables"] = "Нет доступных переменных";
$install_lang["error_charset_incorrect"] = "Неверная кодировка";
$install_lang["error_content_type_mandatory_field"] = "Укажите тип контента";
$install_lang["error_empty_notification_name"] = "Укажите название уведомления";
$install_lang["error_from_email_incorrect"] = "Поле From email неверно заполнено или не заполнено";
$install_lang["error_from_name_incorrect"] = "Поле From name неверно заполнено или не заполнено";
$install_lang["error_gid_empty"] = "Отсутствует системное имя";
$install_lang["error_gid_mandatory_field"] = "Требуется системное имя";
$install_lang["error_name_mandatory_field"] = "Требуется название";
$install_lang["error_protocol_incorrect"] = "Неверный протокол отправки почты";
$install_lang["error_send_pool"] = "Уведомление не отослано. Пожалуйста, проверьте настройки";
$install_lang["error_send_pools"] = "Уведомления не отправляются. Пожалуйста, проверьте настройки";
$install_lang["error_template_already_exists"] = "Такой шаблон уже существует";
$install_lang["error_to_email_incorrect"] = "Поле To email неверно заполнено или не заполнено";
$install_lang["error_useragent_incorrect"] = "Отсутствует user agent";
$install_lang["field_available_global_variables"] = "Глобальные переменные";
$install_lang["field_available_variables"] = "Доступные переменные";
$install_lang["field_content"] = "Содержание";
$install_lang["field_content_type"] = "Тип";
$install_lang["field_content_type_html"] = "HTML";
$install_lang["field_content_type_text"] = "Текст";
$install_lang["field_date_add"] = "Дата создания";
$install_lang["field_default_template"] = "Шаблон по умолчанию";
$install_lang["field_mail_charset"] = "Кодировка";
$install_lang["field_mail_from_email"] = "From email";
$install_lang["field_mail_from_name"] = "From name";
$install_lang["field_mail_mailpath"] = "Путь к почтовой программе на сервере";
$install_lang["field_mail_protocol"] = "Протокол отправки почты";
$install_lang["field_mail_smtp_host"] = "SMTP host";
$install_lang["field_mail_smtp_pass"] = "SMTP password";
$install_lang["field_mail_smtp_port"] = "SMTP port";
$install_lang["field_mail_smtp_user"] = "SMTP user";
$install_lang["field_mail_to_email"] = "To email";
$install_lang["field_mail_useragent"] = "User agent";
$install_lang["field_notification_gid"] = "Системное имя";
$install_lang["field_notification_name"] = "Название";
$install_lang["field_send_type"] = "Тип отсылки";
$install_lang["field_send_type_que"] = "Поставить в очередь";
$install_lang["field_send_type_simple"] = "Отсылка сразу после действия";
$install_lang["field_subject"] = "Тема письма";
$install_lang["field_template_gid"] = "Системное имя";
$install_lang["field_template_name"] = "Название";
$install_lang["field_template_vars"] = "Переменные";
$install_lang["field_template_vars_text"] = "Эти переменные заменяются соответствующими значениями. Разделяйте их запятой ','.";
$install_lang["filter_all_templates"] = "Все шаблоны";
$install_lang["filter_html_templates"] = "HTML";
$install_lang["filter_text_templates"] = "Текст";
$install_lang["link_add_notification"] = "Добавить уведомление";
$install_lang["link_add_template"] = "Создать шаблон";
$install_lang["link_delete_notification"] = "Удалить";
$install_lang["link_delete_pool"] = "Удалить";
$install_lang["link_delete_template"] = "Удалить";
$install_lang["link_edit_notification"] = "Редактировать";
$install_lang["link_edit_template"] = "Редактировать";
$install_lang["link_send_pool"] = "Отправить уведомление";
$install_lang["no_notifications"] = "Уведомления отсутствуют";
$install_lang["no_pool"] = "Нет очереди отправки";
$install_lang["no_templates"] = "Шаблоны отсутствуют";
$install_lang["note_delete_notification"] = "Вы уверены, что хотите удалить это уведомление?";
$install_lang["note_delete_pool"] = "Вы уверены, что хотите удалить это сообщение из очереди?";
$install_lang["note_delete_template"] = "Вы уверены, что хотите удалить этот шаблон?";
$install_lang["notification_contact_us_form"] = "Форма контакта";
$install_lang["notification_news"] = "Новости сайта";
$install_lang["notification_user_registration"] = "Регистрация";
$install_lang["notification_users_account_create"] = "Создан профиль";
$install_lang["notification_users_change_email"] = "Смена email адреса";
$install_lang["notification_users_change_password"] = "Смена пароля";
$install_lang["notification_users_fogot_password"] = "Восстановление пароля";
$install_lang["notification_users_registration"] = "Регистрация пользователя";
$install_lang["notification_users_update_account"] = "Деньги добавлены на счет";
$install_lang["others_languages"] = "Другие языки";
$install_lang["refresh_pool"] = "Обновить очередь";
$install_lang["send_attempts"] = "Попытки отправки";
$install_lang["send_pools"] = "Отправить выбранное";
$install_lang["success_add_notification"] = "Уведомление успешно сохранено";
$install_lang["success_add_template"] = "Уведомление успешно добавлено";
$install_lang["success_delete_pool"] = "Уведомление удалено";
$install_lang["success_delete_pools"] = "Уведомления удалены";
$install_lang["success_error_send_pools"] = "%d уведомлений отправлено, %d уведомлений отправлено с ошибкой. Пожалуйста, проверьте настройки";
$install_lang["success_send_pool"] = "Уведомление успешно отправлено";
$install_lang["success_send_pools"] = "Уведомления успешно отправлены";
$install_lang["success_send_test"] = "Тестовое уведомление успешно отправлено";
$install_lang["success_settings_saved"] = "Настройки сохранены";
$install_lang["success_update_notification"] = "Уведомление успешно обновлено";
$install_lang["success_update_template"] = "Шаблон успешно обновлен";
$install_lang["template_settings"] = "Основные параметры";

