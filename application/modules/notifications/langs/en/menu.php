<?php

$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item"] = "Email notifications";
$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item_tooltip"] = "SMTP server, text of notifications, subscriptions";
$install_lang["admin_notifications_menu_nf_items"] = "Alerts";
$install_lang["admin_notifications_menu_nf_items_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_pool_item"] = "Sending queue";
$install_lang["admin_notifications_menu_nf_pool_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_settings_item"] = "Settings";
$install_lang["admin_notifications_menu_nf_settings_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_templates_item"] = "Templates";
$install_lang["admin_notifications_menu_nf_templates_item_tooltip"] = "";

