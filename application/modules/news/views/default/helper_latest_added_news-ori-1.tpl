{if $news}
<div id="news-n-fb" class="clearfix">
	<div id="news-section">
		<h2 class="head-title">{l i='header_latest_added_news' gid='news'}</h2>
		<!--<div class="latest_added_news_block">-->
				<div class="latestnews clearfix">
					<div class="news-entry clearfix">
					{foreach item=item key=key from=$news_headline}
                    	<div style="width:340px; float:left; margin:4px;">
                        	{if $item.img}
                            <div class="image"><img src="{$item.media.img.thumbs.headline}" style=" margin:0 auto;" align="center" /></div>
                            {elseif $item.img eq NULL || $item.img eq ''}
                            <div class="image"><img src="{$base_url}uploads/news-logo/default/logo-headline{$item.image_randomizer}.jpg" style=" margin:0 auto;" align="center" /></div>
                            {/if}
                            <div style="margin-top:10px;">
                                <b>{$item.name|truncate:100}</b><br/>
                                {$item.annotation|truncate:180}<br />
                                <a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
                            </div>
                        </div>
					{/foreach}
                    </div>
					{foreach item=item key=key from=$news}
					<div class="news-entry clearfix">
						{if $item.img}
                        <div class="image">
                        	<a href="{seolink module='news' method='view' data=$item}">
                            <!-- mod -->
                            	<img src="{$item.media.img.thumbs.small}" align="left" />
                            <!-- end of mod -->
                            </a>
                        </div>
                        {else}
                        <div class="image">
                        	<a href="{seolink module='news' method='view' data=$item}">
                            <!-- mod -->
                            	<img src="{$base_url}uploads/news-logo/default/logo-small{$item.image_randomizer}.jpg" align="left" />
                            <!-- end of mod -->
                            </a>
                        </div>                        
                        {/if}
                        <!--
                        {if $item.img}
						<div class="body">
                        {/if}
                        -->
						<div class="body">                        
							<b>{$item.name|truncate:100}</b>
							{$item.annotation|truncate:180}<br />
							<a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
						</div>
					</div>
				{/foreach}
				</div>
		<!-- </div>-->
	</div>

	<!--<p><a href="{seolink module='news' method='index'}">{l i='link_read_more' gid='news'}</a></p>-->
</div>

{/if}