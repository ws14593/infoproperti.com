<?php

$_permissions["admin_export"]["activate"] = 3;
$_permissions["admin_export"]["activate_driver"] = 3;
$_permissions["admin_export"]["ajax_get_module_fields"] = 3;
$_permissions["admin_export"]["create_relation"] = 3;
$_permissions["admin_export"]["delete"] = 3;
$_permissions["admin_export"]["delete_driver_field"] = 3;
$_permissions["admin_export"]["delete_relation"] = 3;
$_permissions["admin_export"]["driver_fields"] = 3;
$_permissions["admin_export"]["drivers"] = 3;
$_permissions["admin_export"]["edit"] = 3;
$_permissions["admin_export"]["edit_driver"] = 3;
$_permissions["admin_export"]["edit_driver_field"] = 3;
$_permissions["admin_export"]["generate"] = 3;
$_permissions["admin_export"]["index"] = 3;
$_permissions["admin_export"]["save_form_data"] = 3;
$_permissions["admin_export"]["save_relations"] = 3;
$_permissions["api_export"]["generate"] = 1;
$_permissions["export"]["index"] = 1;
$_permissions["export"]["advanced"] = 1;
