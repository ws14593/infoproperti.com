<?php 
/**
 * Vast xml export driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_vast_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Constructor
	 * @return Export_vast_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return file name
	 */
	public function get_filename(){
		return "export.xml";
	}

	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<listing>'."\n");
			
			if(isset($row['record_id']) && !empty($row['record_id']))
				fputs($f, '		<record_id>'.$row['record_id'].'</record_id>'."\n");
				
			if(isset($row['title']) && !empty($row['title']))
				fputs($f, '		<title>'.str_replace($this->search, $this->replace, $row['title']).'</title>'."\n");
				
			if(isset($row['url']) && !empty($row['url']))
				fputs($f, '		<url>'.str_replace($this->search, $this->replace, $row['url']).'</url>'."\n");
					
			if(isset($row['category']) && !empty($row['category']))
				fputs($f, '		<category>'.str_replace($this->search, $this->replace, $row['category']).'</category>'."\n");
					
			if(isset($row['subcategory']) && !empty($row['subcategory']))
				fputs($f, '		<subcategory>'.str_replace($this->search, $this->replace, $row['subcategory']).'</subcategory>'."\n");
					
			if(isset($row['image']) && !empty($row['image']))
				fputs($f, '		<image>'.str_replace($this->search, $this->replace, $row['image']).'</image>'."\n");
					
			if(isset($row['address']) && !empty($row['address']))
				fputs($f, '		<address>'.str_replace($this->search, $this->replace, $row['address']).'</address>'."\n");
					
			if(isset($row['city']) && !empty($row['city']))
				fputs($f, '		<city>'.str_replace($this->search, $this->replace, $row['city']).'</city>'."\n");
					
			if(isset($row['state']) && !empty($row['state']))
				fputs($f, '		<state>'.str_replace($this->search, $this->replace, $row['state']).'</state>'."\n");
					
			if(isset($row['zip']) && !empty($row['zip']))
				fputs($f, '		<zip>'.str_replace($this->search, $this->replace, $row['zip']).'</zip>'."\n");
					
			if(isset($row['country']) && !empty($row['country']))
				fputs($f, '		<country>'.str_replace($this->search, $this->replace, $row['country']).'</country>'."\n");
				
			if(isset($row['bedrooms']) && !empty($row['bedrooms']))				
				fputs($f, '		<bedrooms>'.$row['bedrooms'].'</bedrooms>'."\n");
					
			if(isset($row['bathrooms']) && !empty($row['bathrooms']))
				fputs($f, '		<bathrooms>'.$row['bathrooms'].'</bathrooms>'."\n");
					
			if(isset($row['square_footage']) && !empty($row['square_footage']))
				fputs($f, '		<square_footage>'.str_replace($this->search, $this->replace, $row['square_footage']).'</square_footage>'."\n");
					
			if(isset($row['stories']) && !empty($row['stories']))	
				fputs($f, '		<stories>'.str_replace($this->search, $this->replace, $row['stories']).'</stories>'."\n");
					
			if(isset($row['lot_size']) && !empty($row['lot_size']))
				fputs($f, '		<lot_size>'.$row['lot_size'].'</lot_size>'."\n");
					
			if(isset($row['parking_spots']) && !empty($row['parking_spots']))
				fputs($f, '		<parking_spots>'.str_replace($this->search, $this->replace, $row['parking_spots']).'</parking_spots>'."\n");
					
			if(isset($row['year_built']) && !empty($row['year_built']) && strlen($row['year_built']) == 4)
				fputs($f, '		<year_built>'.$row['year_built'].'</year_built>'."\n");
					
			if(isset($row['currency']) && !empty($row['currency']))
				fputs($f, '		<currency>'.str_replace($this->search, $this->replace, $row['currency']).'</currency>');
					
			if(isset($row['price']) && !empty($row['price']))
				fputs($f, '		<price>'.$row['price'].'</price>'."\n");
				
			if(isset($row['amenities']) && !empty($row['amenities']))
				fputs($f, '		<amenities>'.str_replace($this->search, $this->replace, implode(', ', (array)$row['amenities'])).'</amenities>'."\n");
					
			if(isset($row['description']) && !empty($row['description']))
				fputs($f, '		<description>'.str_replace($this->search, $this->replace, $row['description']).'</description>'."\n");
					
			if(isset($row['listing_time']) && !empty($row['listing_time']))
				fputs($f, '		<listing_time>'.date('Y-m-d H:i:s', strtotime($row['listing_time'])).'</listing_time>'."\n");
					
			if(isset($row['expire_time']) && !empty($row['expire_time']))
				fputs($f, '		<expire_time>'.date('Y-m-d H:i:s', strtotime($row['expire_time'])).'</expire_time>'."\n");
			
			fputs($f, '	</listing>'."\n");
		}
		fputs($f, '</listings>'."\n");
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		$return["data"] = $data;	
		
		if(isset($return['data']['image_url']) && !empty($return['data']['image'])) $return['data']['image'] = array_shift(explode('|', $return['data']['image']));
		
		return $return;
	}
}
