<?php 
/**
 * SyndaFeed agents export driver model
 * 
 * @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_synda_agents_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Symbols for replace
	 * @var array
	 */
	protected $license_types = array('r', 'a', 'f');
	
	/**
	 * Password types
	 * @var array
	 */
	protected $password_types = array('mysql', 'plain');
	
	/**
	 * Constructor
	 * @return Export_synda_agents_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return file name
	 */
	public function get_filename(){
		return "agents.xml";
	}

	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		
		fputs($f, '<?xml version="1.0" encoding="UTF-8">'."\n");
		fputs($f, '<Agents>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			
			fputs($f, '	<Agent>'."\n");
			fputs($f, '		<PublisherId>'.$row['PublisherId'].'</PublisherId>');
			fputs($f, '		<AgentId>'.$row['AgentId'].'</AgentId>');
				
			if(isset($row['PersonalTitle']) && !empty($row['PersonalTitle']))
				fputs($f, '		<PersonalTitle><![CDATA['.$row['PersonalTitle'].']]></PersonalTitle>');

			fputs($f, '		<FirstName><![CDATA['.$row['FirstName'].']]></FirstName>');
			fputs($f, '		<LastName><![CDATA['.$row['LastName'].']]></LastName>');
			fputs($f, '		<LicenseType value="'.(isset($row['LicenseType.value']) && in_array($row['LicenseType.value'], $this->license_types) ? $row['LicenseType.value'] : 'r').'">');
			fputs($f, '			<Jurisdiction><![CDATA['.$row['LicenseType.Jurisdiction'].']]></Jurisdiction>');
			fputs($f, '			<LicenseNumber><![CDATA['.$row['LicenseType.LicenseNumber'].']]></LicenseNumber>');
			fputs($f, '		</LicenseType>');
			fputs($f, '		<Email>'.$row['Email'].'</Email>');
			fputs($f, '		<Password'.(isset($row['Password.type']) && in_array($row['Password.type'], $this->password_types) ? ' type="'.$row['Password.type'].'"' : '').'>'.$row['Password'].'</Password>');
				
			if(isset($row['Telephone']) && !empty($row['Telephone']))
				fputs($f, '		<Telephone>'.$row['Telephone'].'</Telephone>');
				
			if(isset($row['Fax']) && !empty($row['Fax']))
				fputs($f, '		<Fax>'.$row['Fax'].'</Fax>');
					
			if(isset($row['PhotoURL']) && !empty($row['PhotoURL']))
				fputs($f, '		<PhotoURL>'.$row['PhotoURL'].'</PhotoURL>');
				
			if(isset($row['BrokerName']) && !empty($row['BrokerName']))
				fputs($f, '		<BrokerName>'.$row['BrokerName'].'</BrokerName>');
				
			if(isset($row['BrokerPhone']) && !empty($row['BrokerPhone']))
				fputs($f, '		<BrokerPhone>'.$row['BrokerPhone'].'</BrokerPhone>');
				
			if(isset($row['BrokerURL']) && !empty($row['BrokerURL']))
				fputs($f, '		<BrokerURL>'.$row['BrokerURL'].'</BrokerURL>');
				
			if(isset($row['BrokerMlsName']) && !empty($row['BrokerMlsName']))
				fputs($f, '		<BrokerMlsName>'.$row['BrokerMlsName'].'</BrokerMlsName>');
				
			if(isset($row['BrokerMlsCode']) && !empty($row['BrokerMlsCode']))
				fputs($f, '		<BrokerMlsCode>'.$row['BrokerMlsCode'].'</BrokerMlsCode>');
			
			fputs($f, '	</Agent>'."\n");
		}
		fputs($f, '</Agents>'."\n");
		
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		$return["data"] = $data;
		if(!isset($data["PublisherId"]) || empty($data["PublisherId"])) $return["errors"][] = "PublisherId";
		if(!isset($data["FirstName"]) || empty($data["FirstName"])) $return["errors"][] = "FirstName";
		if(!isset($data["LastName"]) || empty($data["LastName"])) $return["errors"][] = "LastName";
		return $return;
	}
}
