<input type="hidden" name="{$sb_input}" id="{$sb_id}" value="{$sb_selected}">
<div class="selectBox" id="{$sb_id}_box">
	<div class="label">{$sb_default|default:'&nbsp;'}</div><div class="arrow"></div>
	<div class="data"><ul>{if $sb_default}<li gid="">{$sb_default}</li>{/if}
		{foreach item=item key=key from=$sb_value}
			
			<li gid="{$key}" {if $sb_subvalue}class="group"{/if}>{$item}</li>
			{if $sb_subvalue}
			{foreach item=subitem key=subkey from=$sb_subvalue[$key]}
			<li gid="{$key}_{$subkey}" class="sub">{$subitem}</li>
			{/foreach}
			{/if}
		{/foreach}
	</ul></div>
</div>
