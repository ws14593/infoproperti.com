<?php

class Start_install_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	var $CI;
	
	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "create",
			"name" => "Admin mode - main menu",
			"items" => array(
				"main_items" => array(
					"action" => "create", 
					"link" => "admin/start", 
					"status" => 1, 
					"sorter" => 1,
					"items" => array(
						"admin-home-item" => array("action" => "create", "link" => "admin/start", "status" => 1, "sorter" => 1),
						'moderation-items' => array("action" => "create", 'link' => 'admin/start/menu/moderation-items', 'status' => 1, 'sorter' => 6, 'indicator_gid' => 'new_item_for_moderation'),
						'exp-import-items' => array("action" => "create", 'link' => 'admin/start/menu/exp-import-items', 'status' => 1, 'sorter' => 7),
					),
				),
				"settings_items" => array(
					"action" => "create", 
					"link" => "admin/start", 
					"status" => 1, 
					"sorter" => 2,
					"items" => array(
						'interface-items' => array("action" => "create", 'link' => 'admin/start/menu/interface-items', 'status' => 1, 'sorter' => 1),
						'content_items' => array("action" => "create", 'link' => 'admin/start/menu/content_items', 'status' => 1, 'sorter' => 2),
						'feedbacks-items' => array("action" => "create", 'link' => 'admin/start/menu/feedbacks-items', 'status' => 1, 'sorter' => 3),
						'uploads-items' => array("action" => "create", 'link' => 'admin/start/menu/uploads-items', 'status' => 1, 'sorter' => 4),
						'system-items' => array(
							"action" => "create", 'link' => 'admin/start/menu/system-items', 'status' => 1, 'sorter' => 5,
							"items" => array(
								'system-numerics-item' => array("action" => "create", 'link' => 'admin/start/settings', 'status' => 1, 'sorter' => 10),
							),
						)
					),
				),
				"other_items" => array(
					"action" => "create", 
					"link" => "admin/start", 
					"status" => 1, 
					"sorter" => 3,
					"items" => array(
						"admin-modules-item" => array("action" => "create", "link" => "admin/start/mod_login", "status" => 1, "sorter" => 1),
						'newsletters-items' => array("action" => "create", 'link' => 'admin/start/menu/newsletters-items', 'status' => 1, 'sorter' => 2),
					),
				),
			),			
		),
		
		"private_top_menu" => array(
			"action" => "create",
			"name" => "User mode - Top menu for private persons",
			"items" => array(
				"private-main-overview-item" => array("action" => "create", "link" => "start/homepage", "status" => 1, "sorter" => 1),
			),
		),
		
		"company_top_menu" => array(
			"action" => "create",
			"name" => "User mode - Top menu for agencies",
			"items" => array(
				"company-main-overview-item" => array("action" => "create", "link" => "start/homepage", "status" => 1, "sorter" => 1),
			),
		),
		
		"agent_top_menu" => array(
			"action" => "create",
			"name" => "User mode - Top menu for agencies",
			"items" => array(
				"agent-main-overview-item" => array("action" => "create", "link" => "start/homepage", "status" => 1, "sorter" => 1),
			),
		),
		
		"guest_main_menu" => array(
			"action" => "create",
			"name" => "User mode - main menu for guests",
			"items" => array(
				
			),
		),
		
		"user_footer_menu" => array(
			"action" => "create",
			"name" => "User mode - menu in the footer",
			"items" => array(
			),
		),
	);
	
	/**
	 * Dynamic blocks
	 * @var array
	 */
	private $dynamic_blocks = array(
		array(
			"gid" => "site_stat_block",
			"module" => "start",
			"model" => "Start_model",
			"method" => "_dynamic_block_get_stat_block",
			"params" => array(),
			"views" => array(array("gid"=>"default")),
		),
		array(
			"gid" => "search_form_block",
			"module" => "start",
			"model" => "Start_model",
			"method" => "_dynamic_block_get_search_form",
			"params" => array(),
			"views" => array(array("gid"=>"default")),
		),
	);
	
	/**
	 * Seo configuration
	 */
	private $seo = array(
		array(
			"module_gid" => "start",
			"model_name" => "Start_model",
			"get_settings_method" => "get_seo_settings",
			"get_rewrite_vars_method" => "request_seo_rewrite",
			"get_sitemap_urls_method" => "get_sitemap_xml_urls",
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	public function Start_install_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model("Install_model");
	}

	/**
	 * Check system requirements
	 * @return array
	 */
	public function _validate_requirements(){
		$result = array("data"=>array(), "result" => true);

		//php 5.2
		$good			= phpversion() >= "5.2.0";
		$result["data"][] = array(
			"name" => "PHP version >= 5.2.0 ",
			"value" => $good ? "Yes" : "No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;

		//json
		$good			= extension_loaded("json");
		$result["data"][] = array(
			"name" => "PECL json",
			"value" => $good ? "Yes" : "No",
			"result" => $good,
		);

		$result["result"] = $result["result"] && $good;

		return $result;
	}

	/**
	 * Validate install settings
	 * @return array
	 */
	function _validate_settings_form(){
		$errors = array();
		$data["product_order_key"] = $this->CI->input->post("product_order_key", true);

		if(empty($data["product_order_key"])){
			$errors[] = $this->CI->pg_language->get_string("start", "error_product_key_incorrect");
		}

		$return = array(
			"data" => $data,
			"errors" => $errors,
		);
		return $return;
	}

	/**
	 * Save install settings
	 * @param array $data
	 */
	private function _save_settings_form($data){
		foreach($data as $setting => $value){
			$this->CI->pg_module->set_module_config("start", $setting, $value);
		}
		return;
	}

	function _get_settings_form($submit=false){
		$data = array(
			"product_order_key" => $this->CI->pg_module->get_module_config("start", "product_order_key"),
		);
		if($submit){
			$validate = $this->_validate_settings_form();
			if(!empty($validate["errors"])){
				$this->CI->template_lite->assign("settings_errors", $validate["errors"]);
				$data = $validate["data"];
			}else{
				$this->_save_settings_form($validate["data"]);
				return false;
			}
		}

		$this->CI->template_lite->assign("settings_data", $data);
		$html = $this->CI->template_lite->fetch("install_settings_form", "admin", "start");
		return $html;
	}

	/**
	 * Install menu
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("start", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
	
		$this->CI->load->model('Menu_model');
		
		foreach($this->menu as $gid => $menu_data){
			$menu = $this->CI->Menu_model->get_menu_by_gid($gid);
			if($menu["id"]) $this->CI->Menu_model->delete_menu($menu["id"]);
		}
	}

	/**
	 * Install banners module
	 */
	public function install_banners(){
		///// add banners module
		$this->CI->load->model("Start_model");
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->load->model("banners/models/Banner_place_model");
		
		$this->CI->Banner_group_model->set_module("start", "Start_model", "_banner_available_pages");

		// create banner groups
		$group_attrs = array(
			"date_created" => date("Y-m-d H:i:s"),
			"date_modified" => date("Y-m-d H:i:s"),
			"price" => 1,
			"gid" => "content_groups",
			"name" => "Content pages",
		);
		$group_id = $this->CI->Banner_group_model->create_unique_group($group_attrs);
		$all_places = $this->CI->Banner_place_model->get_all_places();
		if($all_places){
			foreach($all_places as $key => $value){
				$this->CI->Banner_place_model->save_place_group($value["id"], $group_id);
			}
		}

		$group_attrs = array(
			"date_created" => date("Y-m-d H:i:s"),
			"date_modified" => date("Y-m-d H:i:s"),
			"price" => 1,
			"gid" => "users_groups",
			"name" => "Users pages",
		);
		$group_id = $this->CI->Banner_group_model->create_unique_group($group_attrs);
		$all_places = $this->CI->Banner_place_model->get_all_places();
		if($all_places){
			foreach($all_places as $key => $value){
				$this->CI->Banner_place_model->save_place_group($value["id"], $group_id);
			}
		}

		$group_attrs = array(
			"date_created" => date("Y-m-d H:i:s"),
			"date_modified" => date("Y-m-d H:i:s"),
			"price" => 1,
			"gid" => "start_groups",
			"name" => "Start pages",
		);

		$group_id = $this->CI->Banner_group_model->create_unique_group($group_attrs);
		///add pages in group
		$pages = $this->CI->Start_model->_banner_available_pages();
		if($pages){
			foreach($pages as $key => $value){
				$page_attrs = array(
					"group_id" => $group_id,
					"name" => $value["name"],
					"link" => $value["link"],
				);
				$this->CI->Banner_group_model->add_page($page_attrs);
			}
		}

		//add places in group
		$all_places = $this->CI->Banner_place_model->get_all_places();
		if($all_places){
			foreach($all_places as $key => $value){
				$this->CI->Banner_place_model->save_place_group($value["id"], $group_id);
			}
		}
	}
	
	/**
	 * Import banners languages
	 * @param array $langs_ids
	 */
	public function install_banners_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;		
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
	
		$langs_file = $this->CI->Install_model->language_file_read("start", "banners", $langs_ids);
		if(!$langs_file){log_message("info", "Empty banners langs data");return false;}
		
		$this->CI->load->model("banners/models/Banner_group_model");
		
		$banners_groups[] = "banners_group_content_groups";
		$banners_groups[] = "banners_group_listings_groups";
		$banners_groups[] = "banners_group_users_groups";
		$banners_groups[] = "banners_group_start_groups";
		
		$this->CI->Banner_group_model->update_langs($banners_groups, $langs_file, $langs_ids);
		return true;		
	} 
	
	/**
	 * Export banners languages
	 * @param array $langs_ids
	 * @return array
	 */
	public function install_banners_lang_export($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("banners/models/Banner_group_model");
		$banners_groups[] = "banners_group_content_groups";
		$banners_groups[] = "banners_group_listings_groups";
		$banners_groups[] = "banners_group_users_groups";
		$banners_groups[] = "banners_group_start_groups";
		$langs = $this->CI->Banner_group_model->export_langs($banners_groups, $langs_ids);		
		return array("banners" => $langs);
	}

	/**
	 * Uninstall banners
	 */
	public function deinstall_banners(){
		///// delete banners module
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->Banner_group_model->delete_module("start");
	}

	/**
	 * Install links to dynamic blocks
	 */
	public function install_dynamic_blocks(){
		$this->CI->load->model("Dynamic_blocks_model");

		$area_ids = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			
			$validate_data = $this->CI->Dynamic_blocks_model->validate_block(null, $block_data);
			if(!empty($validate_data["errors"])) continue;
			$id_block = $this->CI->Dynamic_blocks_model->save_block(null, $validate_data["data"]);
		
			if(!isset($block_data["area"])) continue;
		
			if(!isset($area_ids[$block_data["area"]["gid"]])){
				$area = $this->CI->Dynamic_blocks_model->get_area_by_gid($block_data["area"]["gid"]);
				$area_ids[$block_data["area"]["gid"]] = $area["id"];
			}

			// index area
			$block_data["area"]["id_area"] = $area_ids[$block_data["area"]["gid"]];
			$block_data["area"]["id_block"] = $id_block;
			$block_data["area"]["params"] = serialize($block_data["area"]["params"]);
	
			$validate_data = $this->CI->Dynamic_blocks_model->validate_area_block($block_data["area"], true);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Dynamic_blocks_model->save_area_block(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Install languages
	 * @param array $langs_ids
	 */
	public function install_dynamic_blocks_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("start", "dynamic_blocks", $langs_ids);
		if(!$langs_file){log_message("info", "Empty dynamic_blocks langs data");return false;}
		
		$this->CI->load->model("Dynamic_blocks_model");
		
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$this->CI->Dynamic_blocks_model->update_langs($data, $langs_file, $langs_ids);
	}
	
	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_dynamic_blocks_lang_export($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Dynamic_blocks_model");
		$data = array();
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		$langs = $this->CI->Dynamic_blocks_model->export_langs($data, $langs_ids);
		return array("dynamic_blocks" => $langs);
	}	

	/**
	 * Unistall dynamic blocks
	 */
	public function deinstall_dynamic_blocks(){
		$this->CI->load->model("Dynamic_blocks_model");
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$this->CI->Dynamic_blocks_model->delete_block_by_gid($block_data["gid"]);
		}
	}

	/**
	 * Install module
	 */
	public function _arbitrary_installing(){
		$this->CI->load->model("Install_model");
		
		///// seo
		foreach((array)$this->seo as $seo_data){
			$this->CI->pg_seo->set_seo_module("start", $seo_data);
		}
	}
	
	/**
	 * Import module languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_install($langs_ids=null){
		/// admin_home_page
		$langs_file = $this->CI->Install_model->language_file_read("start", "admin_home_page", $langs_ids);
		if(!$langs_file){log_message("info", "Empty admin home page langs data"); return false;}

		foreach($langs_file as $gid => $ldata){
			if(!empty($ldata)) 
				$this->CI->pg_language->pages->set_string_langs("admin_home_page", $gid, $ldata, array_keys($ldata));
		}

		$langs_file = $this->CI->Install_model->language_file_read("start", "arbitrary", $langs_ids);
		if(!$langs_file){log_message("info", "Empty arbitrary langs data"); return false;}
		
		$post_data = array(
			"default_title" => 0,
			"title" => $langs_file["seo_tags_title"],
			"default_keyword" => 0,
			"keyword" => $langs_file["seo_tags_keyword"],
			"default_description" => 0,
			"description" => $langs_file["seo_tags_description"],
			"default_header" => 0,
			"header" => $langs_file["seo_tags_header"],
		);
		$this->CI->pg_seo->set_settings("user", "start", "index", $post_data);
		$this->CI->pg_seo->set_settings("admin", "start", "index", $post_data);

	}

	/**
	 * Export module languages
	 * @param array $langs_ids
	 * @return array
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		if(empty($langs_ids)) return false;

		$admin_home_page_return = array();

		/// admin_home_page
		foreach($langs_ids as $lang_id){
			$mod_langs = $this->CI->pg_language->pages->return_module("admin_home_page", $lang_id);
			foreach($mod_langs as $gid => $value){
				$admin_home_page_return[$gid][$lang_id] = $value;
			}
		}

		//// arbitrary
		$settings = $this->CI->pg_seo->get_settings("user", "", "", $langs_ids);
		$arbitrary_return["seo_tags_title"] = $settings["title"];
		$arbitrary_return["seo_tags_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_description"] = $settings["description"];
		$arbitrary_return["seo_tags_header"] = $settings["header"];

		return array("admin_home_page" => $admin_home_page_return, "arbitrary" => $arbitrary_return);
	}

	/**
	 * Uninstall module
	 */
	public function _arbitrary_deinstalling(){
		$this->CI->pg_seo->delete_seo_module("start");
	}
}
