{include file="header.tpl"}
<div class="actions">
	{if $allow_add}
	<ul>
		<li><div class="l"><a href="{$site_url}admin/upload_gallery/edit">{l i='link_add_gallery' gid='upload_gallery'}</a></div></li>
	</ul>
	{/if}
	&nbsp;
</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first">{l i='field_gid' gid='upload_gallery'}</th>
	<th>{l i='field_name' gid='upload_gallery'}</th>
	<th class="w100">{l i='field_max_items' gid='upload_gallery'}</th>
	<th class="w100">{l i='field_use_moderation' gid='upload_gallery'}</th>
	<th class="w150">{l i='field_date_add' gid='upload_gallery'}</th>
	<th class="w50">&nbsp;</th>
</tr>
{foreach item=item from=$types}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td class="first center">{$item.gid}</td>
	<td class="center">{$item.name|truncate:100}</td>
	<td class="center">{if $item.max_items_count eq 0}{l i='items_count_unlimit' gid='upload_gallery'}{else}{$item.max_items_count}{/if}</td>
	<td class="center">{if $item.use_moderation}{l i='moderation_on' gid='upload_gallery'}{else}{l i='moderation_off' gid='upload_gallery'}{/if}</td>
	<td class="center">{$item.date_add|date_format:$page_data.date_format}</td>
	<td class="icons">
		<a href="{$site_url}admin/upload_gallery/edit/{$item.id}"><img src="{$site_url}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_type' gid='upload_gallery' type='button'}" title="{l i='link_edit_type' gid='upload_gallery' type='button'}"></a>
		{if $allow_add}
		<a href="{$site_url}admin/upload_gallery/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_type' gid='upload_gallery' type='js'}')) return false;"><img src="{$site_url}{$img_folder}icon-delete.gif" width="16" height="16" border="0" alt="{l i='link_delete_type' gid='upload_gallery' type='button'}" title="{l i='link_delete_type' gid='upload_gallery' type='button'}"></a>
		{/if}
	</td>
</tr>
{foreachelse}
<tr><td colspan="6" class="center">{l i='no_upload_gallery' gid='upload_gallery'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}
{include file="footer.tpl"}
