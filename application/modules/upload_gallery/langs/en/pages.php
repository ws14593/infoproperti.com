<?php

$install_lang["admin_header_edit"] = "Photo album";
$install_lang["admin_header_list"] = "Photo album";
$install_lang["admin_header_template_edit"] = "Edit gallery type";
$install_lang["error_badwords_comment"] = "Badwords error";
$install_lang["error_gid_empty"] = "Keyword is a required field";
$install_lang["error_max_items_count"] = "You have reached the max number of files";
$install_lang["error_name_empty"] = "Name is a required field";
$install_lang["field_date_add"] = "Date add";
$install_lang["field_gid"] = "Keyword";
$install_lang["field_max_items"] = "Photos max count";
$install_lang["field_max_items_comment"] = "(0 - unlimit)";
$install_lang["field_name"] = "Name";
$install_lang["field_upload_config"] = "Upload config";
$install_lang["field_use_moderation"] = "Moderate";
$install_lang["items_count_unlimit"] = "unlimited";
$install_lang["link_add_gallery"] = "Add gallery type";
$install_lang["link_delete_type"] = "Delete";
$install_lang["link_edit_type"] = "Edit";
$install_lang["moderation_off"] = "Off";
$install_lang["moderation_on"] = "On";
$install_lang["no_upload_gallery"] = "No galleries types yet";
$install_lang["note_delete_type"] = "Are you sure you want to delete the entry?";
$install_lang["success_add_type"] = "Type is successfully added";
$install_lang["success_update_type"] = "Type is successfully updated";

