DROP TABLE IF EXISTS `[prefix]gallery`;
CREATE TABLE IF NOT EXISTS `[prefix]gallery` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `type_id` int(3) NOT NULL,
  `object_id` int(3) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `status` tinyint(3) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `settings` text NULL,
  `sorter` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`,`object_id`,`status`,`sorter`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]gallery_types`;
CREATE TABLE IF NOT EXISTS `[prefix]gallery_types` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `gid` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_add` datetime NOT NULL,
  `gid_upload_config` varchar(50) NOT NULL,
  `max_items_count` int(3) NOT NULL,
  `use_moderation` tinyint(3) NOT NULL,
  `module` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;