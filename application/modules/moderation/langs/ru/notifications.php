<?php 

$install_lang["notification_auser_need_moderate"] = "Новый объект ожидает проверки";
$install_lang["tpl_auser_need_moderate_content"] = "Здравствуйте, администратор!\n\nНовый объект ([type]) ожидает проверки на сайте [domain]. Для просмотра зайдите в панель администратора > Модерация > Загрузки.\n\nС уважением,\n[name_from]";
$install_lang["tpl_auser_need_moderate_subject"] = "[domain] | Новый объект ожидает проверки";
