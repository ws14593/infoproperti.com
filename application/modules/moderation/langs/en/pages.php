<?php

$install_lang["add_badword_hint"] = "one or more words divided by a space";
$install_lang["admin_header_moderation"] = "Moderation";
$install_lang["admin_header_moderation_badwords_managment"] = "Moderation | Badwords";
$install_lang["admin_header_moderation_managment"] = "Moderation | Objects";
$install_lang["admin_header_moderation_settings_managment"] = "Moderation | Settings";
$install_lang["all_objects"] = "All";
$install_lang["approve_object"] = "Approve";
$install_lang["badwords_in_base_exists"] = "The item already exists:";
$install_lang["check_badwords"] = "Check badwords";
$install_lang["decline_object"] = "Decline";
$install_lang["delete_object"] = "Delete object";
$install_lang["delete_word"] = "Remove";
$install_lang["edit_object"] = "Edit object";
$install_lang["error_empty_email"] = "Empty email";
$install_lang["error_invalid_email"] = "Invalid email";
$install_lang["field_admin_moderation_emails"] = "Emails";
$install_lang["field_date_add"] = "Date added";
$install_lang["field_moderation_send_mail"] = "Send notification to email address";
$install_lang["header_add_badword"] = "Add bad words";
$install_lang["header_badword_found"] = "Bad words found";
$install_lang["header_badwords_base"] = "Bad words";
$install_lang["header_check_text"] = "Check text for a bad word";
$install_lang["moder_object"] = "Details";
$install_lang["moder_object_type"] = "Content type";
$install_lang["mtype_0"] = "<b>Disable</b> moderation";
$install_lang["mtype_1"] = "<b>Post</b>moderation (objects are visible immediately after addition until they are declined)";
$install_lang["mtype_2"] = "<b>Pre</b>moderation (objects are visible only after approval)";
$install_lang["no_objects"] = "No objects for moderation";
$install_lang["no_types"] = "No types of objects for moderation";
$install_lang["note_delete_badword"] = "Are you sure you would like to delete this badword?";
$install_lang["note_delete_object"] = "Are you sure you would like to delete this object?";
$install_lang["stat_header_moderation"] = "Uploads awaiting approval";
$install_lang["success_settings_saved"] = "Settings saved successfully";
$install_lang["view_object"] = "View object";

