<?php

$install_lang["admin_menu_main_items_moderation-items_badwords_item"] = "Badwords";
$install_lang["admin_menu_main_items_moderation-items_badwords_item_tooltip"] = "Bad words database";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2"] = "Moderation settings";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2_tooltip"] = "Set up uploads moderation, check for bad words";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item"] = "Uploads";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item_tooltip"] = "Profile images & listing gallery awaiting approval";
$install_lang["admin_moderation_menu_badwords_item"] = "Badwords";
$install_lang["admin_moderation_menu_badwords_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_settings_item"] = "Moderation settings";
$install_lang["admin_moderation_menu_moder_settings_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_setting_item"] = "Moderation settings";
$install_lang["admin_moderation_menu_moder_setting_item_tooltip"] = "";
$install_lang["admin_moderation_menu_object_list_item"] = "Uploads";
$install_lang["admin_moderation_menu_object_list_item_tooltip"] = "";

