<ul>
{foreach key=key item=item from=$menu}
<li {if $item.active}class="active"{/if}>
	<a href="{$item.link}">{$item.value}</a>
	{if $item.sub}
	<div class="sub_menu_block">
		<span>{$item.value}</span>
		<ul>
			{foreach key=key2 item=item2 from=$item.sub}
			<li class="main-menu"><a href="{$item2.link}">{$item2.value} {if $item.indicator}<span class="num">{$item.indicator}</span>{/if}</a></li>
			{/foreach}
		</ul>
	</div>
	{/if}
</li>
{/foreach}
<li id="social-links"><a href="#">Social</a>
	<ul id="social-contents">
		<li><a id="fb" class="socmed-icons" href="https://www.facebook.com/inproperti" target="_blank" title="Join Infoproperti on Facebook">Join Infoproperti on Facebook</a></li>
		<li><a id="twitter" class="socmed-icons" href="https://twitter.com/inproperti" target="_blank" title="Join Infoproperti on Twitter"></a></li>
		<li><a id="gplus" class="socmed-icons" href="https://plus.google.com/u/0/111307853610134749510/about" target="_blank" title="Join Infoproperti on Google Plus"></a></li>
		<li><a id="youtube" class="socmed-icons" href="http://www.youtube.com/channel/UCpY67JNdkJ8xxxeacwXJT4Q" target="_blank" title="Join Infoproperti on Youtube"></a></li>
		<!-- <li><a id="pinterest" class="socmed-icons" href="#" target="_blank" title="Join Infoproperti on Pinterest"></a></li>-->
		<li><a id="linkedin" class="socmed-icons" href="https://id.linkedin.com/in/infoproperti" target="_blank" title="Join Infoproperti on linkedIn"></a></li>
		<li><a id="instagram" class="socmed-icons" href="http://instagram.com/inproperti" target="_blank" title="Join Infoproperti on Instagram"></a></li>
	</ul>
</li>
</ul>
