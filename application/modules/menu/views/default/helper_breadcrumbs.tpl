{if $breadcrumbs}
<div class="breadcrumb">
<a href="{$site_url}{if !$is_guest}start/homepage{/if}">{l i='link_home' gid='menu'}</a>
{foreach item=item from=$breadcrumbs}
&nbsp;>&nbsp;{if $item.url}<a href="{$item.url}">{$item.text}</a>{else}{$item.text}{/if}
{/foreach}
</div>
{/if}
