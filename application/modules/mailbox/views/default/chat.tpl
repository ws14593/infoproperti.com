{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		{js module=mailbox file='mailbox.js'}
		<script type='text/javascript'>{literal}

			var mb;
			$(function(){
				mb = new Mailbox({
					siteUrl: '{/literal}{$site_url}{literal}',
					folderId: '{/literal}{$data.active_folder}{literal}',
					chatId: '{/literal}{$data.active_chat}{literal}',
					viewType: 'chat',
					initialNextBtn: '{/literal}{$data.next_btn}{literal}'
				});
			});

		{/literal}</script>


		<h1>{l i='my_communication' gid='mailbox'}</h1>

		<div class="content-value mailbox">
			<div id="chat_window">
				<p class="header-comment">{l i='header_chat_with' gid='mailbox'} {$chat.user_data.output_name}:</p>
				<div>
					<textarea id="message_area"></textarea><br>
					<input type="button" name="delete_btn" value="{l i='btn_send_message' gid='mailbox' type='button'}" class='btn' id="msg_submit">
				</div>
				<div class="clr"></div>

				<div class="msgs">
					<ul id="message_list">
					{include file="block_messages_item.tpl" module="mailbox"}
					</ul>
					<a class="btn-link{if !$data.next_btn} hide{/if}" href="#" id="next-page">
					<ins class="with-icon i-rarr"></ins>
					{l i='btn_next_messages' gid='mailbox' type='button'}
					</a>

				</div>

			</div>
			<div class="clr"></div>
		</div>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}