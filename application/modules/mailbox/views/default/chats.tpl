{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		{js module=mailbox file='mailbox.js'}
		<script type='text/javascript'>{literal}

			var mb;
			$(function(){
				mb = new Mailbox({
					siteUrl: '{/literal}{$site_url}{literal}',
					folderId: '{/literal}{$data.active_folder}{literal}',
					viewType: 'chats',
					pageData:{
						orderKey: '{/literal}{$data.order_key}{literal}',
						orderDirection: '{/literal}{$data.order_direction}{literal}'
					}
				});
			});

		{/literal}</script>


		<h1>{l i='my_communication' gid='mailbox'}</h1>

		<div class="content-value mailbox">
			<div id="folders" class="fright actions">
				{$folders_block}
			</div>
			<div id="chat_window">{$chats_block}</div>
			<div class="clr"></div>
		</div>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}