<?php


$_permissions["api_mailbox"]["chat"] = "2";
$_permissions["api_mailbox"]["chats"] = "2";
$_permissions["api_mailbox"]["delete_chats"] = "2";
$_permissions["api_mailbox"]["move_chats"] = "2";
$_permissions["api_mailbox"]["folder"] = "2";
$_permissions["api_mailbox"]["folders"] = "2";
$_permissions["api_mailbox"]["save_folder"] = "2";
$_permissions["api_mailbox"]["delete_folders"] = "2";
$_permissions["api_mailbox"]["send_message"] = "2";
$_permissions["api_mailbox"]["get_messages"] = "2";
$_permissions["api_mailbox"]["delete_message"] = "2";

$_permissions["mailbox"]["ajax_delete_chats"] = "2";
$_permissions["mailbox"]["ajax_delete_folder"] = "2";
$_permissions["mailbox"]["ajax_delete_message"] = "2";
$_permissions["mailbox"]["ajax_get_new_messages"] = "2";
$_permissions["mailbox"]["ajax_move_chats"] = "2";
$_permissions["mailbox"]["ajax_save_folder"] = "2";
$_permissions["mailbox"]["ajax_send_message"] = "2";
$_permissions["mailbox"]["ajax_view_chats"] = "2";
$_permissions["mailbox"]["ajax_view_folders"] = "2";
$_permissions["mailbox"]["ajax_view_messages"] = "2";
$_permissions["mailbox"]["chat"] = "2";
$_permissions["mailbox"]["folders"] = "2";
$_permissions["mailbox"]["index"] = "2";
$_permissions["mailbox"]["ajax_open_edit_folder"] = "2";
$_permissions["mailbox"]["ajax_open_move_folder"] = "2";
