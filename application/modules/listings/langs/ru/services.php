<?php

$install_lang["listings_featured_service"] = "Выделенные объявления";
$install_lang["listings_featured_template"] = "Выделенные объявления";
$install_lang["listings_featured_template_id_listing"] = "Объявление";
$install_lang["listings_featured_template_period"] = "Период (в днях)";
$install_lang["listings_highlight_service"] = "Подсветить объявление в поиске";
$install_lang["listings_highlight_template"] = "Подсветить объявление в поиске";
$install_lang["listings_highlight_template_id_listing"] = "Объявление";
$install_lang["listings_highlight_template_period"] = "Период (в днях)";
$install_lang["listings_lift_up_city_service"] = "Поднять объявление в поиске по выбранному городу";
$install_lang["listings_lift_up_city_template"] = "Поднять объявление в поиске по выбранному городу";
$install_lang["listings_lift_up_city_template_id_listing"] = "Объявление";
$install_lang["listings_lift_up_city_template_period"] = "Период (в днях)";
$install_lang["listings_lift_up_country_service"] = "Поднять объявление в поиске по выбранной стране";
$install_lang["listings_lift_up_country_template"] = "Поднять объявление в поиске по выбранной стране";
$install_lang["listings_lift_up_country_template_id_listing"] = "Объявление";
$install_lang["listings_lift_up_country_template_period"] = "Период (в днях)";
$install_lang["listings_lift_up_region_service"] = "Поднять объявление в поиске по выбранному региону";
$install_lang["listings_lift_up_region_template"] = "Поднять объявление в поиске по выбранному региону";
$install_lang["listings_lift_up_region_template_id_listing"] = "Объявление";
$install_lang["listings_lift_up_region_template_period"] = "Период (в днях)";
$install_lang["listings_lift_up_service"] = "Поднять объявление в поиске";
$install_lang["listings_lift_up_template"] = "Поднять объявление в поиске";
$install_lang["listings_lift_up_template_id_listing"] = "Объявление";
$install_lang["listings_lift_up_template_period"] = "Период (в днях)";
$install_lang["listings_slide_show_service"] = "Показывать слайд-шоу с фотографиями объявления";
$install_lang["listings_slide_show_template"] = "Показывать слайд-шоу с фотографиями объявления";
$install_lang["listings_slide_show_template_id_listing"] = "Объявление";
$install_lang["listings_slide_show_template_period"] = "Период (в днях)";

