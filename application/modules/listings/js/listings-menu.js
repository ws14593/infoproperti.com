function listingsMenu(optionArr){
	this.properties = {
		siteUrl: '',
		viewAjaxUrl: 'listings/ajax_get_section/',
		listBlockId: 'listing_block',
		sectionId: 'listing_sections',
		tryDisplayWithoutAjax: true,
		containerPrefix:'content_',
		errorObj: new Errors(),
		currentSection: 'm_overview',
		idListing: 0,
	}
	
	this.loaded = {};

	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
	}
	
	this.init_links = function(){
		
		$('#' + _self.properties.sectionId + ' li').bind('click', function(){
			var id = $(this).attr('id');
			_self.properties.currentSection = id;
			switch(id){
				case 'm_gallery':
				case 'm_virtual_tour':
				case 'm_map':
				case 'm_panorama':
				case 'm_file':
				case 'm_video':
				case 'm_overview':
				case 'm_calendar':
					if(_self.loaded[id]){
						_self.show_tab(id, false);
					}else{
						_self.show_tab(id, true);
					}
				break;
				case 'm_gallery':
				case 'm_virtual_tour':
					$('#'+_self.properties.containerPrefix+id).css({position: 'relative', top: 0, opacity: 1}).show();
					_self.show_tab(id, false);
				break;
				default:
					_self.show_tab(id, false);
				break;
			}
			return false;
		});
		
		if(_self.properties.currentSection != 'm_gallery')
			$('#'+_self.properties.listBlockId+' #'+_self.properties.containerPrefix+'m_gallery').css({position: 'absolute', top: '-1000px', opacity: 0}).show();
			
		if(_self.properties.currentSection != 'm_virtual_tour')
			$('#'+_self.properties.listBlockId+' #'+_self.properties.containerPrefix+'m_virtual_tour').css({position: 'absolute', top: '-1000px', opacity: 0}).show();	

		if(_self.properties.currentSection == 'm_map' || _self.properties.currentSection == 'm_panorama')
			_self.loaded[_self.properties.currentSection] = true;
	}
	
	this.show_tab = function(id, load){
		$('#' + _self.properties.sectionId + ' li').removeClass('active');
		$('#'+id).addClass('active');
		$('#'+_self.properties.listBlockId + ' .view-section').hide();
		_self.show_block(id, load);
	}
	
	this.show_block = function(id, load){
		var section_gid = $('#'+id).attr('sgid');
		var url = _self.properties.siteUrl + _self.properties.viewAjaxUrl + _self.properties.idListing + '/' + section_gid;
		if(load){
			$.ajax({
				url: url, 
				type: 'GET',
				cache: false,
				success: function(data){
					if(id == 'm_map' || id == 'm_panorama'){
						$('#'+_self.properties.containerPrefix+'m_'+section_gid).show().append(data);
					}else if(id == 'm_gallery'){
						$('#'+_self.properties.listBlockId+' #'+_self.properties.containerPrefix+'m_gallery').css({position: 'relative', top: '0', opacity: 1}).show();
						$('#'+_self.properties.containerPrefix+'m_'+section_gid).html(data).show();
					}else if(id == 'm_virtual_tour'){
						$('#'+_self.properties.listBlockId+' #'+_self.properties.containerPrefix+'m_virtual_tour').css({position: 'relative', top: '0', opacity: 1}).show();	
						$('#'+_self.properties.containerPrefix+'m_'+section_gid).html(data).show();
					}else{
						$('#'+_self.properties.containerPrefix+'m_'+section_gid).html(data).show();
					}
					_self.loaded[id] = true;
				}
			});
		}else{
			$('#'+_self.properties.containerPrefix+'m_'+section_gid).show();
		}
	}
	
	_self.Init(optionArr);
}
