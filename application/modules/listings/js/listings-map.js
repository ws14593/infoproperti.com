function listingsMap(optionArr){
	this.properties = {
		siteUrl: '',
		mapAjaxUrl: 'listings/ajax_index',
		
		operationType: 'sale',
		order: null,
		orderDirection: null,
		page: 1,
		
		useRss: false,
		rssAjaxUrl: 'listings/ajax_get_rss_link',
		rssId: 'rss_link',
		
		sectionId: 'listings_sections',
		mapBlockId: 'listings_map',
		errorObj: new Errors(),
		tIds: []
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
	}
	
	this.init_links = function(){
		$('#' + _self.properties.sectionId + ' li').bind('click', function(){
			var id = $(this).attr('id');
			$('#' + _self.properties.sectionId + ' li').removeClass('active');
			$('#'+id).addClass('active');
			_self.properties.operationType = $('#'+id).attr('sgid');
			_self.properties.page = 1;
			_self.loading_block();
			return false;
		});
		
		if(_self.properties.useRss){
			$('#'+_self.properties.rssId).bind('click', function(){
				$.get(_self.properties.siteUrl + _self.properties.rssAjaxUrl, {}, function(data){
					window.location.href = data; 
				});
				return false;
			});
		}
		
		if(_self.properties.tIds.length){
			for(var index in _self.properties.tIds){
				var id = _self.properties.tIds[index];
				$('#'+id+' select').live('change', function(){
					_self.properties.order = $(this).val();
					_self.loading_block();
					return false;
				});
				$('#'+id+' input[name=sorter_btn]').live('click', function(){
					if(_self.properties.orderDirection == 'ASC'){
						_self.properties.orderDirection = 'DESC';
					}else{
						_self.properties.orderDirection = 'ASC';
					}
					_self.loading_block();
					return false;
				});
				$('#'+id+'>.pages a[data-page]').live('click', function(){
					_self.properties.page = $(this).attr('data-page');
					_self.loading_block();
					return false;
				});				
			}
		}
	}
	
	this.loading_block = function(url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.mapAjaxUrl + '/';
			if(_self.properties.operationType) url += _self.properties.operationType + '/';
			if(_self.properties.order) url += _self.properties.order + '/';
			if(_self.properties.orderDirection) url += _self.properties.orderDirection + '/';
			url +=  _self.properties.page;
		}
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			success: function(data){
				$('#'+_self.properties.mapBlockId).html(data);
			}
		});
	}
	
	this.loading_post_block = function(post_data, url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.viewAjaxUrl;
		}
		$.ajax({
			url: url, 
			type: 'POST',
			data: post_data,
			cache: false,
			success: function(data){			
				$('#'+_self.properties.mapBlockId).append(data);
			}
		});
	}
		
	_self.Init(optionArr);
}
