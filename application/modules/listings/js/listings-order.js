function listingsOrder(optionArr){
	this.properties = {
		siteUrl: '',
		approveUrl: 'listings/ajax_order_approve/',
		declineUrl: 'listings/ajax_order_decline/',
		deleteUrl: 'listings/ajax_request_delete/',
		approveFormUrl: 'listings/ajax_order_approve_form/',
		blockId: 'orders_block',
		sectionId: 'orders_sections',
		cFormId: 'order_approve_form',
		id_close: 'close_btn',
		approveCnt: 0,
		declineCnt: 0,
		waitCnt: 0,
		contentObj: new loadingContent({loadBlockWidth: '544px', closeBtnClass: 'load_content_close', closeBtnPadding: 5}),
		errorObj: new Errors(),
	};
	
	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		
		$('#'+_self.properties.blockId+' .order_approve').live('click', function(){
			_self.get_form($(this).attr('data-id'));
			return false;
		});
		
		$('#'+_self.properties.blockId+' .order_decline').live('click', function(){
			$.ajax({
				url: _self.properties.siteUrl + _self.properties.declineUrl + $(this).attr('data-id'),
				type: 'GET',
				dataType: 'json',
				cache: false,
				success: function(data){
					if(typeof(data.error) != 'undefined' && data.error != ''){
						_self.properties.errorObj.show_error_block(data.error, 'error');
					}else{
						_self.properties.errorObj.show_error_block(data.success, 'success');
						$('#'+_self.properties.sectionId+' [sgid=wait]').trigger('click');
					
						_self.properties.waitCnt--;
						$('#section_wait').html(_self.properties.waitCnt);
						
						_self.properties.declineCnt++;
						$('#section_decline').html(_self.properties.declineCnt);
					}
				}
			});
			return false;
		});
		
		$('#'+_self.properties.blockId+' .request_delete').live('click', function(){
			var status = $(this).attr('data-status');
			$.ajax({
				url: _self.properties.siteUrl + _self.properties.deleteUrl + $(this).attr('data-id'),
				type: 'GET',
				dataType: 'json',
				cache: false,
				success: function(data){
					if(typeof(data.error) != 'undefined' && data.error != ''){
						_self.properties.errorObj.show_error_block(data.error, 'error');
					}else{
						_self.properties.errorObj.show_error_block(data.success, 'success');
						$('#'+_self.properties.sectionId+' [sgid='+status+']').trigger('click');
						
						_self.properties[status+'Cnt']--;
						$('#section_'+status).html(_self.properties[status+'Cnt']);
						
						$('#requests_count').html(_self.properties.approveCnt + _self.properties.declineCnt + _self.properties.waitCnt);
					}
				}
			});
			return false;
		});
	}
	
	this.get_form = function(id){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.approveFormUrl + id,
			type: 'GET',
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
				$('#'+_self.properties.id_close).unbind().bind('click', function(){
					_self.clearBox(id);
					return false;
				});
			}
		});		
		return false;
	}
	
	this.save_form = function(id, data){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.approveUrl + id,			
			type: 'post',
			data: data,
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.contentObj.hide_load_block();
					
					_self.properties.errorObj.show_error_block(data.success, 'success');
					$('#'+_self.properties.sectionId+' [sgid=wait]').trigger('click');
					
					_self.properties.waitCnt--;
						$('#section_wait').html(_self.properties.waitCnt);
						
					_self.properties.approveCnt++;
					$('#section_approve').html(_self.properties.approveCnt);
				}
			},
		});				
		return false;
	}
	
	this.clearBox = function(id){
		var data = $('#'+_self.properties.cFormId).serialize();
		_self.save_form(id, data);
	}
		
	_self.Init(optionArr);
}
