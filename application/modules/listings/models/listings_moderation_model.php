<?php
/**
* Listings moderation model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define('LISTINGS_MODERATION_TABLE', DB_PREFIX.'listings_moderation');
define('LISTINGS_RESIDENTIAL_SALE_MODERATION_TABLE', DB_PREFIX.'listings_residential_moderation');
define('LISTINGS_COMMERCIAL_SALE_MODERATION_TABLE', DB_PREFIX.'listings_commercial_moderation');
define('LISTINGS_LOT_AND_LAND_SALE_MODERATION_TABLE', DB_PREFIX.'listings_lot_and_land_moderation');
//define('LISTINGS_RESIDENTIAL_BUY_MODERATION_TABLE', DB_PREFIX.'listings_residential_moderation');
//define('LISTINGS_COMMERCIAL_BUY_MODERATION_TABLE', DB_PREFIX.'listings_commercial_moderation');
//define('LISTINGS_LOT_AND_LAND_BUY_MODERATION_TABLE', DB_PREFIX.'listings_lot_and_land_moderation');
define('LISTINGS_RESIDENTIAL_RENT_MODERATION_TABLE', DB_PREFIX.'listings_residential_moderation');
define('LISTINGS_COMMERCIAL_RENT_MODERATION_TABLE', DB_PREFIX.'listings_commercial_moderation');
define('LISTINGS_LOT_AND_LAND_RENT_MODERATION_TABLE', DB_PREFIX.'listings_lot_and_land_moderation');
//define('LISTINGS_RESIDENTIAL_LEASE_MODERATION_TABLE', DB_PREFIX.'listings_residential_moderation');
//define('LISTINGS_COMMERCIAL_LEASE_MODERATION_TABLE', DB_PREFIX.'listings_commercial_moderation');
//define('LISTINGS_LOT_AND_LAND_LEASE_MODERATION_TABLE', DB_PREFIX.'listings_lot_and_land_moderation');
define('LISTINGS_MODERATION_ALERTS_TABLE', DB_PREFIX.'listings_moderation_alerts');

class Listings_moderation_model extends Model{
	
	/**
	 * Link to Code Igniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 */
	private $DB;

	/**
	 * Parameters fields
	 */
	private $dop_fields = array();

	/**
	 * All fields
	 * @var array
	 */
	private $all_fields = array(
		'id_listing',
		'id_user',
		'address',
		'zip',
		'headline',
		'listing_file',
		'listing_file_date',
		'listing_file_name',
		'listing_file_comment',
		'listing_video',
		'listing_video_image',
		'listing_video_data',
		'admin_alert',
		'date_created',
		'date_modified',
	);

	/**
	 * Basic moderated fields
	 * @var array
	 */
	private $moderated_fields_main = array(
		'address',
		'zip',
		'headline',
		'listing_file',
		'listing_file_date',
		'listing_file_name',
		'listing_file_comment',
		'listing_video',
		'listing_video_image',
		'listing_video_data',
	);

	/**
	 * Field editor moderated fields
	 * @var array
	 */
	private $moderated_field_editor = array();
	
	/**
	 * All moderated fields
	 * @var array
	 */
	private $moderated_fields_all = array();

	/**
	 * Overview fields
	 * @var array
	 */
	private $_overview = array(
		'listing_file', 
		'listing_file_date', 
		'listing_video', 
		'listing_video_image', 
		'listing_video_data',
	);

	/**
	 * Constructor
	 * @return Listings_moderation_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		$this->CI->load->model('Listings_model');
	}

	/**
	 * Return listing by identifier
	 * @param integer $listing_id listing identifier
	 * @return array
	 */
	public function get_listing_by_id($listing_id){
		$attrs = (!empty($this->dop_fields))?array_merge($this->all_fields, $this->dop_fields):$this->all_fields;
		$result = $this->DB->select(implode(', ', $attrs))
						   ->from(LISTINGS_MODERATION_TABLE)
						   ->where('id_listing', $listing_id)
						   ->get()
						   ->result_array();
		return (is_array($result) && !empty($result))?$this->format_listing($result[0]):array();
	}

	/**
	 * Check listing is moderated
	 * @param integer $listing_id listing identifier
	 * @param boolean
	 */
	public function is_listing_moderated($listing_id){
		$result = $this->DB->select('COUNT(*) AS cnt')
						   ->from(LISTINGS_MODERATION_TABLE)
						   ->where('id_listing', $listing_id)
						   ->get()
						   ->result_array();
		return (!empty($result) && intval($result[0]['cnt']) > 0)?true:false;
	}

	/**
	 * Return listings as array
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param array $order_by soring order
	 * @param array $params query parameters
	 * @return array
	 */
	public function _get_listings_list($page=null, $items_on_page=null, $order_by=null, $params=array()){
		$this->DB->from(LISTINGS_MODERATION_TABLE);

		$attrs = (!empty($this->dop_fields))?array_merge($this->all_fields, $this->dop_fields):$this->all_fields;

		$this->DB->select(implode(', ', $attrs));

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach ($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field=>$dir){
				if(in_array($field, $this->all_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}

		if(!is_null($page)){
			$page = intval($page) ? intval($page) : 1;
			$this->DB->limit($items_on_page, $items_on_page * ($page - 1));
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return $this->format_listings($results);
		}
		return array();
	}

	/**
	 * Return number of listings
	 * @param array $params query parameters
	 * @return integer
	 */
	private function _get_listings_count($params=array()){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_MODERATION_TABLE);

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach ($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		$result = $this->DB->get()->result();
		if(!empty($result)){
			return intval($result[0]->cnt);
		}else{
			return 0;
		}
	}
	
	/**
	 * Return listings as array
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param array $order_by sorting order
	 * @return array
	 */
	public function get_listings_list($page=null, $items_on_page=null, $order_by=null){
		return $this->_get_listings_list($page, $items_on_page, $order_by);
	}
	
	/**
	 * Return number of listings
	 * @return integer
	 */
	public function get_listings_count(){
		return $this->_get_listings_count();
	}

	/**
	 * Format listing
	 * @param array $data listing data
	 * @return array
	 */
	public function format_listing($data){
		return array_shift($this->format_listings(array($data)));
	}

	/**
	 * Format listings list
	 * @param array $data listings data
	 * @return array
	 */
	public function format_listings($data){
		if(!empty($data)){
			$listings_ids = array();
			$property_type_gids = array();
		
			foreach($data as $k=>$v){
				$listings_ids[] = $data[$k]['id'] = $v['id_listing'];	
			}
		
			if(!empty($listings_ids)){
				$listings = array();
				$category_ids = array();
				$result = $this->Listings_model->get_listings_list(array('ids'=>$listings_ids), null, null, null, false);	
				foreach($result as $row){
					$listings[$row['id']] = $row;
					$property_type_gid = $this->Listings_model->get_field_editor_type($row);
					if(!in_array($property_type_gid, $property_type_gids)) $property_type_gids[] = $property_type_gid;
				}
				
				$field_editor_data = array();
				foreach($property_type_gids as $property_type_gid){
					$fields_for_select = $this->get_field_editor_for_moderate($property_type_gid);
					$results = $this->_get_field_editor_data($property_type_gid, $fields_for_select, $listings_ids);
					foreach($results as $listing_id=>$result){
						$field_editor_data[$listing_id] = $result;
					}
				}

				foreach($data as $k => $v){
					if(isset($field_editor_data[$v['id']]))
						$data[$k] = array_merge($data[$k], $field_editor_data[$v['id']]);
					
					if(isset($listings[$v['id']])) 
						$data[$k] = array_merge($listings[$v['id']], $data[$k]);
				}
			}
		}
		return $this->CI->Listings_model->format_listings($data);
	}
	
	/**
	 * Return field editor data
	 * @param string $property_type_gid property type GUID
	 * @param array $fields_for_select set of fields
	 * @param array $listing_ids listings identificators
	 */
	private function _get_field_editor_data($property_type_gid, $fields_for_select, $listing_ids){
		array_unshift($fields_for_select, 'id_listing');
		$this->DB->select(implode(',', $fields_for_select));
		$this->DB->from(constant('LISTINGS_'.strtoupper($property_type_gid).'_MODERATION_TABLE'));
		foreach((array)$listing_ids as $listing_id){
			$this->DB->where_in('id_listing', $listing_id);
		}		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[$r['id_listing']] = $r;
				unset($data[$r['id_listing']]['id_listing']);
			}
			return $data;
		}
		return array();
	}

	/**
	 * Save listing
	 * @param integer $listing_id listing identifier
	 * @param array $data save data
	 */
	public function save_listing($listing_id, $data){
		if(!$listing_id) return;

		$this->CI->Listings_model->set_format_settings(array('get_user'=>false, 'get_location'=>false, 'get_description'=>true));
		$listing_and_field_editor = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->CI->Listings_model->set_format_settings(array('get_user'=>true, 'get_location'=>true, 'get_description'=>false));
		
		$property_type_gid = $listing_and_field_editor['field_editor_type'];
		
		$this->CI->Listings_model->set_format_settings('use_format', false);
		$listing = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->CI->Listings_model->set_format_settings('use_format', true);
		
		$listing = array_merge($listing_and_field_editor, $listing);
	
		$fields = $this->get_fields_for_moderate();
		$field_editor = $this->get_field_editor_for_moderate($property_type_gid);		
		
		if(!$this->is_listing_moderated($listing_id)){
			//// при создании записи модерации нужны все поля
			foreach($fields as $field){
				if(array_key_exists($field, $data)){
					$save[$field] = $data[$field];					
				}else{
					$save[$field] = $listing[$field];
				}
			}
			foreach($field_editor as $field){
				if(array_key_exists($field, $data)){
					$fields_save[$field] = $data[$field];					
				}else{
					$fields_save[$field] = $listing[$field];
				}
			}
			
			$save['date_created'] = $save['date_modified'] = date('Y-m-d H:i:s');
			$save['id_listing'] = $listing_id;
			$save['id_user'] = $listing['id_user'];
			$this->DB->insert(LISTINGS_MODERATION_TABLE, $save);
		
			if(!empty($fields_save)){
				$fields_save['id_listing'] = $listing_id;
				$this->DB->insert(constant('LISTINGS_'.strtoupper($property_type_gid).'_MODERATION_TABLE'), $fields_save);
			}
			
			$moderation_send_mail = $this->CI->pg_module->get_module_config('listings', 'moderation_send_mail');
			if($moderation_send_mail){
				$emails = $this->CI->pg_module->get_module_config('listings', 'admin_moderation_emails');
				if($emails){
					$this->CI->load->model('Notifications_model');	
					$this->CI->Notifications_model->send_notification($emails, 'listing_need_moderate', array());
				}
			}
			
			$this->CI->load->model('menu/models/Indicators_model');
			$this->CI->Indicators_model->add('new_item_for_moderation', 'listings-'.$listing_id);
		}else{
			/// при повторном сохранении записи  меняем только пришедшие изменения
			foreach($fields as $field){
				if(array_key_exists($field, $data)){
					$save[$field] = $data[$field];
				}
			}
			foreach($field_editor as $field){
				if(array_key_exists($field, $data)){
					$fields_save[$field] = $data[$field];
				}
			}
		
			if(isset($data['admin_alert'])) $save['admin_alert'] = $data['admin_alert'];
			$save['date_modified'] = date('Y-m-d H:i:s');
			$this->DB->where('id_listing', $listing_id);
			$this->DB->update(LISTINGS_MODERATION_TABLE, $save);
		
			if(!empty($fields_save)){
				$this->DB->where('id_listing', $listing_id);
				$this->DB->update(constant('LISTINGS_'.strtoupper($property_type_gid).'_MODERATION_TABLE'), $fields_save);
			}
		}
	}

	/**
	 * Approve listing
	 * @param integer $listing_id listing identifier
	 */
	public function approve_listing($listing_id){
	
		$this->set_format_settings('use_format', false);
		$moderation = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('use_format', true);
		
		$this->CI->Listings_model->set_format_settings('use_format', false);
		$main_listing = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->CI->Listings_model->set_format_settings('use_format', true);

		$fields = $this->get_fields_for_moderate();
		$this->set_dop_fields($fields);
	
		$property_type_gid = $this->CI->Listings_model->get_field_editor_type($main_listing);
		$field_editor = $this->get_field_editor_for_moderate($property_type_gid);
		
		$save_data = array();
		foreach($fields as $field){
			$save_data[$field] = $moderation[$field];
		}
		foreach($field_editor as $field){
			$save_data[$field] = $moderation[$field];
		}
		$save_data['initial_moderation'] = 1;
		
		/// check initial_activity
		if($main_listing['initial_activity']){
			$save_data['initial_activity'] = 0;
		}
		$this->CI->Listings_model->save_listing($listing_id, $save_data);

		if($main_listing['initial_activity']){
			$this->CI->Listings_model->activate_listing($listing_id, 1, date('Y-m-d H:i:s', time()+$main_listing['initial_activity']*24*60*60));
		}

		$this->save_alert($listing_id, $moderation['id_user'], $moderation['admin_alert'], 'approve');

		$this->delete_listing($listing_id);
	}

	/**
	 * Decline listing
	 * @param integer $listing_id listing identifier
	 */
	public function decline_listing($listing_id){
		$moderation = $this->get_listing_by_id($listing_id);
		$this->save_alert($listing_id, $moderation['id_user'], $moderation['admin_alert'], 'decline');
		$this->delete_listing($listing_id);
		return;
	}

	/**
	 * Remove listing 
	 * @param integer $listing_id listing identifier
	 */
	public function delete_listing($listing_id){
		$this->set_format_settings('use_format', false);
		$listing = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->set_format_settings('use_format', true);
		
		$this->DB->where('id_listing', $listing_id);
		$this->DB->delete(LISTINGS_MODERATION_TABLE);
		
		$property_type_gid = $this->CI->Listings_model->get_field_editor_type($listing);
		if($property_type_gid){
			$this->DB->where('id_listing', $listing_id);
			$this->DB->delete(constant('LISTINGS_'.strtoupper($property_type_gid).'_MODERATION_TABLE'));
		}
		
		$this->CI->load->model('menu/models/Indicators_model');
		$this->CI->Indicators_model->delete('new_item_for_moderation', 'listings-'.$listing_id);
	}

	/**
	 * Set additional fields
	 * @param array $fields set of fields
	 */
	public function set_dop_fields($fields){
		$this->dop_fields = $fields;
	}

	/**
	 * Set format settings
	 * @param string $name option name
	 * @param boolean $value option value
	 * @return array
	 */
	public function set_format_settings($name, $value=false){
		return $this->CI->Listings_model->set_format_settings($name, $value);
	}

	/**
	 * Compare listing data
	 * @param integer $listing_id listing identifier
	 */
	public function compare($listing_id){
		$return = array('fields'=>array(), 'sections'=>array());

		$sections = array('overview'=>$this->_overview);

		$fields = $this->get_fields_for_moderate($listing_id);
		$this->set_dop_fields($fields);
	
		$this->set_format_settings('get_description', true);
		$listing_and_field_editor = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->set_format_settings('get_description', false);

		$this->CI->Listings_model->set_format_settings('use_format', false);
		$listing = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->CI->Listings_model->set_format_settings('use_format', true);
		
		$listing = array_merge($listing_and_field_editor, $listing);
		$moderation = $this->get_listing_by_id($listing_id);

		$property_type_gid = $listing_and_field_editor['field_editor_type'];
		$field_editor = $this->get_field_editor_for_moderate($property_type_gid);

		foreach($fields as $field){
			if(!empty($moderation[$field]) && (!$listing['initial_moderation'] || $listing[$field] != $moderation[$field])){
				$return['fields'][$field] = 1;
			}
		}
		
		foreach($field_editor as $field){
			$moderation[$field] = trim(strip_tags($moderation[$field]));
			$listing[$field] = trim(strip_tags($listing[$field]));

			if(!empty($moderation[$field]) && (!$listing['initial_moderation'] || $listing[$field] != $moderation[$field])){
				$return['fields'][$field] = 1;
			}
		}

		foreach($sections as $section => $sfields){
			foreach($sfields as $sfield){
				if(!empty($return['fields']) && isset($return['fields'][$sfield])){
					$return['sections'][$section] = 1;
					break;
				}
			}
		}
		return $return;
	}

	/**
	 * Check data is changed
	 * @param integer $listing_id listing identifier
	 * @param array $saved_data saved data
	 * @param array $new_data new data
	 * @return boolean
	 */
	public function is_data_changed($listing_id, $saved_data, $new_data){
	
		$fields = $this->get_fields_for_moderate();
		foreach($fields as $field){
			if(isset($new_data[$field])){
				$saved_value = trim(strip_tags($saved_data[$field]));
				$new_value = trim(strip_tags($new_data[$field]));
				if($saved_value != $new_value && !empty($new_value)){
					return true;
				}
			}
		}
		
		$property_type_gid = $this->CI->Listings_model->get_field_editor_type(array_merge($saved_data, $new_data));
		$fields = $this->get_field_editor_for_moderate($property_type_gid);
		foreach($fields as $field){
			if(isset($new_data[$field])){
				$saved_value = trim(strip_tags($saved_data[$field]));
				$new_value = trim(strip_tags($new_data[$field]));
				if($saved_value != $new_value && !empty($new_value)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Return field editor fields for moderationn guid
	 * @param integer $property_type_gid field editor type guid
	 * @return array
	 */
	public function get_field_editor_for_moderate($property_type_gid){
		if(!isset($this->moderated_field_editor[$property_type_gid])){
			$this->CI->load->model('Field_editor_model');
		
			$this->CI->Field_editor_model->initialize($property_type_gid);
		
			$fields = array();
			$params['where']['editor_type_gid'] = $property_type_gid;
			$params['where_in']['field_type'] = array('text', 'textarea');
			$custom_fields = $this->CI->Field_editor_model->get_fields_list($params, null, array(), false);
			if(!empty($custom_fields)){
				foreach($custom_fields as $field){
					$fields[] = $this->CI->Field_editor_model->get_field_select_name($field['gid']);
				}
			}
			$this->moderated_field_editor[$property_type_gid] = $fields;
		}
		return $this->moderated_field_editor[$property_type_gid];
	}

	/**
	 * Return fields for moderation
	 * @return array
	 */
	public function get_fields_for_moderate(){
		if(empty($this->moderated_fields_all)){
			$this->moderated_fields_all = $this->moderated_fields_main;
		}
		return $this->moderated_fields_all;
	}

	/// alert methods
	
	/**
	 * Save moderation alert
	 * @param integer $listing_id listing identifier
	 * @param integer $user_id user identifier
	 * @param string $content alert content
	 * @param string $status alert status
	 */
	public function save_alert($listing_id, $user_id, $content, $status='approve'){
		$this->delete_alert($listing_id);

		$save_data['id_listing'] = $listing_id;
		$save_data['content'] = $content;
		$save_data['status'] = $status;
		$save_data['id_user'] = $user_id;
		$this->DB->insert(LISTINGS_MODERATION_ALERTS_TABLE, $save_data);
	}

	/**
	 * Remove moderation alert
	 * @param integer $listing_id listing identifier
	 */
	public function delete_alert($listing_id){
		$this->DB->where('id_listing', $listing_id);
		$this->DB->delete(LISTINGS_MODERATION_ALERTS_TABLE);
		return;
	}

	/**
	 * Return moderation alert data
	 * @param integer $listing_id listing identifier
	 * @return array
	 */
	public function get_alert($listing_id){
		$this->DB->select('id, content, status')
				 ->from(LISTINGS_MODERATION_ALERTS_TABLE)
				 ->where('id_listing', $listing_id);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			return $result[0];
		}else{
			return array();
		}
	}

	/**
	 * Return user moderation alerts
	 * @param integer $user_id user identifier
	 * @return array
	 */
	public function get_user_alerts($user_id){
		$this->DB->select('id, id_listing, content, status')
				 ->from(LISTINGS_MODERATION_ALERTS_TABLE)
				 ->where('id_user', $user_id)
				 ->order_by('id DESC');
		$result = $this->DB->get()->result_array();
		return $result;
	}

	/**
	 * Return last moderation alert from user
	 * @param integer $user_id
	 * @return array
	 */
	public function get_last_user_alert($user_id){
		$res = $this->get_user_alerts($user_id);
		if(!empty($res)){
			return $res[0];
		}else{
			return array();
		}
	}

	/**
	 * Return listings moderation status
	 * @param array $listing_ids lsitings identifiers
	 */
	public function get_listings_status($listing_ids){
		$return = array();
		foreach($listing_ids as $listing_id){
			$return[$listing_id] = 'default';
		}

		$this->DB->select('id_listing')->from(LISTINGS_MODERATION_TABLE)->where_in('id_listing', $listing_ids);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			foreach($result as $r){
				$return[$r['id_listing']] = 'wait';
			}
		}

		$this->DB->select('id_listing, status')
				 ->from(LISTINGS_MODERATION_ALERTS_TABLE)
				 ->where_in('id_listing', $listing_ids);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			foreach($result as $r){
				$return[$r['id_listing']] = ($r['status'] == 'decline')?'decline':'approved';
			}
		}

		return $return;
	}
}
