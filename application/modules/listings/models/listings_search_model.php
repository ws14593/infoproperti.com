<?php
/**
* Listings saved searches model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define('LISTINGS_SEARCHES_TABLE', DB_PREFIX.'listings_searches');

class Listings_search_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * @var object
	 */
	private $DB;

	/**
	 * Searches fields
	 */
	private $_fields = array(
		'id', 
		'id_user', 
		'search_data', 
		'date_search',
	);

	/**
	 * Saved search parameters
	 */
	private $saved_params = array(
		'keyword',
		'type',
		'id_country',
		'id_region',
		'id_city',
		'id_category',
		'property_type',
		'price_min',
		'price_max',
		'field_editor_data',
		'with_photo',
		'open_house',
		'by_private',
		'by_discount',
		'id',
		'zip',
		'radius',
		'booking_date_start',
		'booking_date_end',
		'booking_guests',
	);
	
	/**
	 * Criteria separator
	 * @var string
	 */
	private $_separator = '; ';

	/**
	 * Construct
	 * @return Listings_search_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return search criteria
	 * @param integer $search_id saved search identifier
	 * @return array
	 */
	public function get_search_by_id($search_id){
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_SEARCHES_TABLE);
		$this->DB->where('id', $search_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = $this->format_search($results[0]);
			return $data;
		}
	}
	
	/**
	 * Filter by user
	 * @param integer $user_id user identifier
	 * @return array
	 */
	private function _get_searches_by_user($user_id){
		$params = array();
		if(!$user_id) return array();
		$params['where']['id_user'] = $user_id;
		return $params;
	}

	/**
	 * Return list of search requests as array
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param array $order_by sorting order
	 * @param array $params filter criteria
	 * @return array
	 */
	private function _get_searches_list($page=null, $items_on_page=null, $order_by=array(), $params=array()){
		$this->DB->from(LISTINGS_SEARCHES_TABLE);

		$this->DB->select('id, id_user, search_data, date_search');

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field => $value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field => $value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				$this->DB->order_by($field.' '.$dir);
			}
		}

		if(!is_null($page)){
			$page = intval($page) ? intval($page) : 1;
			$this->DB->limit($items_on_page, $items_on_page * ($page - 1));
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return $this->format_searches($results);
		}
		return array();
	}

	/**
	 * Return number of search requests
	 * @param array $params filter criteria
	 * @return integer
	 */
	private function _get_searches_count($params){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_SEARCHES_TABLE);

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		$result = $this->DB->get()->result();
		if(!empty($result)){
			return intval($result[0]->cnt);
		}else{
			return 0;
		}
	}
	
	/**
	 * Return list of search requests as array
	 * @param array $filters filters criteria
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param array $order_by sorting order
	 * @return array
	 */
	public function get_searches_list($filters=array(), $page=null, $items_on_page=null, $order_by=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge($params, $this->{'_get_searches_by_'.$filter}($value));
		}
		return $this->_get_searches_list($page, $items_on_page, $order_by, $params, array(), $formatted);
	}
	
	/**
	 * Return number of search requests
	 * @param array $filters filters criteria
	 * @return integer
	 */
	public function get_searches_count($filters=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge($params, $this->{'_get_searches_by_'.$filter}($value));
		}
		return $this->_get_searches_count($params);
	}

	/**
	 * Format data of search requests
	 * @param array $data
	 * @return array
	 */
	public function format_search($data){
		return array_shift($this->format_searches(array($data)));
	}

	/**
	 * Format list of search criteria
	 * @param array $data data
	 * @return array
	 */
	public function format_searches($data){
		$this->CI->load->model('Listings_model');
		
		$operation_types = $this->CI->Listings_model->get_operation_types(true);
	
		foreach($data as $key=>$search){
			$search_data = $search['search_data'] = $search['search_data'] ? (array)unserialize($search['search_data']) : array();
		
			$current_search_data = array();
			if(isset($search_data['type']) && in_array($search_data['type'], $operation_types)){
				$operation_type = $search['search_data']['type'];
			}else{
				$operation_type = $this->CI->Listings_model->get_operation_type_gid_default(true);
			}
		
			if($operation_type == 'sold'){
				$search['name'] = l('text_sold', 'listings');
			}else{
				$search['name'] = ld_option('operation_type_search', 'listings', $operation_type);
			}
		
			$exclude = array('type');
		
			if(isset($search_data['id_category'])){
				$this->CI->load->helper('properties');
				if(isset($search_data['property_type'])){
					$value = property_value($search_data);
					$search['name'] .= $this->_separator.($value ? $value : $search_data['property_type']);
					$exclude[] = 'property_type';
				}else{
					$value = category_value($search_data);
					$search['name'] .= $this->_separator.($value ? $value : $search_data['id_category']);
				}
				$exclude[] = 'id_category';
			}

			if(isset($search_data['id_country'])){
				$this->CI->load->helper('countries');
				$location_data = array();
				$location_data[] = $search_data['id_country'];
				if(isset($search_data['id_region'])){
					$location_data[] = $search_data['id_region'];				
					if(isset($search_data['id_city'])){
						$location_data[] = $search_data['id_city'];
						$exclude[] = 'id_city';
					}
					$exclude[] = 'id_region';
				}
				$exclude[] = 'id_country';
				$location = cities_output_format(array($location_data));
				if(!empty($location)) $search['name'] .= $this->_separator.$location[0];
			}
			
			$this->config->load('date_formats', TRUE);
			$date_format = $this->config->item('date_format_date_literal', 'date_formats');
	
			if(isset($search_data['booking_date_start'])){
				$search_data['booking_date_start'] = date($date_format, strtotime($search_data['booking_date_start']));
			}
		
			if(isset($search_data['booking_date_end'])){
				$search_data['booking_date_end'] = date($date_format, strtotime($search_data['booking_date_end']));
			}
			
			$this->load->helper('start');
	
			$price = '';
			if(isset($search_data['price_min'])){
				$price = l('text_price_from', 'listings').' '.strip_tags(currency_format_output(array('value'=>$search_data['price_min'])));
				$exclude[] = 'price_min';
			}
		
			if(isset($search_data['price_max'])){
				if($price) $price .= ' ';
				$price .= l('text_price_to', 'listings').' '.strip_tags(currency_format_output(array('value'=>$search_data['price_max'])));
				$exclude[] = 'price_max';
			}
		
			if(!empty($price)) $search['name'] .= $this->_separator.'<span dir="ltr">'.$price.'</span>';
		
			if(isset($search_data['field_editor_data'])){
				$property_type_gid = $this->CI->Listings_model->get_field_editor_type($search_data);
			
				$this->CI->load->model('Field_editor_model');
				$this->CI->Field_editor_model->initialize($property_type_gid);
			
				$fields_gids = array();
				foreach($search_data['field_editor_data'] as $form_name=>$form_data){
					$fields_gids = array_merge($fields_gids, array_keys($form_data));
				}
			
				$params = array('where_in'=>array('gid'=>array_keys($fields_gids)));
				$fields_data = $this->CI->Field_editor_model->get_fields_list($params);
			
				$field_editor_data = array('gids'=>array(), 'data'=>array());
				$field_editor_range = array('gids'=>array(), 'data'=>array());
				foreach($search_data['field_editor_data'] as $form_name => $form_data){
					foreach($form_data as $field_gid => $field_data){
						if(isset($field_data['range']) && is_array($field_data['range'])){
							if(isset($field_data['range']['min'])){
								$field_editor_data['gids'][$field_gid] = $field_data['range']['min'];
								$field_editor_data['data'][$fields_data[$field_gid]['field_name']] = $field_data['range']['min'];
							}else{
								$field_editor_data['gids'][$field_gid] = 0;
								$field_editor_data['data'][$fields_data[$field_gid]['field_name']] = 0;
							}
							if(isset($field_data['range']['max'])){
								$field_editor_range['gids'][$field_gid] = $field_data['range']['max'];
								$field_editor_range['data'][$fields_data[$field_gid]['field_name']] = $field_data['range']['max'];
							}else{
								$field_editor_range['gids'][$field_gid] = 0;
								$field_editor_range['data'][$fields_data[$field_gid]['field_name']] = 0;
							}
						}else{
							$field_editor_data['gids'][$field_gid] = $field_data;
							$field_editor_data['data'][$fields_data[$field_gid]['field_name']] = $field_data;
						}
					}
				}
			
				$values = array();
			
				if(!empty($field_editor_data['gids'])){
					$params = array('where_in'=>array('gid'=>array_keys($field_editor_data['gids'])));
					$results = $this->CI->Field_editor_model->format_list_fields_for_view($params, array($field_editor_data['data']));
					foreach($results as $result){
						foreach($result as $field_gid=>$field_data){
							if($field_data['value_str']){
								$value = $field_data['value_str'];
							}elseif($field_data['value_dec']){
								$value = $field_data['value_dec'];
							}elseif($field_data['value_original']){
								$value = $field_data['value_original'];
							}else{
								$value = $field_data['value'];
							}
							$values[$field_gid] = $value || $field_editor_data['gids'][$field_gid] !== 0 ? $value : 0;
						}
					}
				}
			
				if(!empty($field_editor_range['gids'])){
					$params = array('where_in'=>array('gid'=>array_keys($field_editor_range['gids'])));
					$results = $this->CI->Field_editor_model->format_list_fields_for_view($params, array($field_editor_range['data']));
					foreach($results as $result){
						foreach($result as $field_gid=>$field_data){
							if($field_data['value_str']){
								$value = $field_data['value_str'];
							}elseif($field_data['value_dec']){
								$value = $field_data['value_dec'];
							}elseif($field_data['value_original']){
								$value = $field_data['value_original'];
							}else{
								$value = $field_data['value'];
							}
						
							if($value){
								if(isset($values[$field_gid]) && $values[$field_gid]){
									$values[$field_gid] .= '-'.$value;
								}else{
									$values[$field_gid] = '<'.$value;
								}
							}elseif(isset($values[$field_gid])){
								$values[$field_gid] = '>'.$values[$field_gid];
							}
						}
					}
				}
			
				foreach($values as $field_gid => $value){
					$search['name'] .= $this->_separator.$fields_data[$field_gid]['name'].': '.$value;
				}
			
				$exclude[] = 'field_editor_data';
			}
	
			foreach($search_data as $k=>$v){
				if(in_array($k, $exclude)) continue;
				$search['name'] .= $this->_separator.l('field_'.$k, 'listings').': '.$v;
			}
		
			$data[$key] = $search;
		}
		return $data;
	}

	/**
	 * Validate data of search criteria
	 * @param integer $search_id search identifier
	 * @param array $data search data
	 */
	public function validate_search($search_id, $data){
		$return = array('errors'=>array(), 'data'=>array());

		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
		}

		if(isset($data['search_data'])){
			$search_data = array();
			$this->CI->load->model('Listings_model');

			foreach($this->saved_params as $f){
				if(!empty($data['search_data'][$f])){
					$search_data[$f] = $data['search_data'][$f];
				}
			}

			if(!empty($search_data)){
				$return['data']['search_data'] = serialize($search_data);
			}else{
				$return['errors'][] = l('error_empty_save_search_data', 'listings');
			}
		}

		return $return;
	}

	/**
	 * Save search criteria
	 * @param integer $search_id search identifier
	 * @param array $data search data
	 * @return integer
	 */
	public function save_search($search_id, $data){
		if(empty($search_id)){
			$data['date_search'] = date('Y-m-d H:i:s');
			$this->DB->insert(LISTINGS_SEARCHES_TABLE, $data);
			$search_id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $search_id);
			$this->DB->update(LISTINGS_SEARCHES_TABLE, $data);
		}
		return $search_id;
	}

	/**
	 * Remove search criteria
	 * @param integer $search_id search identifier
	 */
	public function delete_search($search_id){
		$this->DB->where('id', $search_id);
		$this->DB->delete(LISTINGS_SEARCHES_TABLE);
	}
}
