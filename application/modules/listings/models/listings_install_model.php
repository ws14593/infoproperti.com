<?php
/**
* Listings install model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Listings_install_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Operation types of demo listings
	 */
	private $operation_types = array('sale', 'buy', 'rent', 'lease');

	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"main_items" => array(
					"action" => "none",
					"items" => array(
						"listings_menu_item" => array("action" => "create", "link" => "admin/listings/index", "status" => 1, "sorter" => 3),
						'moderation-items' => array(
							'action'=>'none',
							'items' => array(
								'listings_moder_item' => array('action' => 'create', 'link' => 'admin/listings/moderation', 'status' => 1, 'sorter' => 1),
							)
						)
					),
				),
			),			
		),
		
		"admin_listings_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Listings",
			"items" => array(
				"listings-list-item" => array("action" => "create", "link" => "admin/listings/index", "status" => 1, "sorter" => 1),
				"listings-moderation-item" => array("action" => "create", "link" => "admin/listings/moderation", "status" => 1, "sorter" => 2),
				"listings-booking-item" => array("action" => "create", "link" => "admin/listings/orders", "status" => 1, "sorter" => 3),
				"wish-lists-item" => array("action" => "create", "link" => "admin/listings/wish_lists", "status" => 1, "sorter" => 4),
			),
		),
		
		"private_top_menu" => array(
			"action" => "none",
			"items" => array(
				"private-main-my-listings-item" => array("action" => "create", "link" => "listings/my", "status" => 1, "sorter" => 3),
				"private-main-my-booking-item" => array("action" => "create", "link" => "listings/orders", "status" => 1, "sorter" => 4),
				"private-main-search-preferences-item" => array("action" => "create", "link" => "listings/index/sale", "status" => 1, "sorter" => 6),
			),
		),
		
		"company_top_menu" => array(
			"action" => "none",
			"items" => array(
				"company-main-my-listings-item" => array("action" => "create", "link" => "listings/my", "status" => 1, "sorter" => 3),
				"company-main-my-booking-item" => array("action" => "create", "link" => "listings/orders", "status" => 1, "sorter" => 4),
				"company-main-search-preferences-item" => array("action" => "create", "link" => "listings/index/sale", "status" => 1, "sorter" => 6),
			),
		),
		
		"agent_top_menu" => array(
			"action" => "none",
			"items" => array(
				"agent-main-my-listings-item" => array("action" => "create", "link" => "listings/my", "status" => 1, "sorter" => 3),
				"agent-main-my-booking-item" => array("action" => "create", "link" => "listings/orders", "status" => 1, "sorter" => 4),
				"agent-main-search-preferences-item" => array("action" => "create", "link" => "listings/index/sale", "status" => 1, "sorter" => 6),
			),
		),

		"private_account_menu" => array(
			"action" => "none",
			"items" => array(
				"private_my_listings_item" => array("action" => "create", "link" => "listings/my", "status" => 1, "sorter" => 3),
				"private_my_booking_item" => array(
					"action" => "create", 
					"link" => "listings/orders", 
					"status" => 1, 
					"sorter" => 4,
					"items" => array(
						"private_my_orders_item" => array("action" => "create", "link" => "listings/orders", "status" => 1, "sorter" => 1),
						"private_my_requests_item" => array("action" => "create", "link" => "listings/requests", "status" => 1, "sorter" => 2),
					),	
				),
				"private-listings-main-find-item" => array(
					"action" => "create", 
					"link" => "listings/search", 
					"status" => 1, 
					"sorter" => 7,				
					"items" => array(
						"private_my_quick_search" => array("action" => "create", "link" => "listings/index/sale", "status" => 1, "sorter" => 1),
						"private_my_saved_listings" => array("action" => "create", "link" => "listings/saved", "status" => 1, "sorter" => 2),
						"private_my_searches_item" => array("action" => "create", "link" => "listings/preferences", "status" => 1, "sorter" => 3),
					),				
				),
			),
		),
		
		"company_account_menu" => array(
			"action" => "none",
			"items" => array(
				"company_my_listings_item" => array("action" => "create", "link" => "listings/my", "status" => 1, "sorter" => 3),
				"company_my_booking_item" => array(
					"action" => "create", 
					"link" => "listings/orders", 
					"status" => 1, 
					"sorter" => 4,
					"items" => array(
						"company_my_orders_item" => array("action" => "create", "link" => "listings/orders", "status" => 1, "sorter" => 1),
						"company_my_requests_item" => array("action" => "create", "link" => "listings/requests", "status" => 1, "sorter" => 2),
					)
				),
				"company-listings-main-find-item" => array(
					"action" => "create", 
					"link" => "listings/search", 
					"status" => 1, 
					"sorter" => 7,
					"items" => array(
						"company_my_quick_search" => array("action" => "create", "link" => "listings/index/sale", "status" => 1, "sorter" => 1),
						"company_my_saved_listings" => array("action" => "create", "link" => "listings/saved", "status" => 1, "sorter" => 2),
						"company_my_searches_item" => array("action" => "create", "link" => "listings/preferences", "status" => 1, "sorter" => 3),
					),				
				),
			),
		),
		
		"agent_account_menu" => array(
			"action" => "none",
			"items" => array(
				"agent_my_listings_item" => array("action" => "create", "link" => "listings/my", "status" => 1, "sorter" => 3),
				"agent_my_booking_item" => array(
					"action" => "create", 
					"link" => "listings/orders", 
					"status" => 1, 
					"sorter" => 4,
					"items" => array(
						"agent_my_orders_item" => array("action" => "create", "link" => "listings/orders", "status" => 1, "sorter" => 1),
						"agent_my_requests_item" => array("action" => "create", "link" => "listings/requests", "status" => 1, "sorter" => 2),
					)
				),
				"agent-listings-main-find-item" => array(
					"action" => "create", 
					"link" => "listings/search", 
					"status" => 1, 
					"sorter" => 7,
					"items" => array(
						"agent_my_quick_search" => array("action" => "create", "link" => "listings/index/sale", "status" => 1, "sorter" => 1),
						"agent_my_saved_listings" => array("action" => "create", "link" => "listings/saved", "status" => 1, "sorter" => 2),
						"agent_my_searches_item" => array("action" => "create", "link" => "listings/preferences", "status" => 1, "sorter" => 3),
					),				
				),
			),
		),
		
		"listings_menu" => array(
			"action" => "create",
			"name" => "User mode - Types of listings",
			"items" => array(
				"listings_for_sale_item" => array("action" => "create", "link" => "listings/sale_list", "status" => 1, "sorter" => 1),
				"listings_for_buy_item" => array("action" => "create", "link" => "listings/buy_list", "status" => 1, "sorter" => 2),
			),
		),
		
		'private_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'private-main-buyers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/sale', 
					'status' => 1, 
					'sorter' => 1,
					'items' => array(
						'private-main-buyers-properties-item' => array('action' => 'create', 'link' => 'listings/index/sale', 'status' => 1, 'sorter' => 1),
						'private-main-buyers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/sale', 'status' => 1, 'sorter' => 2),
						'private-main-buyers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/sale', 'status' => 1, 'sorter' => 3),
						'private-main-buyers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/sale', 'status' => 1, 'sorter' => 4),
						'private-main-buyers-sold-item' => array('action' => 'create', 'link' => 'listings/index/sold', 'status' => 1, 'sorter' => 5),
					),
				),
				'private-main-sellers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/buy', 
					'status' => 1, 
					'sorter' => 2,
					'items' => array(
						'private-main-sellers-properties-item' => array('action' => 'create', 'link' => 'listings/index/buy', 'status' => 1, 'sorter' => 1),
						'private-main-sellers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/buy', 'status' => 1, 'sorter' => 2),
						'private-main-sellers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/buy', 'status' => 1, 'sorter' => 3),
						'private-main-sellers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/buy', 'status' => 1, 'sorter' => 4),
					),
				),				
			),
		),
		
		'company_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'company-main-buyers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/sale', 
					'status' => 1, 
					'sorter' => 1,
					'items' => array(
						'company-main-buyers-properties-item' => array('action' => 'create', 'link' => 'listings/index/sale', 'status' => 1, 'sorter' => 1),
						'company-main-buyers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/sale', 'status' => 1, 'sorter' => 2),
						'company-main-buyers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/sale', 'status' => 1, 'sorter' => 3),
						'company-main-buyers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/sale', 'status' => 1, 'sorter' => 4),
						'company-main-buyers-sold-item' => array('action' => 'create', 'link' => 'listings/index/sold', 'status' => 1, 'sorter' => 5),
					),
				),
				'company-main-sellers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/buy', 
					'status' => 1, 
					'sorter' => 2,
					'items' => array(
						'company-main-sellers-properties-item' => array('action' => 'create', 'link' => 'listings/index/buy', 'status' => 1, 'sorter' => 1),
						'company-main-sellers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/buy', 'status' => 1, 'sorter' => 2),
						'company-main-sellers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/buy', 'status' => 1, 'sorter' => 3),
						'company-main-sellers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/buy', 'status' => 1, 'sorter' => 4),
					),
				),
			),
		),
		
		'agent_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'agent-main-buyers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/sale', 
					'status' => 1, 
					'sorter' => 1,
					'items' => array(
						'agent-main-buyers-properties-item' => array('action' => 'create', 'link' => 'listings/index/sale', 'status' => 1, 'sorter' => 1),
						'agent-main-buyers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/sale', 'status' => 1, 'sorter' => 2),
						'agent-main-buyers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/sale', 'status' => 1, 'sorter' => 3),
						'agent-main-buyers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/sale', 'status' => 1, 'sorter' => 4),
						'agent-main-buyers-sold-item' => array('action' => 'create', 'link' => 'listings/index/sold', 'status' => 1, 'sorter' => 5),
					),
				),
				'agent-main-sellers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/buy', 
					'status' => 1, 
					'sorter' => 2,
					'items' => array(
						'agent-main-sellers-properties-item' => array('action' => 'create', 'link' => 'listings/index/buy', 'status' => 1, 'sorter' => 1),
						'agent-main-sellers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/buy', 'status' => 1, 'sorter' => 2),
						'agent-main-sellers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/buy', 'status' => 1, 'sorter' => 3),
						'agent-main-sellers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/buy', 'status' => 1, 'sorter' => 4),
					),
				),
			),
		),
		
		'guest_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'guest-main-buyers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/sale', 
					'status' => 1, 
					'sorter' => 1,
					'items' => array(
						'guest-main-buyers-properties-item' => array('action' => 'create', 'link' => 'listings/index/sale', 'status' => 1, 'sorter' => 1),
						'guest-main-buyers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/sale', 'status' => 1, 'sorter' => 2),
						'guest-main-buyers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/sale', 'status' => 1, 'sorter' => 3),
						'guest-main-buyers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/sale', 'status' => 1, 'sorter' => 4),
						'guest-main-buyers-sold-item' => array('action' => 'create', 'link' => 'listings/index/sold', 'status' => 1, 'sorter' => 5),
					),
				),
				'guest-main-sellers-item' => array(
					'action' => 'create', 
					'link' => 'listings/index/buy', 
					'status' => 1, 
					'sorter' => 2,
					'items' => array(
						'guest-main-sellers-properties-item' => array('action' => 'create', 'link' => 'listings/index/buy', 'status' => 1, 'sorter' => 1),
						'guest-main-sellers-agents-item' => array('action' => 'create', 'link' => 'listings/agents/buy', 'status' => 1, 'sorter' => 2),
						'guest-main-sellers-owner-item' => array('action' => 'create', 'link' => 'listings/privates/buy', 'status' => 1, 'sorter' => 3),
						'guest-main-sellers-open-house-item' => array('action' => 'create', 'link' => 'listings/open_house/buy', 'status' => 1, 'sorter' => 4),
					),
				),
			),
		)
	);
	
	/**
	 * Uploads configuration
	 * @var array
	 */
	private $uploads = array(
		array(
			"gid" => "listing-photo", 
			"name" => "Listings photo", 
			"min_height" => 400, 
			"min_width" => 400, 
			"max_height" => 5000, 
			"max_width" => 5000, 
			"max_size" => 10000000, 
			"name_format" => "generate", 
			"file_formats" => array("jpg", "gif", "png"), 
			"default_img" => "default_listing_photo.png",
			"thumbs" => array(
				"620_400" => array("width"=>620, "height"=>400, "effect"=>"none", "watermark"=>"image-wm", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"200_200" => array("width"=>200, "height"=>200, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"60_60" => array("width"=>60, "height"=>60, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
			),
		),
		array(
			"gid" => "listing-vtour", 
			"name" => "Listings virtual tour", 
			"min_height" => 400, 
			"min_width" => 620, 
			"max_height" => 5000, 
			"max_width" => 5000, 
			"max_size" => 10000000, 
			"name_format" => "generate", 
			"file_formats" => array("jpg", "gif", "png"), 
			"default_img" => "",
			"thumbs" => array(
				"620_400" => array("width"=>620, "height"=>400, "effect"=>"none", "crop_param"=>"static_height", "crop_color"=>"ffffff"),
				"200_200" => array("width"=>200, "height"=>200, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"60_60" => array("width"=>60, "height"=>60, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
			),
		),
		array(
			"gid" => "listing-search-photo", 
			"name" => "Listings search photo", 
			"max_height" => 1000, 
			"max_width" => 1000, 
			"max_size" => 100000, 
			"name_format" => "generate", 
			"file_formats" => array("jpg", "gif", "png"), 
			"default_img" => "default_listing_photo.png",
			"thumbs" => array(
				"big" => array("width"=>200, "height"=>200, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"middle" => array("width"=>150, "height"=>150, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"middle_anim" => array("width"=>150, "height"=>150, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff", "animation"=>1, "delay"=>"200", "loops"=>0, "disposal"=>2, "transparent_color"=>"ffffff"),
				"small" => array("width"=>100, "height"=>100, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
			),
		),
		array(
			"gid" => "listing-slider-photo", 
			"name" => "Listings slider photo", 
			"max_height" => 5000, 
			"max_width" => 5000, 
			"max_size" => 10000000, 
			"name_format" => "generate", 
			"file_formats" => array("jpg", "gif", "png"), 
			"default_img" => "default_listing_photo.png",
			"thumbs" => array(
				"1600_440" => array("width"=>1600, "height"=>440, "effect"=>"none", "watermark"=>"image-wm", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"980_440" => array("width"=>980, "height"=>440, "effect"=>"none", "watermark"=>"image-wm", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"654_395" => array("width"=>654, "height"=>395, "effect"=>"none", "watermark"=>"image-wm", "crop_param"=>"crop", "crop_color"=>"ffffff"),
			),
		),
		array(
			"gid" => "listing-wish-list-photo", 
			"name" => "Listings wish list photo", 
			"max_height" => 1000, 
			"max_width" => 1000, 
			"max_size" => 100000, 
			"name_format" => "generate", 
			"file_formats" => array("jpg", "gif", "png"), 
			"default_img" => "default_listing_photo.png",
			"thumbs" => array(
				"big" => array("width"=>200, "height"=>200, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
			),
		),
	);
	
	/**
	 * Upload gallery configuration
	 * @var array
	 */
	private $upload_gallery = array(
		array("gid"=>"listings-photo", "name" => "Listing photos", "gid_upload_config" => "listing-photo", "max_items_count" => 15, "use_moderation" => 0, "module"=>"listings", "model"=>"Listings_model", "callback"=>"photo_callback"),
		array("gid"=>"listings-vtour", "name" => "Listing virtual tours", "gid_upload_config" => "listing-vtour", "max_items_count" => 10, "use_moderation" => 0, "module"=>"listings", "model"=>"Listings_model", "callback"=>"vtour_callback"),
	); 

	/**
	 * Video uploads configuration
	 * @var array
	 */
	private $video_uploads = array(
		array(
			"gid" => "listing-video", 
			"name" => "Listing video", 
			"max_size" => 1073741824, 
			"file_formats" => array("avi", "flv", "mkv", "asf", "mpeg", "mpg", "mov"), 
			"default_img" => "", 
			"upload_type" => "local", 
			"use_convert" => 1, 
			"use_thumbs" => 1, 
			"module" => "listings", 
			"model" => "Listings_model", 
			"method_status" => "video_callback", 
			"thumbs_settings" => array(array("gid"=>"small", "width"=>100, "height"=>70, "animated"=>0)), 
			"local_settings" => array("width"=>630, "height"=>400, "audio_freq"=>22050, "audio_brate"=>"64k", "video_brate"=>"300k", "video_rate"=>100),
		),
	);
	
	/**
	 * File uploads configuration
	 * @var array
	 */
	private $file_uploads = array(
		array(
			"gid" => "listing-file", 
			"name" => "Listing file", 
			"max_size" => 262144, 
			"name_format" => "generate", 
			"file_formats" => array("doc", "docx", "pdf", "ppt", "rtf", "text", "txt", "word", "xls", "xlsx", "csv", "xml", "bmp", "gif", "jpeg", "jpg", "png"),
		),
	);
	
	/**
	 * Notifications configuration
	 */
	private $notifications = array(
		"templates" => array(
			array("gid"=>"share_listing", "name"=>"Share listing", "vars" => array("user", "listing_id", "listing_name", "listing_owner", "listing_url", "message"), "content_type"=>"text"),
			array("gid"=>"listing_status_updated", "name"=>"Listing status updated", "vars"=>array("user", "listing", "status"), "content_type"=>"text"),
			array("gid"=>"listing_need_moderate", "name"=>"New listing awaiting moderation", "vars"=>array(), "content_type"=>"text"),
			array("gid"=>"listing_service_enabled", "name"=>"Listings paid service enabled", "vars"=>array('user', 'name', 'date'), "content_type"=>"text"),
			array("gid"=>"listing_service_expired", "name"=>"Listings paid service expired", "vars"=>array('user', 'name'), "content_type"=>"text"),
			array("gid"=>"listing_booking_request", "name"=>"New booking reguest", "vars"=>array('user', 'name', 'fname', 'sname'), "content_type"=>"text"),
			array("gid"=>"admin_booking_request", "name"=>"New booking reguest (for_admin)", "vars"=>array('user', 'name', 'owner'), "content_type"=>"text"),
			array("gid"=>"listing_booking_approve", "name"=>"Approve booking reguest", "vars"=>array('user', 'name', 'fname', 'sname'), "content_type"=>"text"),
			array("gid"=>"admin_booking_approve", "name"=>"Approve booking reguest (for admin)", "vars"=>array('user', 'name', 'owner'), "content_type"=>"text"),
			array("gid"=>"listing_booking_decline", "name"=>"Decline booking reguest", "vars"=>array('user', 'name', 'fname', 'sname'), "content_type"=>"text"),
			array("gid"=>"admin_booking_decline", "name"=>"Decline booking reguest (for admin)", "vars"=>array('user', 'name', 'owner'), "content_type"=>"text"),
		),
		"notifications" => array(
			array("gid"=>"share_listing", "template"=>"share_listing", "send_type"=>"simple"),
			array("gid"=>"listing_status_updated", "template"=>"listing_status_updated", "send_type"=>"simple"),
			array("gid"=>"listing_need_moderate", "template"=>"listing_need_moderate", "send_type"=>"simple"),
			array("gid"=>"listing_service_enabled", "template"=>"listing_service_enabled", "send_type"=>"simple"),
			array("gid"=>"listing_service_expired", "template"=>"listing_service_expired", "send_type"=>"simple"),
			array("gid"=>"listing_booking_request", "template"=>"listing_booking_request", "send_type"=>"simple"),
			array("gid"=>"admin_booking_request", "template"=>"admin_booking_request", "send_type"=>"simple"),
			array("gid"=>"listing_booking_approve", "template"=>"listing_booking_approve", "send_type"=>"simple"),
			array("gid"=>"admin_booking_approve", "template"=>"admin_booking_approve", "send_type"=>"simple"),
			array("gid"=>"listing_booking_decline", "template"=>"listing_booking_decline", "send_type"=>"simple"),
			array("gid"=>"admin_booking_decline", "template"=>"admin_booking_decline", "send_type"=>"simple"),
		),
	);

	/**
	 * Ausers configuration
	 * @var array
	 */
	private $ausers = array(
		array("module" => "listings", "method" => "index", "is_default" => 0),
		array("module" => "listings", "method" => "moderation", "is_default" => 1),
	);

	/**
	 * Dynamic blocks
	 * @var array
	 */
	private $dynamic_blocks = array(
		array(
			"gid" => "form_slider_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_form_slider_listings",
			"min_width" => 100,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"featured_980_440"), 
				array("gid"=>"featured_1600_440"), 
				array("gid"=>"latest_added_980_440"), 
				array("gid"=>"latest_added_1600_440"), 
				array("gid"=>"sale_980_440"), 
				array("gid"=>"sale_1600_440"), 
				array("gid"=>"rent_980_440"), 
				array("gid"=>"rent_1600_440"), 
			),
			"area" => array(
				"gid" => "index-page", 
				"params" => array("count"=>8),
				"view_str" => "latest_added_1600_440", 
				'width' => 100,
				"cache_time" => "600", 
				"sorter" => 1,
			),
		),
		array(
			"gid" => "slider_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_slider_listings",
			"min_width" => 70,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"featured_654_395"), 
				array("gid"=>"featured_980_440"), 
				array("gid"=>"latest_added_654_395"), 
				array("gid"=>"latest_added_980_440"), 
				array("gid"=>"sale_654_395"), 
				array("gid"=>"sale_980_440"), 
				array("gid"=>"rent_654_395"), 
				array("gid"=>"rent_980_440"),
			),
		),
		array(
			"gid" => "featured_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_featured_listings",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"gallery_small"), 
				array("gid"=>"gallery_big"), 
				array("gid"=>"scroller_small"), 
				array("gid"=>"scroller_big"),
			),
		),		
		array(
			"gid" => "latest_added_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_latest_added_listings",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"gallery_small"), 
				array("gid"=>"gallery_big"), 
				array("gid"=>"scroller_small"), 
				array("gid"=>"scroller_big"),
			),
		),
		array(
			"gid" => "sale_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_sale_listings",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"gallery_small"), 
				array("gid"=>"gallery_big"), 
				array("gid"=>"scroller_small"), 
				array("gid"=>"scroller_big"),
			),
		),
		array(
			"gid" => "buy_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_buy_listings",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"gallery_small"), 
				array("gid"=>"gallery_big"), 
				array("gid"=>"scroller_small"), 
				array("gid"=>"scroller_big"),
			),
		),
		array(
			"gid" => "rent_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_rent_listings",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"gallery_small"), 
				array("gid"=>"gallery_big"), 
				array("gid"=>"scroller_small"), 
				array("gid"=>"scroller_big"),
			),
			"area" => array(
				"gid" => "index-page", 
				"params" => array("count"=>4),
				"view_str" => "gallery_big", 
				'width' => 100,
				"cache_time" => "600", 
				"sorter" => 2,
			),
		),
		array(
			"gid" => "lease_listings_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_lease_listings",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(
				array("gid"=>"gallery_small"), 
				array("gid"=>"gallery_big"), 
				array("gid"=>"scroller_small"), 
				array("gid"=>"scroller_big"),
			),
		),
		array(
			"gid" => "listings_wish_lists_block",
			"module" => "listings",
			"model" => "Wish_list_model",
			"method" => "_dynamic_block_get_wish_lists",
			"min_width" => 30,
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default"=>8)),
			"views" => array(array("gid"=>"default")),
			"area" => array(
				"gid" => "index-page", 
				"params" => array("count"=>4),
				"view_str" => "default", 
				'width' => 100,
				"cache_time" => "600", 
				"sorter" => 5,
			),
		),
		array(
			"gid" => "sale_listings_categories_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_sale_categories_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
			"area" => array(
				"gid" => "index-page", 
				"params" => array(),
				"view_str" => "default", 
				'width' => 100,
				"cache_time" => "600", 
				"sorter" => 6,
			),
		),
		array(
			"gid" => "buy_listings_categories_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_buy_categories_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
		array(
			"gid" => "rent_listings_categories_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_rent_categories_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
		array(
			"gid" => "lease_listings_categories_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_lease_categories_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
		array(
			"gid" => "sale_listings_regions_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_sale_regions_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
		array(
			"gid" => "buy_listings_regions_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_buy_regions_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
		array(
			"gid" => "rent_listings_regions_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_rent_regions_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
		array(
			"gid" => "lease_listings_regions_block",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "_dynamic_block_get_lease_regions_search",
			"min_width" => 30,
			"params" => array(),
			"views" => array(
				array("gid"=>"default"),
			),
		),
	);
	
	/**
	 * Moderation configuration
	 * @var array
	 */
	private $moderation_types = array(
		array(
			"name" => "listings",
			"mtype" => "-1",
			"module" => "listings",
			"model" => "Listings_model",
			"check_badwords" => "1",
			"method_get_list" => "",
			"method_set_status" => "",
			"method_delete_object" => "",
			"allow_to_decline" => "0",
			"template_list_row" => "",
		),
		array(
			"name" => "share_listing",
			"mtype" => "-1",
			"module" => "listings",
			"model" => "Listings_model",
			"check_badwords" => "1",
			"method_get_list" => "",
			"method_set_status" => "",
			"method_delete_object" => "",
			"allow_to_decline" => "0",
			"template_list_row" => "",
		),
 	);
	
	/**
	 * Service configuration
	 * @var array
	 */
	private $services = array(
		"templates" => array(
			array(
				"gid" => "listings_featured_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_featured_activate",
				"callback_validate_method" => "service_featured_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "listings_lift_up_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_lift_up_activate",
				"callback_validate_method" => "service_lift_up_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "listings_lift_up_country_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_lift_up_country_activate",
				"callback_validate_method" => "service_lift_up_country_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "listings_lift_up_region_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_lift_up_region_activate",
				"callback_validate_method" => "service_lift_up_region_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "listings_lift_up_city_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_lift_up_city_activate",
				"callback_validate_method" => "service_lift_up_city_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "listings_highlight_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_highlight_activate",
				"callback_validate_method" => "service_highlight_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "listings_slide_show_template",
				"callback_module" => "listings",
				"callback_model" => "Listings_model",
				"callback_method" => "service_slide_show_activate",
				"callback_validate_method" => "service_slide_show_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_listing" => "hidden"),
				"moveable" => 0,
			),
		),
		"services" => array(
			array(
				"gid" => "listings_featured_service",
				"template_gid" => "listings_featured_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "listings_lift_up_service",
				"template_gid" => "listings_lift_up_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "listings_lift_up_country_service",
				"template_gid" => "listings_lift_up_country_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "listings_lift_up_region_service",
				"template_gid" => "listings_lift_up_region_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "listings_lift_up_city_service",
				"template_gid" => "listings_lift_up_city_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "listings_highlight_service",
				"template_gid" => "listings_highlight_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "listings_slide_show_service",
				"template_gid" => "listings_slide_show_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
		),
	);

	/**
	 * Subscription configuration
	 * @var array
	 */
	private $subscriptions = array(
		/*"types" => array(
			array("gid"=>"last_listings", "module"=>"listings", "model"=>"listings_model", "method"=>"get_last_listings"),
		),
		"subscriptions" => array(
			array("gid"=>"last_listings", "template" => "last_listings", "type"	=> "user", "content_type" => "last_listings", "scheduler" => array("type"=>1, "date_for_cron"=>0)),
		),*/
	);

	/**
	 * Export configuration
	 * @var array
	 */
	private $export_data = array(
		"module" => "listings",
		"model"	 => "Listings_export_model",
		"callback_get_fields" => "callback_get_fields",
		"callback_get_admin_form" => "callback_get_admin_form",
		"callback_get_user_form" => "callback_get_user_form",
		"callback_process_form" => "callback_process_form",
		"callback_export_data" => "callback_export_data",
		"sorter" => "1",
	);
	
	/**
	 * Import configuration
	 * @var array
	 */
	private $import_data = array(
		"module" => "listings",
		"model"	 => "Listings_import_model",
		"callback_get_fields" => "callback_get_fields",
		"callback_import_data" => "callback_import_data",
		"sorter" => "1",
	);
	
	/**
	 * Import configuration
	 * @var array
	 */
	private $site_map_data = array(
		"module_gid" => "listings",
		"model_name" => "Listings_model",
		"get_urls_method" => "get_sitemap_urls",
	);
	
	/**
	 * Import configuration
	 * @var array
	 */
	private $seo_data = array(
		"module_gid" => "listings",
		"model_name" => "Listings_model",
		"get_settings_method" => "get_seo_settings",
		"get_rewrite_vars_method" => "request_seo_rewrite",
		"get_sitemap_urls_method" => "get_sitemap_xml_urls",
	);
	
	/**
	 * Reviews configuration
	 */
	private $reviews = array(
		"reviews_fields" => array(
			"review_data" => array("type" => "TEXT", "null" => TRUE),
			"review_count" => array("type" => "smallint(5)", "null" => FALSE),
			"review_sorter"	=> array("type" => "decimal(5,3)", "null" => FALSE),
			"review_type" => array("type" => "varchar(20)", "null" => FALSE),
		),

		"reviews" => array(
			array("gid"=>"listings_object", "name"=>"Reviews in listings", "rate_type"=>"stars", "module"=>"listings", "model"=>"listings", "callback"=>"callback_reviews"),
		),

		"rate_types" => array(
			"stars" => array(
				"main" => array(1, 2, 3, 4, 5),
				"dop1" => array(1, 2, 3, 4, 5),
				"dop2" => array(1, 2, 3, 4, 5),
			),
			"hands" => array(
				"main" => array(1, 5),
				"dop1" => array(1, 5),
				"dop2" => array(1, 5),
			),
		),
	);
	
	/**
	 * Spam configuration
	 * @var array
	 */
	private $spam = array(
		array("gid"=>"listings_object", "form_type"=>"select_text", "send_mail"=>true, "status"=>true, "module"=>"listings", "model"=>"Listings_model", "callback"=>"spam_callback"),
	);
	
	/**
	 * Geomap configuration
	 * @var array
	 */
	private $geomap = array(
		array(
			"map_gid" => "googlemapsv3",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "listing_view",
			"use_type_selector" => 1,
			"use_panorama" => 1,
			"use_router" => 0,
			"use_searchbox" => 1,
			"use_search_radius" => 1,
			"use_search_auto" => 1,
			"use_show_details" => 1,
			"use_amenities" => 1,
			"amenities" => array("bank"),
		),
		array(
			"map_gid" => "yandexmapsv2",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "listing_view",
			"use_type_selector" => 1,
			"use_router" => 0,
			"use_searchbox" => 1,
			"use_tools" => 1,
			"use_clusterer" => 1,
			"use_click_zoom" => 1,
		),
		array(
			"map_gid" => "bingmapsv7",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "listing_view",
			"use_type_selector" => 1,
			"use_router" => 0,
			"use_searchbox" => 1,
		),
		array(
			"map_gid" => "googlemapsv3",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "listing_search",
			"zoom" => 5,
			"use_type_selector" => 0,
			"use_smart_zoom" => 1,
			"use_panorama" => 0,
			"use_router" => 0,
			"use_searchbox" => 0,
			"use_search_radius" => 1,
			"use_search_auto" => 1,
			"use_show_details" => 0,
			"use_amenities" => 0,
		),
		array(
			"map_gid" => "yandexmapsv2",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "listing_search",
			"zoom" => 5,
			"use_type_selector" => 0,
			"use_smart_zoom" => 1,
			"use_router" => 0,
			"use_searchbox" => 0,
			"use_tools" => 0,
			"use_clusterer" => 1,
			"use_click_zoom" => 1,
		),
		array(
			"map_gid" => "bingmapsv7",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "listing_search",
			"zoom" => 5,
			"use_type_selector" => 0,
			"use_smart_zoom" => 1,
			"use_router" => 0,
			"use_searchbox" => 0,
		),
	);
	
	/**
	 * Social networking configuration
	 */
	private $social_networking = array(
		array(
			'controller' => 'listings',
			'method' => 'view',
			'name' => 'View listing page',
			'data' => array(
				'like' => array('facebook'=>'on', 'vkontakte'=>'on', 'google'=>'on'),
			),
		),
		array(
			'controller' => 'listings',
			'method' => 'ajax_share_form',
			'name' => 'Share listing page',
			'data' => array(
				'share' => array('facebook'=>'on'),
			),
		),
	);
	
	/**
	 * Cronjobs configuration
	 */
	private $cronjobs = array(
		array(
			"name" => "Lift Up Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_lift_up_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Lift Up in country Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_lift_up_country_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Lift Up in region Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_lift_up_region_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Lift Up in city Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_lift_up_city_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Featured Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_featured_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Listings status section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_post_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Highlight Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_highlight_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Slide show Listings section clean",
			"module" => "listings",
			"model" => "Listings_model",
			"method" => "service_slide_show_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
		array(
			"name" => "Search booking cache clean",
			"module" => "listings",
			"model" => "Listings_booking_model",
			"method" => "search_booking_cron",
			"cron_tab" => "0 * * * *",
			"status" => "1",
		),
	);
	
	/**
	 * Fields depended of languages
	 */
	private $lang_dm_data = array(
		array(
			"module" => "listings",
			"model" => "Wish_list_model",
			"method_add" => "lang_dedicate_module_callback_add",
			"method_delete" => "lang_dedicate_module_callback_delete",
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Check system requirements of module
	 */
	public function _validate_requirements(){
		$result = array("data"=>array(), "result" => true);

		//check for Mbstring
		$good			= function_exists("mb_convert_encoding");
		$result["data"][] = array(
			"name" => "Mbstring extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;

		//check for iconv
		$good			= function_exists("iconv");
		$result["data"][] = array(
			"name" => "Iconv extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;
		return $result;
	}

	/**
	 * Install links to menu module
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("listings", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}
	}
	
	/**
	 * Install uploades
	 */
	public function install_uploads(){
		///// upload config
		$this->CI->load->model("uploads/models/Uploads_config_model");
		
		$watermark_ids = array();
		
		foreach((array)$this->uploads as $upload_data){
			$config_data = array(
				"gid" 			=> $upload_data["gid"],
				"name" 			=> $upload_data["name"],
				"min_height" 	=> isset($upload_data["min_height"]) ? $upload_data["min_height"] : 0,
				"min_width" 	=> isset($upload_data["min_width"]) ? $upload_data["min_width"] : 0,
				"max_height" 	=> $upload_data["max_height"],
				"max_width" 	=> $upload_data["max_width"],
				"max_size" 		=> $upload_data["max_size"],
				"name_format" 	=> $upload_data["name_format"],
				"file_formats" 	=> serialize((array)$upload_data["file_formats"]),
				"default_img" 	=> $upload_data["default_img"],
				"date_add" => date("Y-m-d H:i:s"),
			);
			$config_id = $this->CI->Uploads_config_model->save_config(null, $config_data);
		
			$wm_data = $this->CI->Uploads_config_model->get_watermark_by_gid("image-wm");
			$wm_id = isset($wm_data["id"])?$wm_data["id"]:0;
			
			foreach((array)$upload_data["thumbs"] as $thumb_gid => $thumb_data){
				if(isset($thumb_data["watermark"])){
					if(!isset($watermark_ids[$thumb_data["watermark"]])){
						$wm_data = $this->CI->Uploads_config_model->get_watermark_by_gid($thumb_data["watermark"]);
						$watermark_ids[$thumb_data["watermark"]] = isset($wm_data["id"])?$wm_data["id"]:0;
					}
					$watermark_id = $watermark_ids[$thumb_data["watermark"]];
				}else{
					$watermark_id = 0;
				}
				
				$thumb_data["config_id"] = $config_id;
				$thumb_data["prefix"] = $thumb_gid;
				$thumb_data["effect"] = "none";
				$thumb_data["watermark_id"] = $watermark_id;

				$validate_data = $this->CI->Uploads_config_model->validate_thumb(null, $thumb_data);
				if(!empty($validate_data["errors"])) continue;
				$this->CI->Uploads_config_model->save_thumb(null, $validate_data["data"]);
			}
		}
	}

	/**
	 * Uninstall uploads links
	 */
	public function deinstall_uploads(){
		$this->CI->load->model("uploads/models/Uploads_config_model");
		
		foreach((array)$this->uploads as $upload_data){
			$config_data = $this->CI->Uploads_config_model->get_config_by_gid($upload_data["gid"]);
			if(!empty($config_data["id"])){
				$this->CI->Uploads_config_model->delete_config($config_data["id"]);
			}
		}
	}
	
	/**
	 * Install upload gallery
	 */
	public function install_upload_gallery(){
		$this->CI->load->model("Upload_gallery_model");
		
		foreach((array)$this->upload_gallery as $gallery_data){
			$validate_data = $this->CI->Upload_gallery_model->validate_type(null, $gallery_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Upload_gallery_model->set_type(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Uninstall upload gallery
	 */
	public function deinstall_upload_gallery(){
		$this->CI->load->model("Upload_gallery_model");
		foreach((array)$this->upload_gallery as $gallery_data){
			$type = $this->CI->Upload_gallery_model->get_type_by_gid($gallery_data["gid"]);
			if(!$type) continue;
			$this->CI->Upload_gallery_model->delete_type($type["id"]);	
		}
	}
	
	/**
	 * Install links to video uploads
	 */
	public function install_video_uploads(){
		///// add video settings
		$this->CI->load->model("video_uploads/models/Video_uploads_config_model");
		foreach((array)$this->video_uploads as $config_data){
			$validate_data = $this->CI->Video_uploads_config_model->validate_config(null, $config_data);
			if(!empty($validate_data["errors"])) continue;
			$validate_data["data"]["gid"] = $config_data["gid"];
			$validate_data["data"]["module"] = "listings";
			$validate_data["data"]["model"] = "Listings_model";
			$validate_data["data"]["method_status"] = "video_callback";
			$this->CI->Video_uploads_config_model->save_config(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Uninstall video uploads
	 */
	public function deinstall_video_uploads(){
		///// delete video settings
		$this->CI->load->model("video_uploads/models/Video_uploads_config_model");
		foreach((array)$this->video_uploads as $config_data){
			$config_data = $this->CI->Video_uploads_config_model->get_config_by_gid($config_data["gid"]);
			if(empty($config_data["id"])) continue;
			$this->CI->Video_uploads_config_model->delete_config($config_data["id"]);
		}
	}

	/**
	 * Install links to file uploads
	 */
	public function install_file_uploads(){
		///// add file settings
		$this->CI->load->model("file_uploads/models/File_uploads_config_model");
		foreach((array)$this->file_uploads as $config_data){
			$validate_data = $this->CI->File_uploads_config_model->validate_config(null, $config_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->File_uploads_config_model->save_config(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Uninstall file uploads
	 */
	public function deinstall_file_uploads(){
		///// delete file settings
		$this->CI->load->model('file_uploads/models/File_uploads_config_model');
		foreach((array)$this->file_uploads as $config_data){
			$config_data = $this->CI->File_uploads_config_model->get_config_by_gid($config_data["gid"]);
			if(empty($config_data["id"])) continue;
			$this->CI->File_uploads_config_model->delete_config($config_data["id"]);
		}
	}
	
	/**
	 * Install links to notifications module
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"])) $template_data["vars"] = implode(",", $template_data["vars"]);
			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_data['gid']] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);
		}

		foreach((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				$template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				$templates_ids[$notification_data["template"]] = $template["id"];
			}
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
	
		$this->CI->Notifications_model->update_langs($this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs((array)$this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Uninstall links to notifications module
	 */
	public function deinstall_notifications(){
		////// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}
	
	/**
	 * Install link to subscriptions
	 */
	public function install_subscriptions() {
		$this->CI->load->model("Subscriptions_model");
		$this->CI->load->model("subscriptions/models/Subscriptions_types_model");
		$this->CI->load->model("notifications/models/Templates_model");

		/*foreach((array)$this->notifications["templates"] as $tpl){
			$template_data = array(
				"gid" => $tpl["gid"],
				"name" => $tpl["name"],
				"vars" => serialize($tpl["vars"]),
				"content_type" => $tpl["content_type"],
				"date_add" =>  date("Y-m-d H:i:s"),
				"date_update" => date("Y-m-d H:i:s"),
			);
			$this->CI->Templates_model->save_template(null, $template_data);
		}*/

		foreach((array)$this->subscriptions["types"] as $type) {
			$subscr_data = array(
				"gid" => $type["gid"],
				"module" => $type["module"],
				"model" => $type["model"],
				"method" => $type["method"],
			);
			$this->CI->Subscriptions_types_model->save_subscriptions_type(null, $subscr_data);
		}
		foreach((array)$this->subscriptions["subscriptions"] as $subscription) {
			$subscr_type = $this->CI->Subscriptions_types_model->get_subscriptions_type_by_gid($subscription["content_type"]);
			$subscr_template = $this->CI->Templates_model->get_template_by_gid($subscription["template"]);
			$subsc_data = array(
				"gid" => $subscription["gid"],
				"id_template" => $subscr_template["id"],
				"subscribe_type" => $subscription["type"],
				"id_content_type" => $subscr_type["id"],
				"scheduler" => $subscription["scheduler"],
			);
			$this->CI->Subscriptions_model->save_subscription(null, $subsc_data);
		}
	}

	/**
	 * Import languages for subscriptions
	 */
	public function install_subscriptions_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Subscriptions_model");
		$this->CI->load->model("Notifications_model");
		$no_data = false;

		/*$langs_file = $this->CI->Install_model->language_file_read("listings", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");$no_data = true;}
		$this->CI->Notifications_model->update_langs((array)$this->notifications, $langs_file, $langs_ids);*/

		$langs_file = $this->CI->Install_model->language_file_read("listings", "subscriptions", $langs_ids);
		if(!$langs_file){log_message("info", "Empty subscriptions langs data");$no_data = true;}
		$this->CI->Subscriptions_model->update_langs("listings", (array)$this->subscriptions["subscriptions"], $langs_file, $langs_ids);

		return !$no_data;
	}

	/**
	 * Export languages for subscriptions
	 */
	public function install_subscriptions_lang_export($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Subscriptions_model");
		$this->CI->load->model("Notifications_model");

		//$langs["notifications"] = $this->CI->Notifications_model->export_langs((array)$this->notifications, $langs_ids);
		$langs["subscriptions"] = $this->CI->Subscriptions_model->export_langs((array)$this->subscriptions["subscriptions"], $langs_ids);
		return $langs;
	}

	/**
	 * Uninstall link to subscriptions
	 */
	public function deinstall_subscriptions() {
		$this->CI->load->model("Subscriptions_model");
		$this->CI->load->model("subscriptions/models/Subscriptions_types_model");
		$this->CI->load->model("notifications/models/Templates_model");

		/*foreach((array)$this->notifications["templates"] as $tpl){
			$this->CI->Templates_model->delete_template_by_gid($tpl["gid"]);
		}*/
		foreach((array)$this->subscriptions["types"] as $type) {
			$this->CI->Subscriptions_types_model->delete_subscriptions_type_by_gid($type["gid"]);
		}
		foreach((array)$this->subscriptions["subscriptions"] as $subscription) {
			$this->CI->Subscriptions_model->delete_subscription_by_gid($subscription["gid"]);
		}
	}
	
	/**
	 * Install links to moderation module
	 */
	public function install_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$mtype['date_add'] = date("Y-m-d H:i:s");
			$this->CI->Moderation_type_model->save_type(null, $mtype);
		}
	}
	
	/**
	 * Import moderation languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('listings', 'moderation', $langs_ids);

		if(!$langs_file){log_message('info', 'Empty moderation langs data'); return false;}

		$this->CI->load->model('moderation/models/Moderation_type_model');
		$this->CI->Moderation_type_model->update_langs($this->moderation_types, $langs_file);
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('moderation/models/Moderation_type_model');
		return array('moderation' => $this->CI->Moderation_type_model->export_langs($this->moderation_types, $langs_ids));
	}
	
	/**
	 * Uninstall links to moderation module
	 */
	public function deinstall_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$type = $this->CI->Moderation_type_model->get_type_by_name($mtype["name"]);
			$this->CI->Moderation_type_model->delete_type($type['id']);
		}
	}
	
	/**
	 * Install links to ausers module
	 */
	public function install_ausers(){
		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		
		foreach((array)$this->ausers as $method_data){
			//$validate_data = $this->CI->Ausers_model->validate_method($method_data, true);
			$validate_data = array("errors"=>array(), "data"=>$method_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Ausers_model->save_method(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read("listings", "ausers", $langs_ids);
		if(!$langs_file){log_message("info", "Empty ausers langs data");return false;}

		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "listings";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method["method"]])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method["method"]]);
			}
		}
	}
	
	/**
	 * Export moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_export($langs_ids){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "listings";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method["method"]] = $method["langs"];
		}
		return array('ausers' => $return);
	}
	
	/**
	 * Uninstall links to ausers module
	 */
	public function deinstall_ausers(){
		$this->CI->load->model("Ausers_model");
		$params = array();
		$params["where"]["module"] = "listings";
		$this->CI->Ausers_model->delete_methods($params);
	}	
	
	/**
	 * Install links to field editor module
	 */
	public function install_field_editor(){
		$this->CI->load->model("Listings_model");
		$this->CI->load->model("Field_editor_model");
		$this->CI->load->model("field_editor/models/Field_editor_forms_model");
		
		$editor_types = array('residential_sale', 'commercial_sale', 'lot_and_land_sale', 'residential_rent', 'commercial_rent', 'lot_and_land_rent');
		
		foreach($editor_types as $editor_type_gid){
			include MODULEPATH . 'listings/install/listings_fields_'.$editor_type_gid.'_data.php';
			$this->CI->Field_editor_model->import_type_structure($editor_type_gid, $fe_sections, $fe_fields, $fe_forms);
			unset($fe_sections); unset($fe_fields); unset($fe_form);
		}	
	
		return true;
	}

	/**
	 * Import languages
	 * @param array $langs_ids
	 */
	public function install_field_editor_lang_update($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("listings", "field_editor", $langs_ids);
		if(!$langs_file){log_message('info', 'Empty field editor langs data');return false;}
		$this->CI->load->model('Field_editor_model');	
		$editor_types = array('residential_sale', 'commercial_sale', 'lot_and_land_sale', 'residential_rent', 'commercial_rent', 'lot_and_land_rent');	
		foreach($editor_types as $editor_type_gid){
			include MODULEPATH . 'listings/install/listings_fields_'.$editor_type_gid.'_data.php';
			$this->CI->Field_editor_model->initialize($editor_type_gid);
			$this->CI->Field_editor_model->update_sections_langs($fe_sections, $langs_file);
			$this->CI->Field_editor_model->update_fields_langs($editor_type_gid, $fe_fields, $langs_file);
		}
		
		return true;
	}
	
	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_field_editor_lang_export($langs_ids=null) {
		$this->CI->load->model('Field_editor_model');

		$editor_types = array('residential_sale', 'commercial_sale', 'lot_and_land_sale', 'residential_rent', 'commercial_rent', 'lot_and_land_rent');
		$sections = $fields = array();
		
		foreach($editor_types as $editor_type_gid){
			$this->CI->Field_editor_model->initialize($editor_type_gid);
			list($fe_sections, $fe_fields, $fe_forms) = $this->CI->Field_editor_model->export_type_structure($editor_type_gid, 'application/modules/listings/install/listings_fields_'.$editor_type_gid.'_data.php');
			$sections = array_merge($sections, $this->CI->Field_editor_model->export_sections_langs($fe_sections, $langs_ids));
			$fields = array_merge($fields, $this->CI->Field_editor_model->export_fields_langs($editor_type_gid, $fe_fields, $langs_ids));
		}

		return array('field_editor' => array_merge($sections, $fields));
	}

	/**
	 * Uninstall links to field editor module
	 */
	public function deinstall_field_editor(){
	    $this->CI->load->model("Listings_model");
	    $this->CI->load->model("Field_editor_model");
	    $this->CI->load->model("field_editor/models/field_editor_forms_model");

		$editor_types = array('residential_sale', 'commercial_sale', 'lot_and_land_sale', 'residential_rent', 'commercial_rent', 'lot_and_land_rent');	

	    foreach($editor_types as $property_type_gid){
		$this->CI->Field_editor_model->initialize($property_type_gid);

		list($fe_sections, $fe_fields, $fe_forms) = $this->CI->Field_editor_model->export_type_structure($property_type_gid, 'application/modules/listings/install/listings_fields_'.$editor_type_gid.'_data.php');
		
		foreach((array)$fe_forms as $form_gid=>$form_data){
		    if($form_data['data']["editor_type_gid"] != $property_type_gid) continue;
		    $this->CI->Field_editor_forms_model->delete_form_by_gid($form_data['data']['gid']);
		}

		foreach((array)$fe_fields as $field_gid=>$field_data){
		    if($field_data['data']["editor_type_gid"] != $property_type_gid) continue;
		    $this->CI->Field_editor_model->delete_field_by_gid($field_data['data']['gid']);
		}
		foreach((array)$fe_sections as $section_gid=>$section_data){
		    if($section_data['data']["editor_type_gid"] != $property_type_gid) continue;
		    $this->CI->Field_editor_model->delete_section_by_gid($section_data['data']['gid']);
		}
	    }
	}
	
	/**
	 * Install links to service module
	 */
	public function install_services(){
		$this->CI->load->model("Services_model");
		
		foreach((array)$this->services["templates"] as $template_data){
			$data_admin = $template_data["data_admin"];
			$data_user = $template_data["data_user"];
			$validate_data = $this->CI->Services_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Services_model->save_template(null, $validate_data["data"]);
		}	
		
		foreach((array)$this->services["services"] as $service_data){
			$validate_data = $this->CI->Services_model->validate_service(null, $service_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Services_model->save_service(null, $validate_data["data"]);
		}
	}

	/**
	 * Import services languages
	 * @param array $langs_ids
	 */
	public function install_services_lang_update($langs_ids=null){
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "services", $langs_ids);
		if(!$langs_file){log_message("info", "Empty services langs data");return false;}
		
		$services_data = array(
			"service" 	=> array(),
			"template" 	=> array(),
			"param"		=> array(),
		);
		
		$this->CI->load->model("Services_model");
		
		foreach((array)$this->services["templates"] as $template_data){
			$data_admin = isset($template_data["data_admin"]) ? array_keys($template_data["data_admin"]) : array();
			$data_user = isset($template_data["data_user"]) ? array_keys($template_data["data_user"]) : array();
			$services_data["template"][] = $template_data["gid"];
			$services_data["param"][$template_data["gid"]] = array_unique(array_merge($data_admin, $data_user));
		}
		
		foreach((array)$this->services["services"] as $service_data){
			$services_data["service"][] = $service_data["gid"];
		}

		$this->CI->Services_model->update_langs($services_data, $langs_file);
		return true;
	}

	/**
	 * Export services languages
	 * @param array $langs_ids
	 */
	public function install_services_lang_export($langs_ids=null){

		$services_data = array(
			"service" 	=> array(),
			"template" 	=> array(),
			"param"		=> array(),
		);

		$this->CI->load->model("Services_model");

		foreach((array)$this->services["templates"] as $template_data){
			$data_admin = isset($template_data["data_admin"]) ? array_keys($template_data["data_admin"]) : array();
			$data_user = isset($template_data["data_user"]) ? array_keys($template_data["data_user"]) : array();			
			$services_data["template"][] = $template_data["gid"];
			$services_data["param"][$template_data["gid"]] = array_merge($data_admin, $data_user);
		}
		
		foreach((array)$this->services["services"] as $service_data){
			$services_data["service"][] = $service_data["gid"];
		}
		
		return array("services" => $this->CI->Services_model->export_langs($services_data, $langs_ids));
	}

	
	/**
	 * Uninstall links to services module
	 */
	public function deinstall_services(){
		$this->CI->load->model("Services_model");
		
		foreach((array)$this->services["templates"] as $template_data){
			$this->CI->Services_model->delete_template_by_gid($template_data["gid"]);
		}
		
		foreach((array)$this->services["services"] as $service_data){
			$this->CI->Services_model->delete_service_by_gid($service_data["gid"]);
		}
	}
	
	/**
	 * Install links to dynamic blocks
	 */
	public function install_dynamic_blocks(){
		$this->CI->load->model("Dynamic_blocks_model");

		$area_ids = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			
			$validate_data = $this->CI->Dynamic_blocks_model->validate_block(null, $block_data);
			if(!empty($validate_data["errors"])) continue;
			$id_block = $this->CI->Dynamic_blocks_model->save_block(null, $validate_data["data"]);
		
			if(!isset($block_data["area"])) continue;
		
			if(!isset($area_ids[$block_data["area"]["gid"]])){
				$area = $this->CI->Dynamic_blocks_model->get_area_by_gid($block_data["area"]["gid"]);
				$area_ids[$block_data["area"]["gid"]] = $area["id"];
			}

			// index area
			$block_data["area"]["id_area"] = $area_ids[$block_data["area"]["gid"]];
			$block_data["area"]["id_block"] = $id_block;
			$block_data["area"]["params"] = serialize($block_data["area"]["params"]);
	
			$validate_data = $this->CI->Dynamic_blocks_model->validate_area_block($block_data["area"], true);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Dynamic_blocks_model->save_area_block(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Install languages for dynamic blocks
	 * @param array $langs_ids
	 */
	public function install_dynamic_blocks_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;	
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "dynamic_blocks", $langs_ids);
		if(!$langs_file){log_message("info", "Empty dynamic_blocks langs data");return false;}
		
		$this->CI->load->model("Dynamic_blocks_model");
		
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$this->CI->Dynamic_blocks_model->update_langs($data, $langs_file, $langs_ids);
	}
	
	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_dynamic_blocks_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Dynamic_blocks_model");
		$data = array();
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}		
		$langs = $this->CI->Dynamic_blocks_model->export_langs($data, $langs_ids);
		return array("dynamic_blocks" => $langs);
	}

	/**
	 * Unistall dynamic blocks
	 */
	public function deinstall_dynamic_blocks(){
		$this->CI->load->model("Dynamic_blocks_model");
		foreach((array)$this->dynamic_blocks as $block_data){
			$this->CI->Dynamic_blocks_model->delete_block_by_gid($block_data["gid"]);
		}
	}
	
	/**
	 * Install links to export
	 */
	public function install_export(){
		$this->CI->load->model("export/models/Export_module_model");
		
		$validate_data = $this->CI->Export_module_model->validate_module(null, (array)$this->export_data);
		if(!empty($validate_data["errors"])) return;
		$this->CI->Export_module_model->save_module(null, $validate_data["data"]);
	}
	
	/**
	 * Install languages
	 * @param array $langs_ids
	 */
	public function install_export_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "export", $langs_ids);
		if(!$langs_file){log_message("info", "Empty export langs data");return false;}
		
		$this->CI->load->model("export/models/Export_module_model");
		$this->CI->Export_module_model->update_lang(array("module_listings"), $langs_file, $langs_ids);
	}
	
	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_export_lang_export($langs_ids=null){
		$this->CI->load->model("export/models/Export_module_model");
		$langs = $this->CI->Export_module_model->export_lang(array("module_listings"), $langs_ids);
		return array("export" => $langs);
	}
	
	/**
	 * Unistall export
	 */
	public function deinstall_export(){
		$this->CI->load->model("export/models/Export_module_model");
		$this->CI->Export_module_model->remove_module("listings");		
	}
	
	/**
	 * Install links to import
	 */
	public function install_import(){
		$this->CI->load->model("import/models/Import_module_model");
		
		$validate_data = $this->CI->Import_module_model->validate_module(null, (array)$this->import_data);
		if(!empty($validate_data["errors"])) return;
		$this->CI->Import_module_model->save_module(null, $validate_data["data"]);
	}
	
	/**
	 * Install languages
	 * @param array $langs_ids
	 */
	public function install_import_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "import", $langs_ids);
		if(!$langs_file){log_message("info", "Empty import langs data");return false;}
		
		$this->CI->load->model("import/models/Import_module_model");
		$this->CI->Import_module_model->update_lang(array("module_listings"), $langs_file, $langs_ids);
	}
	
	/**
	 * Import languages
	 * @param array $langs_ids
	 */
	public function install_import_lang_export($langs_ids=null){
		$this->CI->load->model("import/models/Import_module_model");
		
		$langs = $this->CI->Import_module_model->export_lang(array("module_listings"), $langs_ids);
		return array("import" => $langs);
	}
	
	/**
	 * Unistall import
	 */
	public function deinstall_import(){
		$this->CI->load->model("import/models/Import_module_model");
		$this->CI->Import_module_model->remove_module("listings");		
	}

	/**
	 * Install links to site map
	 */
	public function install_site_map(){
		////// site_map
		$this->CI->load->model("Site_map_model");
		$this->CI->Site_map_model->set_sitemap_module("listings", $this->site_map_data);
	}
	
	/**
	 * Uninstall links to site map
	 */
	public function deinstall_site_map(){
		$this->CI->load->model("Site_map_model");
		$this->CI->Site_map_model->delete_sitemap_module("listings");
	}
	
	/**
	 * Install geomap links
	 */
	public function install_geomap(){
		//add geomap settings
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		foreach((array)$this->geomap as $settings){
			$map_gid = $settings["map_gid"];
			unset($settings["map_gid"]);
			
			$id_user = intval($settings["id_user"]);
			unset($settings["id_user"]);
			
			$id_object = intval($settings["id_object"]);
			unset($settings["id_object"]);
			
			$gid = $settings["gid"];
			unset($settings["gid"]);
			
			$validate_data = $this->CI->Geomap_settings_model->validate_settings($settings);
			if(!empty($validate_data["errors"])) continue;			
			$this->CI->Geomap_settings_model->save_settings($map_gid, $id_user, $id_object, $gid, $validate_data["data"]);
		}
	}
	
	/**
	 * Install languages
	 * @param array $langs_ids
	 */
	public function install_geomap_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "geomap", $langs_ids);
		if(!$langs_file){log_message("info", "Empty geomap langs data");return false;}
		
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		$gids = array();
		foreach((array)$this->geomap as $geomap_data){
			$gids[] = 'map_'.$geomap_data['gid'];
		}
		$this->CI->Geomap_settings_model->update_lang($gids, $langs_file, $langs_ids);
	}
	
	/**
	 * Import languages
	 * @param array $langs_ids
	 */
	public function install_geomap_lang_export($langs_ids=null){
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		$gids = array();
		foreach((array)$this->geomap as $geomap_data){
			$gids[] = 'map_'.$geomap_data['gid'];
		}
		$langs = $this->CI->Geomap_settings_model->export_lang($gids, $langs_ids);
		return array("geomap" => $langs);
	}
	
	/**
	 * Uninstall geomap links
	 */
	public function deinstall_geomap(){
		//add geomap settings
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		foreach((array)$this->geomap as $settings){
			$map_gid = $settings["map_gid"];
			unset($settings["data"]["map_gid"]);
			
			$id_user = $settings["id_user"];
			unset($settings["id_user"]);
			
			$id_object = $settings["id_object"];
			unset($settings["id_object"]);
			
			$gid = $settings["gid"];
			unset($settings["gid"]);
			
			$this->CI->Geomap_settings_model->delete_settings($map_gid, $id_user, $id_object, $gid);
		}
	}
	
	/**
	 * Install reviews links
	 */
	public function install_reviews(){
		
		$this->CI->load->model("Listings_model");
		
		// add reviews type
		$this->CI->load->model("reviews/models/Reviews_type_model");		
		
		$this->CI->Listings_model->install_reviews_fields((array)$this->reviews["reviews_fields"]);
	
		foreach((array)$this->reviews["reviews"] as $review_data){
			$validate_data = $this->CI->Reviews_type_model->validate_type(null, $review_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Reviews_type_model->save_type(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Install reviews languages
	 * @param array $langs_ids
	 */
	public function install_reviews_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Reviews_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "reviews", $langs_ids);
		if(!$langs_file){log_message("info", "Empty reviews langs data");return false;}
		
		foreach((array)$this->reviews["reviews"] as $review_data){
			$this->CI->Reviews_model->update_langs($review_data, $langs_file, $langs_ids);
		}
		
		foreach($langs_ids as $lang_id){
			foreach ((array)$this->reviews["rate_types"] as $type_gid=>$type_data){
				$types_data = array();
				foreach($type_data as $rate_type=>$votes){
					$votes_data = array();
					foreach($votes as $vote){
						$votes_data[$vote] = isset($langs_file[$type_gid.'_'.$rate_type."_votes_".$vote][$lang_id]) ?
							$langs_file[$type_gid.'_'.$rate_type."_votes_".$vote][$lang_id] : $vote;
					}
					$types_data[$rate_type] = array(
						"header" => $langs_file[$type_gid.'_'.$rate_type."_header"][$lang_id],
						"votes" => $votes_data,
					);
				}	
				$this->CI->Reviews_model->add_rate_type($type_gid, $types_data, $lang_id);
			}			
		}
		
		return true;
	}

	/**
	 * Export reviews languages
	 * @param array $langs_ids
	 */
	public function install_reviews_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Reviews_model");
		$langs = array();
		foreach((array)$this->reviews["reviews"] as $review_data){
			$langs = array_merge($langs, $this->CI->Reviews_model->export_langs($review_data['gid'], $langs_ids));
		}
		return array("reviews" => $langs);
	}

	/**
	 * Uninstall reviews links
	 */
	public function deinstall_reviews(){
		
		$this->CI->load->model("Listings_model");
		
		//add reviews type
		$this->CI->load->model("reviews/models/Reviews_type_model");

		foreach((array)$this->reviews["reviews"] as $review_data){
			$this->CI->Reviews_type_model->delete_type($review_data["gid"]);
		}
		
		$this->CI->Listings_model->deinstall_reviews_fields(array_keys((array)$this->reviews["reviews_fields"]));
	}
	
	/**
	 * Install spam links
	 */
	public function install_spam(){
		// add spam type
		$this->CI->load->model("spam/models/Spam_type_model");

		foreach((array)$this->spam as $spam_data){
			$validate_data = $this->CI->Spam_type_model->validate_type(null, $spam_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Spam_type_model->save_type(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import spam languages
	 * @param array $langs_ids
	 */
	public function install_spam_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$this->CI->load->model("spam/models/Spam_type_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("listings", "spam", $langs_ids);
		if(!$langs_file){log_message("info", "Empty spam langs data");return false;}
	
		$this->CI->Spam_type_model->update_langs($this->spam, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export spam languages
	 * @param array $langs_ids
	 */
	public function install_spam_lang_export($langs_ids=null){
		$this->CI->load->model("spam/models/Spam_type_model");
		$langs = $this->CI->Spam_type_model->export_langs((array)$this->spam, $langs_ids);
		return array("spam" => $langs);
	}
	
	/**
	 * Uninstall spam links
	 */
	public function deinstall_spam(){
		//add spam type
		$this->CI->load->model("spam/models/Spam_type_model");

		foreach((array)$this->spam as $spam_data){
			$this->CI->Spam_type_model->delete_type($spam_data["gid"]);
		}
	}
	
	/**
	 * Install banners links
	 */
	public function install_banners(){
		///// add banners module
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->Banner_group_model->set_module("listings", "Listings_model", "_banner_available_pages");
		$this->add_banners();
	}
	
	/**
	 * Import banners languages
	 */
	public function install_banners_lang_update(){
		$lang_ids = array_keys($this->CI->pg_language->languages);
		$lang_id = $this->CI->pg_language->get_default_lang_id();
 		$lang_data[$lang_id] = "Listings pages";
		$this->CI->pg_language->pages->set_string_langs("banners", "banners_group_listings_groups", $lang_data, $lang_ids);
	}

	/**
	 * Unistall banners links
	 */
	public function deinstall_banners(){
		// delete banners module
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->Banner_group_model->delete_module("listings");
		$this->remove_banners();
	}
	
	/**
	 * Add default banners
	 */
	public function add_banners(){
		$this->CI->load->model("Users_model");
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->load->model("banners/models/Banner_place_model");

		$group_attrs = array(
			"date_created" => date("Y-m-d H:i:s"),
			"date_modified" => date("Y-m-d H:i:s"),
			"price" => 1,
			"gid" => "listings_groups",
			"name" => "Listings pages",
		);
		$group_id = $this->CI->Banner_group_model->create_unique_group($group_attrs);
		$all_places = $this->CI->Banner_place_model->get_all_places();
		if($all_places){
			foreach($all_places as $key => $value){
				$this->CI->Banner_place_model->save_place_group($value["id"], $group_id);
			}
		}
		
		///add pages in group
		$this->CI->load->model("Listings_model");
		$pages = $this->CI->Listings_model->_banner_available_pages();
		
		if ($pages){
			foreach($pages  as $key => $value){
				$page_attrs = array(
					"group_id" => $group_id,
					"name" => $value["name"],
					"link" => $value["link"],
				);
				$this->CI->Banner_group_model->add_page($page_attrs);
			}
		}
	}	

	/**
	 * Remove banners
	 */
	public function remove_banners(){
		$this->CI->load->model("banners/models/Banner_group_model");
		$group_id = $this->CI->Banner_group_model->get_group_id_by_gid("listings_groups");
		$this->CI->Banner_group_model->delete($group_id);
	}
	
	/**
	 * Install links to social networking
	 */
	public function install_social_networking() {
		///// add social netorking page
		$this->CI->load->model('social_networking/models/Social_networking_pages_model');
		foreach((array)$this->social_networking as $social_networking_data){
			$validation_data = $this->CI->Social_networking_pages_model->validate_page(null, $social_networking_data);
			if(!empty($validation_data['errors'])) continue;
			$this->CI->Social_networking_pages_model->save_page(null, $validation_data['data']);
		}
	}
	
	/**
	 * Uninstall links to social networking
	 */
	public function deinstall_social_networking() {
		///// delete social netorking page
		$this->CI->load->model('social_networking/models/Social_networking_pages_model');
		$this->CI->Social_networking_pages_model->delete_pages_by_controller('listings');
	}
	
	/**
	 * Install links to cronjobs
	 */
	public function install_cronjob(){
		////// add lift up cronjob
		$this->CI->load->model('Cronjob_model');
		foreach((array)$this->cronjobs as $cron_data){
			$validation_data = $this->CI->Cronjob_model->validate_cron(null, $cron_data);
			if(!empty($validation_data['errors'])) continue;
			$this->CI->Cronjob_model->save_cron(null, $validation_data['data']);
		}
	}
	
	/**
	 * Uninstall links to cronjobs
	 */
	public function deinstall_cronjob(){
		$this->CI->load->model('Cronjob_model');
		$cron_data = array();
		$cron_data["where"]["module"] = "listings";
		$this->CI->Cronjob_model->delete_cron_by_param($cron_data);
	}
	
	/**
	 * Install fields
	 */
	public function _prepare_installing(){
		$this->CI->load->model("listings/models/Wish_list_model");
		foreach($this->CI->pg_language->languages as $lang_id => $value){
			$this->CI->Wish_list_model->lang_dedicate_module_callback_add($lang_id);
		}
	}
	
	/**
	 * Install model
	 */
	function _arbitrary_installing(){
		///// add entries for lang data updates
		foreach($this->lang_dm_data as $lang_dm_data){
			$this->CI->pg_language->add_dedicate_modules_entry($lang_dm_data);
		}

		///// SEO
		$this->CI->pg_seo->set_seo_module("listings", $this->seo_data);
		
		// add seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-index-][tpl:1:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'index', $data);
		
		$xml_data['listings']['index'] = $this->CI->pg_seo->url_template_transform('listings', 'index', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-by-][tpl:1:category:literal:all][text:-][tpl:2:property:literal:all][text:-][tpl:3:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'category', $data);
		
		$xml_data['listings']['category'] = $this->CI->pg_seo->url_template_transform('listings', 'category', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-in-][tpl:1:country:literal:all][text:-][tpl:2:region:literal:all][text:-][tpl:3:city:literal:all][text:-][tpl:4:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'location', $data);
		
		$xml_data['listings']['location'] = $this->CI->pg_seo->url_template_transform('listings', 'location', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-from-][opt:user:literal:user][text:-][tpl:1:id_user:numeric:0][text:-][tpl:2:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'user', $data);
		
		$xml_data['listings']['user'] = $this->CI->pg_seo->url_template_transform('listings', 'user', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-search-][tpl:2:operation_type:literal:all][text:-][tpl:1:keyword:literal:empty]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'search', $data);
		
		$xml_data['listings']['search'] = $this->CI->pg_seo->url_template_transform('listings', 'search', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-by-open_house-][tpl:1:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'open_house', $data);
		
		$xml_data['listings']['open_house'] = $this->CI->pg_seo->url_template_transform('listings', 'open_house', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-by-privates-][tpl:1:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'privates', $data);
		
		$xml_data['listings']['privates'] = $this->CI->pg_seo->url_template_transform('listings', 'privates', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-by-agents-][tpl:1:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'agents', $data);
		
		$xml_data['listings']['agents'] = $this->CI->pg_seo->url_template_transform('listings', 'agents', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listings-by-discount-][tpl:1:operation_type:literal:all]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'discount', $data);
		
		$xml_data['listings']['discount'] = $this->CI->pg_seo->url_template_transform('listings', 'discount', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:listing-][opt:property:literal:all][text:-in-][opt:city:literal:all][text:-id-][tpl:1:id:numeric:0][text:-][tpl:2:section:literal:overview][text:-operation-][opt:operation_type:literal:no][text:-][tpl:3:pdf:literal:no]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'view', $data);
		
		$xml_data['listings']['view'] = $this->CI->pg_seo->url_template_transform('listings', 'view', $data["url_template"], 'base', 'xml');

		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:recommended-properties]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'wish_lists', $data);
		
		$xml_data['listings']['wish_lists'] = $this->CI->pg_seo->url_template_transform('listings', 'wish_lists', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '[text:recommended-properties]',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'wish_list', $data);

		$xml_data['listings']['wish_list'] = $this->CI->pg_seo->url_template_transform('listings', 'wish_list', $data["url_template"], 'base', 'xml');

		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
	
		///// DEMO
		if(!INSTALL_DONE){
			$this->add_demo_content();
		}
	}
	
	/**
	 * Import module languages
	 */
	public function _arbitrary_lang_install($langs_ids=null){
		
	}
	
	/**
	 * Export module languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		return array();
	}
	
	/**
	 * Uninstall module
	 */
	function _arbitrary_deinstalling(){
		// add seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'index', $data);
		
		$xml_data['listings']['index'] = $this->CI->pg_seo->url_template_transform('listings', 'index', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'category', $data);
		
		$xml_data['listings']['category'] = $this->CI->pg_seo->url_template_transform('listings', 'category', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'location', $data);
		
		$xml_data['listings']['location'] = $this->CI->pg_seo->url_template_transform('listings', 'location', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'user', $data);
		
		$xml_data['listings']['user'] = $this->CI->pg_seo->url_template_transform('listings', 'user', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'search', $data);
		
		$xml_data['listings']['search'] = $this->CI->pg_seo->url_template_transform('listings', 'search', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'open_house', $data);
		
		$xml_data['listings']['open_house'] = $this->CI->pg_seo->url_template_transform('listings', 'open_house', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'privates', $data);
		
		$xml_data['listings']['privates'] = $this->CI->pg_seo->url_template_transform('listings', 'privates', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'agents', $data);
		
		$xml_data['listings']['agents'] = $this->CI->pg_seo->url_template_transform('listings', 'agents', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'discount', $data);
		
		$xml_data['listings']['discount'] = $this->CI->pg_seo->url_template_transform('listings', 'discount', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);
		$this->CI->pg_seo->set_settings('user', 'listings', 'view', $data);

		$xml_data['listings']['view'] = $this->CI->pg_seo->url_template_transform('listings', 'view', $data["url_template"], 'base', 'xml');

		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'wish_lists', $data);
		
		$xml_data['listings']['wish_lists'] = $this->CI->pg_seo->url_template_transform('listings', 'wish_lists', $data["url_template"], 'base', 'xml');
		
		$data = array(
			"default_title" => 1,
			"default_keyword" => 1,
			"default_description" => 1,
			"default_header" => 1,
			"title" => '',
			"keyword" => '',
			"description" => '',
			"header" => '',
			"url_template" => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'listings', 'wish_list', $data);

		$xml_data['listings']['wish_list'] = $this->CI->pg_seo->url_template_transform('listings', 'wish_list', $data["url_template"], 'base', 'xml');

		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
		
		$this->CI->pg_seo->delete_seo_module("listings");
		
		/// delete entries in dedicate modules
		$lang_dm_data["where"] = array(
			"module" => "reviews",
			"model" => "Reviews_type_model",
		);
		foreach($this->lang_dm_data as $lang_dm_data){
			$this->CI->pg_language->delete_dedicate_modules_entry(array('where'=>$lang_dm_data));
		}
	}
	
	/**
	 * Install demo content
	 */
	function add_demo_content(){
		$this->CI->load->model("listings/models/Listings_import_model");
		
		include MODULEPATH."listings/install/demo_content.php";

		foreach((array)$demo_content as $listing_gid=>$listing_data){
			if(!in_array($listing_data['id_type'], $this->operation_types)) continue;
			$return = $this->CI->Listings_import_model->callback_import_data($listing_data, (array)$relations);
		}

		if($this->CI->pg_module->is_module_installed("upload_gallery")){
			$this->CI->load->model("Listings_model");
			$this->CI->load->model("Upload_gallery_model");
		
			$gallery_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->CI->Listings_model->gallery_type);	

			foreach((array)$uploads as $upload){
				if(!in_array($demo_content['realty_'.$upload['object_id']]['id_type'], $this->operation_types)) continue;
				
				$upload["type_id"] = $gallery_type["id"];
				
				if(isset($upload["id"])){
					if($this->CI->Upload_gallery_model->get_file_by_id($upload["id"])){
						continue;
					}
				}
				
				$validate_data = $this->CI->Upload_gallery_model->validate_file_data(null, $gallery_type['id'], $upload);
				if(!empty($validate_data['errors'])) continue;
				$validate_data['data'] = array_merge($upload, $validate_data['data']);
			
				$ret = $this->CI->Upload_gallery_model->save_file_data(null, $validate_data['data'], "", false);
				
				$params = array();
				$params["where"]["type_id"] = $gallery_type["id"];
				$params["where"]["object_id"] = $upload["object_id"];
				$params["where"]["status"] = 1;
		
				$photos = $this->CI->Upload_gallery_model->get_files_by_param($params);
	
				$post_data = array('photo_count'=>count($photos));
				$validate_data = $this->CI->Listings_model->validate_listing($upload["object_id"], $post_data);
				if(!empty($validate_data['errors'])) continue;
				$this->CI->Listings_model->save_listing($upload["object_id"], $validate_data["data"]);
			}
			
			$vtour_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->CI->Listings_model->virtual_tour_type);	

			foreach((array)$vtours as $vtour){
				if(!in_array($demo_content['realty_'.$vtour['object_id']]['id_type'], $this->operation_types)) continue;
				
				$vtour["type_id"] = $vtour_type["id"];
				
				if(isset($vtour["id"])){
					if($this->CI->Upload_gallery_model->get_file_by_id($vtour["id"])){
						continue;
					}
				}
				
				$validate_data = $this->CI->Upload_gallery_model->validate_file_data(null, $vtour_type['id'], $vtour);
				if(!empty($validate_data['errors'])) continue;
				$validate_data['data'] = array_merge($vtour, $validate_data['data']);
			
				$ret = $this->CI->Upload_gallery_model->save_file_data(null, $validate_data['data'], "", false);
				
				$params = array();
				$params["where"]["type_id"] = $vtour_type["id"];
				$params["where"]["object_id"] = $vtour["object_id"];
				$params["where"]["status"] = 1;
		
				$panoramas = $this->CI->Upload_gallery_model->get_files_by_param($params);
	
				$post_data = array('is_vtour'=>count($panoramas) > 0);
				$validate_data = $this->CI->Listings_model->validate_listing($vtour["object_id"], $post_data);
				if(!empty($validate_data['errors'])) continue;
				$this->CI->Listings_model->save_listing($vtour["object_id"], $validate_data["data"]);
			}
		}
		
		$this->CI->load->model('listings/models/Wish_list_model');
		foreach((array)$wish_lists as $wish_list){
			$data = array('id'=> $wish_list['id']);
			foreach($this->CI->pg_language->languages as $id=>$value){
				if(isset($wish_list["name_".$value["code"]])){
					$data["name_".$value["id"]] = $wish_list['name_'.$value["code"]];
				}else{
					$data["name_".$value["id"]] = $wish_list['name'];
				}
			}
			$validate_data = $this->CI->Wish_list_model->validate_wish_list($wish_list['id'], $data);
			if(!empty($validate_data['errors'])) continue;
			$this->CI->Wish_list_model->save_wish_list($wish_list['id'], $validate_data['data']);
		}
		
		if($this->CI->pg_module->is_module_installed("reviews")){
			$this->CI->load->model('Reviews_model');
			foreach((array)$reviews as $review){
				$validate_data = $this->CI->Reviews_model->validate_review(null, $review);
				if(!empty($validate_data['errors'])) continue;
				$this->CI->Reviews_model->save_review(null, $validate_data['data']);
			}
		}

		return true;
	}
}
