	<div class="photo-info-area">
		<div class="photo-area">
			<img src="{$panorama.media.thumbs.200_200}" hspace="3" id="listing_photo_{$panorama.id}" class="panorama" data-url="{$panorama.media.url}" data-file="{$panorama.media.url|str_replace:'':$panorama.media.thumbs.620_400}" data-width="{$panorama.settings.width}" data-height="{$panorama.settings.height}" data-comment="{$panorama.comment|escape}">
			<div class="photo-info">
				<div class="panel">
					{if $panorama.status}
					<font class="stat-active">{l i='panorama_active' gid='listings'}</font>
					{else}
					<font class="stat-moder">{l i='panorama_moderate' gid='listings'}</font>
					{/if}
					{if $panorama.comment}<br>{$panorama.comment|truncate:50:'...':true}{/if}
				</div>
				<div class="background"></div>
			</div>
		</div>
		<div class="action" onclick="javascript: vtourUpload.open_edit_form({$panorama.id});">
			<div class="btn-link"><ins class="with-icon w i-edit no-hover"></ins></div>
			<div class="background"></div>
		</div>
		<div class="canvas hide"></div>
		<div class="clr"></div>
	</div>
