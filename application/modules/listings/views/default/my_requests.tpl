{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_my_requests' gid='listings'} (<span id="requests_count">{$requests_count_sum}</span>)</h1>

		<div class="tabs tab-size-15 noPrint">
			<ul id="requests_sections">
				<li id="m_wait" sgid="wait" class="{if $status eq 'wait'}active{/if}"><a href="{$site_url}listings/requests/wait">{l i='booking_status_wait' gid='listings'} (<span id="section_wait">{$requests_count.wait}</span>)</a></li>
				<li id="m_approved" sgid="approve" class="{if $status eq 'approve'}active{/if}"><a href="{$site_url}listings/requests/approve">{l i='booking_status_approved' gid='listings'} (<span id="section_approve">{$requests_count.approve}</span>)</a></li>
				<li id="m_declined" sgid="decline" class="{if $status eq 'decline'}active{/if}"><a href="{$site_url}listings/requests/decline">{l i='booking_status_declined' gid='listings'} (<span id="section_decline">{$requests_count.decline}</span>)</a></li>
			</ul>
		</div>
		
		<div id="requests_block">{$block}</div>
		
		{js module=listings file='listings-order.js'}
		{js module=listings file='listings-list.js'}
		<script>{literal}
			var requests;
			$(function(){
				new listingsList({
					siteUrl: '{/literal}{$site_url}{literal}',
					listAjaxUrl: '{/literal}listings/ajax_requests{literal}',
					sectionId: 'requests_sections',
					listBlockId: 'requests_block',
					operationType: '{/literal}{$status}{literal}',
					order: '{/literal}{$order}{literal}',
					orderDirection: '{/literal}{$order_direction}{literal}',
					page: {/literal}{$page_data.cur_page}{literal},
					tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
				});
				requests = new listingsOrder({
					siteUrl: '{/literal}{$site_url}{literal}',
					blockId: 'requests_block',
					sectionId: 'requests_sections',
					waitCnt: {/literal}{$requests_count.wait}{literal},
					approveCnt: {/literal}{$requests_count.approve}{literal},
					declineCnt: {/literal}{$requests_count.decline}{literal},
				});
			});
		{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
