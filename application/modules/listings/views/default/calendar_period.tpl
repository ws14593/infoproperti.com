	<tr id="period{$period.id}">
		<td>
			{switch from=$listing.price_period}
				{case value='1'}
					{$period.date_start|date_format:$page_data.date_format} &mdash; {$period.date_end|date_format:$page_data.date_format}
				{case value='2'}
					{ld_option i='month-names' gid='start' option=$period.date_start|date_format:'%m'}
					{$period.date_start|date_format:'%Y'}
					&mdash; 
					{ld_option i='month-names' gid='start' option=$period.date_end|date_format:'%m'}
					{$period.date_end|date_format:'%Y'}
			{/switch}
		</td>
		<td>{if $period.guests_str}{$period.guests_str}{else}-{/if}</td>
		<td>
			{if $period.status eq 'open'}
				{if $period.price}{$period.price}{else}{l i='text_booking_price_unknown' gid='listings'}{/if}
			{else}
				&nbsp;
			{/if}
		</td>
		<td title="{$period.comment|escape}">{if $period.comment}{$period.comment|truncate:50}{else}<span class="gray_italic">{l i='no_information' gid='listings'}</span>{/if}</td>
		{if $edit}
		<td>
			<a href="{$site_url}listings/period_delete/{$period.id}" id="period_delete_{$period.id}" class="btn-link fright" alt="{l i='btn_delete' gid='start' type='button'}" title="{l i='btn_delete' gid='start' type='button'}"><ins class="with-icon i-delete"></ins></a>
			<a href="{$site_url}listings/period_edit/{$period.id}" id="period_edit_{$period.id}" class="btn-link fright" alt="{l i='btn_edit' gid='start' type='button'}" title="{l i='btn_edit' gid='start' type='button'}"><ins class="with-icon i-edit"></ins></a>
			<script>{literal}
				var period_{/literal}{$period.id}{literal};
				$(function(){
					period_{/literal}{$period.id}{literal} = new bookingForm({
						siteUrl: '{/literal}{$site_url}{literal}',
						listingId: '{/literal}{$period.id_listing}{literal}',
						periodId: '{/literal}{$period.id}{literal}',
						bookingBtn: 'period_edit_{/literal}{$period.id}{literal}',
						deleteBtn: 'period_delete_{/literal}{$period.id}{literal}',
						cFormId: '{/literal}{if $period.status eq 'open'}period_form{else}order_form{/if}{literal}',
						urlGetForm: '{/literal}listings/{if $period.status eq 'open'}ajax_period_form{else}ajax_order_form{/if}/{literal}',
						urlSaveForm: '{/literal}listings/{if $period.status eq 'open'}ajax_save_period{else}ajax_save_order{/if}/{literal}',
						urlDeletePeriod: '{/literal}listings/{if $period.status eq 'open'}ajax_period_delete{else}ajax_order_delete{/if}/{literal}',
						note_delete: '{/literal}{l i='note_period_delete' gid='listings' type='js'}{literal}',
						{/literal}{if $rand}calendar: listings_calendar{$rand},{/if}{literal}
						successCallback: function(id, data, calendar){
							$('#period{/literal}{$period.id}{literal}').replaceWith(data);
							period_{/literal}{$period.id}{literal}.set_calendar(calendar);
						},
						deleteCallback: function(){
							var periods = $('#{/literal}{if $period.status eq 'open'}listing_periods{else}listing_booked{/if}{literal}');
							if(periods.find('tr').length != 1) return;
							periods.find('tr').after(
								'<tr><td colspan="{/literal}{if $edit}5{else}4{/if}{literal}" class="center gray_italic">{/literal}{l i='no_periods' gid='listings'}{literal}</td></tr>'
							);
						},
					});
				});
			{/literal}</script>
		</td>
		{/if}
	</tr>
	
