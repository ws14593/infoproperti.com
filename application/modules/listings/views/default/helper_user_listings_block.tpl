<div class="user_listings clearfix">
<!-- user listing starts -->
	<h2>{l i='header_listings' gid='listings'}</h2>
	{foreach item=item from=$listings}
	<div class="listing-block {if $item.is_highlight}highlight{/if}">
		<div id="item-block-{$item.id}" class="item listing">
			<h3 class="listing-heading"><a href="{seolink module='listings' method='view' data=$item}">{$item.output_name|truncate:40}</a></h3>
			<div class="image">
				<a href="{seolink module='listings' method='view' data=$item}">
					<img src="{$item.media.photo.thumbs.big}" alt="{$item.output_name|truncate:40}" title="{$item.output_name|escape}">
					{if $item.photo_count || $item.is_vtour}
					<div class="photo-info">
						<div class="panel">
							{if $item.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$item.photo_count}</span>{/if}
							{if $item.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
						</div>
						<div class="background"></div>
					</div>
					{/if}
				</a>
			</div>
			<div class="listing-content">
				<div class="body">
					{l i='no_information' gid='start' assign='no_info_str'}
					<h3 class="price-highlight">{block name=listing_price_block module='listings' data=$item}</h3>
					<div class="t-1">
                        {$item.property_type_str} {$item.operation_type_str}<br />
						{l i='field_square' gid='listings'} : {$item.square_output|truncate:30}
                        <ul class="icon-stats">
               			{foreach item=residen from=$residential}

                            {if $item.id == $residen.id_listing}
                            	
                                {if $residen.fe_bd_rooms_1 != '0'}	 
                                	<li class="listing-icon bedroom"> : {$residen.fe_bd_rooms_1}</li>
                                {/if}
                                
                                {if $residen.fe_bth_rooms_1 != '0' }
                                	<li class="listing-icon bathroom"> : {$residen.fe_bth_rooms_1}</li>
                                {/if}
                                
                                {if $residen.fe_garages_1 != '0' }
                                	<li class="listing-icon garages"> : {$residen.fe_garages_1}</li>
                                {/if}
                            {/if}
      					{/foreach}
                        
						
                        {foreach item=commer from=$commercial}
                            {if $item.id == $commer.id_listing}
                                {if $commer.fe_year_2 != '0'}	 
                                	<li class="listing-icon year">Year : {$commer.fe_year_2}</li>
                                {/if}
                                
                                {if $commer.fe_foundation_2 != '0' }
                                	<li class="listing-icon foundation">Foundation : {$commer.fe_foundation_2}</li>
                                {/if}
                            {/if}
      					{/foreach}
                        </ul> 
                        
						{if $item.date_open|strtotime >= $tstamp}{l i='text_date_open_status' gid='listings'}{/if}
						<?php /*** Enable these lines below to show the open house's date and time
						{if $item.date_open|strtotime > 0}
							{$item.date_open|date_format:$page_data.date_format} 
							{if $item.date_open_begin}{ld_option i='dayhour-names' gid='start' option=$item.date_open_begin}{/if}
							{if $item.date_open_end}{if $item.date_open_begin} - {/if}{ld_option i='dayhour-names' gid='start' option=$item.date_open_end}{/if}
						{/if}
						* */?>
					</div>
					<div class="t-2">
                    	<?php /* lines below for displaying modified listing by date */ ?>
						{capture assign='date_modified'}{strip}
						{$item.date_modified|date_format:$page_data.date_format} <?php /* replaced the $listing.date_modified with $item.date_modified */ ?>
						{/strip}{/capture} 
						<span>{l i='field_date_modified' gid='listings'}: {if $date_modified}{$date_modified}{else}{l i='inactive_listing' gid='listings'}{/if}</span><br>
						<span>{l i='field_views' gid='listings'}: {$item.views}</span>
                    </div>
					
				</div>
			</div>
			<div class="clr"></div>
			{if $item.headline}<p class="headline" title="{$item.headline}">{$item.headline|truncate:50}</p>{/if}
					<div class="a-link clearfix">
						{block name='save_listing_block' module='listings' listing=$item template='link' separator=''}
						{block name='listings_booking_block' module='listings' listing=$item template='link' show_link=1 show_price=1 separator='' no_save=1}
						<a href="{seolink module='listings' method='view' data=$item}">{l i='link_details' gid='listings'}</a>
						<span class="prioritas">Prioritas</span>
					</div>
		</div>
	</div>
	{/foreach}
	
</div>
{if $listings_count}<div class="more-listings"><a href="{seolink module='listings' method='user' id_user=$user_id user=$user_name}">{l i='link_all_user_listings' gid='listings'} ({$listings_count})</a></div>{/if}
