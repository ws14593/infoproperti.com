{if $listings}
	<!-- inner pg result in Search page-->
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	
	<!-- MOD inner pg result for comparison -->
	<div id="comp-container" class="hidden clearfix">
		<button class="close-container" title="Tutup">x</button>
		<h3 class="msg-compare">There is 0 product selected</h3>
		<a class="compare-btn" href="#" target="_blank">Bandingkan</a>
		<!--<a class="compare-btn btn-close" href="#">Close</a>-->
	</div>
    <!-- End MOD : Inner pg result for comparison -->
{/if}

<div>
    {if $msg_word_1}        
        <span class="search-msg">{$msg_word_1}</span>
    {/if}
        
    {foreach item=item from=$listings}
		<div class="listing-block {if $item.is_highlight}highlight{/if}">
			<div id="item-block-{$item.id}" class="item listing {if $item.is_highlight}highlight{/if}">
				<h3 class="listing-heading search-result-entry-heading">
                	<a href="{seolink module='listings' method='view' data=$item}" title="{$item.output_name|escape}">{$item.output_name|truncate:50}</a>
                <div style="position:relative; float:right; margin-top:-5px; display:block;">
                {if $item.review_sorter neq 0}                
                {depends module=reviews}
                    {block name=get_rate_block module=reviews rating_data_main=$item.review_sorter type_gid='listings_object' template='normal' read_only='true'}
				{/depends}
                {/if}
                </div>                    
                </h3>
				<div class="image">
					<a href="{seolink module='listings' method='view' data=$item}">
						{if $animate_available && $item.is_slide_show}
							<img src="{$item.media.photo.thumbs.middle_anim}" title="{$item.output_name|escape}">
						{else}
							<img src="{$item.media.photo.thumbs.middle}" title="{$item.output_name|escape}">
						{/if}
                        
						{if $item.photo_count || $item.is_vtour}
							<div class="photo-info">
								<div class="panel">
									{if $item.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$item.photo_count}</span>{/if}
									{if $item.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
								</div>
								<div class="background"></div>
							</div>
						{/if}
					</a>
				</div>
		
				{capture assign='status_info'}{strip}
					{if $item.is_lift_up || $location_filter eq 'country' && $item.is_lift_up_country || $location_filter eq 'region' && ($item.is_lift_up_country || $item.is_lift_up_region) || 
					$location_filter eq 'city' && ($item.is_lift_up_country || $item.is_lift_up_region || $item.is_lift_up_city) || $sort_data.order ne 'default' && ($item.is_lift_up ||
                    $item.is_lift_up_country || $item.is_lift_up_region || $item.is_lift_up_city) }
						{l i='status_lift_up' gid='listings'}
					{elseif $item.is_featured}
						{l i='status_featured' gid='listings'}
					{/if}
				{/strip}{/capture}
				
				<div class="body clearfix">
					<div class="prop-info">
						<h3 class="price-highlight">{block name=listing_price_block module='listings' data=$item}</h3>
						<div class="t-1">
							{$item.property_type_str} {$item.operation_type_str}
                            <br>
							{l i='field_square' gid='listings'} : {$item.square_output|truncate: 30}
							<ul class="icon-stats">
							{foreach item=residen from=$residential}
								{if $item.id == $residen.id_listing}
									{if $residen.fe_bd_rooms_1 != '0'}	 
										<li class="listing-icon bedroom"> : {$residen.fe_bd_rooms_1}</li>
									{/if}
									
									{if $residen.fe_bth_rooms_1 != '0' }
										<li class="listing-icon bathroom"> : {$residen.fe_bth_rooms_1}</li>
									{/if}
									
									{if $residen.fe_garages_1 != '0' }
										<li class="listing-icon garages"> : {$residen.fe_garages_1}</li>
									{/if}
								{/if}
							{/foreach}
							
							{foreach item=commer from=$commercial}
								{if $item.id == $commer.id_listing}
									{if $commer.fe_year_2 != '0'}	 
										<li class="listing-icon year">Year : {$commer.fe_year_2}</li>
									{/if}
									
									{if $commer.fe_foundation_2 != '0' }
										<li class="listing-icon foundation">Foundation : {$commer.fe_foundation_2}</li>
									{/if}
								{/if}
							{/foreach}
							</ul>
														
							{if $item.date_open|strtotime >= $tstamp}{l i='text_date_open_status' gid='listings'}{/if}
                            <!-- Enable these lines below to show the open house's date and time
							{if $item.date_open|strtotime > 0}
                            	{$item.date_open|date_format:$page_data.date_format} 
                                {if $item.date_open_begin}{ld_option i='dayhour-names' gid='start' option=$item.date_open_begin}{/if}
                                {if $item.date_open_end}{if $item.date_open_begin} - {/if}{ld_option i='dayhour-names' gid='start' option=$item.date_open_end}{/if}
                            {/if}
							-->
							{if $item.sold}<br><span class="status_text">{l i='text_sold' gid='listings'}</span>{/if}
							<span class="status_text">{$status_info}</span>
						</div>
						<div class="t-2">
			
							<?php /* lines below for displaying modified listing by date */ ?>
							{capture assign='date_modified'}{strip}
							{$item.date_modified|date_format:$page_data.date_format} <?php /* replaced the $listing.date_modified with $item.date_modified */ ?>
							{/strip}{/capture} 
							
							<span>{l i='field_date_modified' gid='listings'}: {if $date_modified}{$date_modified}{else}{l i='inactive_listing' gid='listings'}{/if}</span><br />
							<span>{l i='field_views' gid='listings'}: {$item.views}</span>
                            
						</div>
					</div>
					{if $show_user_info}
					<div class="user-info">
						<div class="t-3"></div>
						<div class="t-4">
							{capture assign='user_logo'}
								<?php /* #MOD# {if $show_logo || $item.user.is_show_logo} */ ?>
								<img src="{$item.user.media.user_logo.thumbs.small}" title="{$item.user.output_name|escape}">
								<?php /* #MOD# {else}
								<img src="{$item.user.media.default_logo.thumbs.small}" title="{$item.user.output_name|escape}">
								{/if} */ ?>
								{$item.user.output_name|truncate:20}
							{/capture}
							{if $item.user.status}
							<a href="{seolink module='users' method='view' data=$item.user}">{$user_logo}</a>
							{else}
							{$user_logo}
							{/if}
						</div>
					</div>	
					{/if}
				</div>
				<div class="clr"></div>
				{if $item.headline}<p class="headline" title="{$item.headline}">{$item.headline|truncate:100}</p>
					{else}<p class="headline">&nbsp;</p>
				{/if}
				<div class="a-link clearfix">
					{block name='save_listing_block' module='listings' listing=$item template='link' separator=''}
					{block name='listings_booking_block' module='listings' listing=$item template='link' show_link=1 show_price=1 separator='' no_save=1}
					<a href="{seolink module='listings' method='view' data=$item}">{l i='link_details' gid='listings'}</a>

					<?php /* compare / comparison feature */ ?>
					<div class="compare-feat">
						<span class="ListId">{$item.output_name|escape}</span>
						<a id="{$item.id}" href="#" class="compare">Bandingkan</a>
						<span class="prodId" hidden="">{$item.id}</span>
						<img class="ListImg hidden" src="{$item.media.photo.thumbs.middle}" title="{$item.output_name|escape}">
					</div>

					<span class="prioritas">Prioritas</span>
				</div>
				<!--<div class="a-link clearfix">-->
                {if $kpr_button && $item.property_type_str neq 'Lain-Lain'}
                <div style="margin-top:5px; margin-bottom:-20px;">
                <!--
                <div style="margin-top:5px;">                
					<a href="kpr-bca-{$item.property}-id-{$item.id}" target="#">{l i='kpr_bca' gid='listings'}</a>	
					<a href="kpr-mandiri-{$item.property}-id-{$item.id}" target="#">{l i='kpr_mandiri' gid='listings'}</a>
                </div>                    
                -->
                {foreach item=button from=$kpr_button}
                	<a href="kpr-{$button.name}-{$item.property}-id-{$item.id}" target="#"><img src="{$base_url}uploads/kpr_button/{$button.image}" style=" width:167px; height:19px; padding:0px; border:none;" /></a>	
                {/foreach}
                </div>
                {/if}
                
			</div>
	</div>
	{foreachelse}
		<div class="item empty">{l i='no_listings' gid='listings'}</div>
	{/foreach}
	</div>
	{if $listings}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}

	{if $update_map}
	<!-- display google map  -->
	{block name=update_default_map module=geomap markers=$markers map_id='listings_map_container'}
	{/if}
	
	{if $update_operation_type}
	<script>{literal}
	$(function(){
		var operation_type = $('#operation_type');
		if(operation_type.val() != '{/literal}{$update_operation_type}{literal}')
			$('#operation_type').val('{/literal}{$update_operation_type}{literal}').trigger('change');
	});
	{/literal}</script>
	{/if}

	<script>{literal}
	$(function(){
		{/literal}{if $update_search_block}$('#search_filter_block').html('{strip}{$search_filters_block}{/strip}');{/if}{literal}
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
	});
	{/literal}</script>

<script>{literal}
// JS Compare
$(function(){
	function getSelectedIds() {
   return $('.box .list-id').map(function(){ 
		return $(this).text(); }).toArray();
   }

	function updateLinkAndCounter() {
    var ids = getSelectedIds().map(function(x,i) { return [x].join(''); });
    $('#comp-container > a').attr('href', '{/literal}{$site_url}{literal}compare-listings-id-' + ids.join('-'));
    $("h3.msg-compare").text(ids.length == 1 ? 'Anda telah memilih 1 properti:' : 'Anda telah memilih ' + ids.length + ' properti:');
	if (ids.length == 0)
	{
		$("#comp-container").slideToggle("fast");
	}
	}

	// trigger
	$(".compare").click(function(e) {
	var prodName= $(this).prev('.ListId').text();
    var id= $(this).next('.prodId').text();
	var img= $(this).next(".prodId").next(".ListImg").attr("src");
		
    var selected = getSelectedIds();
	if(selected.length == 0)
	{
		$("#comp-container").slideToggle("fast"); //open slide the container
		e.preventDefault();
	}
    if(selected.length == 4) return false; // already 4 items added
    if(selected.indexOf(id) != -1) return false; // item already added
	
    $('<div/>', { 'class': 'box' })
       .append($('<img/>', { class: 'list-img', src:img }))
	   .append($('<span/>', { class: 'list-name', text:prodName }))
       .append($('<a/>', { href: '#', text: 'x', 'class':'close' }))
	   .append($('<span/>', { class: 'list-id hidden', text:id }))
       .appendTo('#comp-container');

	e.preventDefault();
    updateLinkAndCounter();
    $("#comp-container").removeClass("hidden");
	});
	
	// remove from list
	$(".close").live("click", function(e) {
		$(this).parent().remove();
		e.preventDefault() //disable jumping to top
		updateLinkAndCounter();
	});
	
//	$(".btn-close").live("click", function(e) {
//		$("#comp-container").slideToggle("fast"); //close the container
//		e.preventDefault() //disable jumping to top
//		var minimize=1;
//	}); 

	$(".close-container").live("click", function(e) {
		$("#comp-container").slideToggle("fast"); //close the container
		$(".box").remove();
		$(".compare-btn").attr("href", "#");
//		e.preventDefault() //disable jumping to top
	}); 

}); 
{/literal}</script>