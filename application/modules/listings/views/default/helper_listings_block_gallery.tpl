{if $listings}
	<div id="container-home-featured-prop">
		<div class="slider-container featured-property">
			<h2 class="head-title">{l i='header_'+$type+'_listings' gid='listings'}</h2>

			<ul class="bxslider clearfix">
				<!-- DISABLE <div class="{$type|escape}_listings_block">-->
					{foreach item=item key=key from=$listings}
					<li class="listing {$photo_size|escape}"><!-- DISABLE <div class="listing {$photo_size|escape}">-->
							<a href="{seolink module='listings' method='view' data=$item}">
								<img src="{$item.media.photo.thumbs[$photo_size]}" alt="{$item.output_name|escape}" title="{$item.output_name|escape}">
								{if $item.photo_count || $item.is_vtour}
								<div class="photo-info">
									<div class="panel">
										{if $item.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$item.photo_count}</span>{/if}
										{if $item.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
									</div>
									<div class="background"></div>
								</div>
								{/if}
							</a>
							<a href="{seolink module='listings' method='view' data=$item}">{if $photo_size eq 'big'}{$item.output_name|truncate:35}{else}{$item.output_name|truncate:30}{/if}</a>		
							<span>{$item.property_type_str} {$item.operation_type_str}</span>
							<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
						<!-- DISABLE </div>-->
					</li>
					{/foreach}
				<!-- DISABLE </div> -->
			</ul>
			
			<div id="finance">
				<div id="slidercaption-two"></div>
				<h2 class="finance-tool-btn">Alat Bantu Finansial</h2>
				<div id="fin-tools">
					<ul class="ftools clearfix">
						<li>
							<a href="#">Coming Soon</a>
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</li>
						<li>
							<a href="#">Coming Soon</a>
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</li>
						<li>
							<a href="#">Coming Soon</a>
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</li>
					</ul>
					<button class="close-btn">[x] Close</button>
				</div>
			</div>
		</div>
	</div>

<script>{literal}
$(document).ready(function () {
   $('.bxslider').bxSlider({
	   auto: false,
	   autoHover:true,
	   touchEnabled: false,
	   autoControls: false,
	   pager: true,
	   minSlides: 4,
	   maxSlides: 4,
	   slideWidth: 220,
	   startSlide: 0
	});
});

$(document).ready(function () {
   $('.ftools').bxSlider({
	   auto: false,
	   touchEnabled: false,
	   autoControls: false,
	   pager: true,
	   controls: false,
	   infiniteLoop: true,
	   captions: false,
	   startSlide: 0
	});
});
{/literal}</script>
	
	
{/if}