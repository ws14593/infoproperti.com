<div class="content-block load_content">
	<h1>{l i='header_order_approve' gid='listings'}</h1>
	<div class="inside edit_block">
		<form method="post" action="" name="save_form" id="order_approve_form" enctype="multipart/form-data">
			<div class="r" id="order_comment">
				<div class="f">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v">{if $order.comment}{$order.comment}{else}{l i='no_comment' gid='listings'}{/if}</div>
			</div>
			<div class="r">
				<div class="f">{l i='field_booking_answer' gid='listings'}:</div>
				<div class="v"><textarea name="period[answer]" rows="10" cols="80"></textarea></div>
			</div>
		
			<div class="b">
				<input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}" id="close_btn">
			</div>
		</form>
	</div>
</div>
