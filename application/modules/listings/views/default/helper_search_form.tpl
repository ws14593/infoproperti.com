{js module=listings file='listings-search.js'}
{if $search_filters_block}

<!-- save searches -->
<div class="listings_form edit_block" id="save_search_block">
	<h2>{l i='header_current_search' gid='listings'}</h2>
	<form id="save_search_form" name="save_search_form" action="{$site_url}listings/save_search" method="POST">	
		<div id='search_filter_block'>{$search_filters_block}</div>
		<div class="clr"></div>
		{if $use_save_search}
		<div class="r">
			<input type="submit" value="{l i='btn_save_search' gid='listings' type='button'}" id="btn_save_search" />
		</div>
		<input type="hidden" name="form_name" value="save_search" />
		{/if}
	</form>
	<div id="saved_searches_block">{$saved_searches_block}</div>
</div>

{/if}

{assign var='operation_type' value=$search_form_settings.object}
<div class="listings_form edit_block">
	<h2 id="quick_search_block">{l i='header_refine_search' gid='listings'}</h2>
	<form id="quick_search_form" name="search_listing_form" action="" method="POST">
	<div class="r">
	<div class="f">{l i='field_listing_type' gid='listings'}:</div>
		<div class="v">
			<select name="filters[type]" id="operation_type">
			{foreach item=item from=$search_form_settings.operations}
			<option value="{$item|escape}" {if $item eq $data.type}selected{/if}>{l i='operation_search_'+$item gid='listings'}</option>
			{/foreach}
			</select>
			<script>{literal}
				$(function(){
					$('#operation_type').live('change', function(){
						switch($(this).val()){
							case 'sale':
								$('#with_photo_box{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_date_start{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#date_start{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#alt_date_start{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#booking_date_end{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#date_end{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#alt_date_end{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal} select').val('');
							break;
							case 'buy':
								$('#with_photo_box{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#with_photo{/literal}{$search_form_settings.rand}{literal}').removeAttr('checked');
								$('#booking_date_start{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#date_start{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#alt_date_start{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#booking_date_end{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#date_end{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#alt_date_end{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal} select').val('');
							break;
							case 'rent':
								$('#with_photo_box{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_date_start{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_date_end{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal}').show();
							break;
							case 'lease':
								$('#with_photo_box{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#with_photo{/literal}{$search_form_settings.rand}{literal}').removeAttr('checked');
								$('#booking_date_start{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_date_end{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal}').show();
							break;
							case 'sold':
								$('#with_photo_box{/literal}{$search_form_settings.rand}{literal}').show();
								$('#booking_date_start{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#date_start{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#alt_date_start{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#booking_date_end{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#date_end{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#alt_date_end{/literal}{$search_form_settings.rand}{literal}').val('');
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal}').hide();
								$('#booking_guests{/literal}{$search_form_settings.rand}{literal} select').val('');
							break;
						}
					});
				});
			{/literal}</script>
		</div>
	</div>	
	<div class="r">
		<div class="f">{l i='field_location' gid='listings'}:</div>
		<div class="v">{country_input select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city var_country='filters[id_country]' var_region='filters[id_region]' var_city='filters[id_city]'}</div>
	</div>	
	<div class="r">
		<div class="f">{l i='field_category' gid='listings'}:</div>
		<div class="v">
			{if $data.id_category && $data.property_type}
				{assign var='category' value=$data.id_category+'_'+$data.property_type}
			{else}
				{assign var='category' value=$data.id_category}
			{/if}
			{block name='properties_select' module='properties' var_name='filters[category]' js_var_name='category' selected=$category cat_select=true}
		</div>
	</div>
	<div class="r periodbox {if $operation_type ne 'rent' && $operation_type ne 'lease'}hide{/if}" id="booking_date_start{$search_form_settings.rand}">
		<div class="f">{l i='field_booking_date_start' gid='listings'}:</div>
		<div class="v">
			<input type="text" name="booking_date_start" value="{if $data.booking_date_start|strtotime>0}{$data.booking_date_start|date_format:$search_form_settings.date_format|escape}{/if}" id="date_start{$search_form_settings.rand}">
			<input type="hidden" name="filters[booking_date_start]" value="{if $data.booking_date_start|strtotime>0}{$data.booking_date_start|date_format:$search_form_settings.date_format|escape}{/if}" id="alt_date_start{$search_form_settings.rand}">
			<script>{literal}
				$(function(){
					$('#date_start{/literal}{$search_form_settings.rand}{literal}').datepicker({dateFormat: '{/literal}{$search_form_settings.datepicker_date_format}{literal}', altFormat: '{/literal}{$search_form_settings.datepicker_alt_format}{literal}', altField: '#alt_date_start{/literal}{$search_form_settings.rand}{literal}', showOn: 'both'});
				});
			{/literal}</script>
		</div>
	</div>	
	<div class="r periodbox {if $operation_type ne 'rent' && $operation_type ne 'lease'}hide{/if}" id="booking_date_end{$search_form_settings.rand}">
		<div class="f">{l i='field_booking_date_end' gid='listings'}</div>
		<div class="v">
			<input type="text" name="booking_date_end" value="{if $data.booking_date_end|strtotime>0}{$data.booking_date_end|date_format:$search_form_settings.date_format|escape}{/if}" id="date_end{$search_form_settings.rand}">
			<input type="hidden" name="filters[booking_date_end]" value="{if $data.booking_date_end|strtotime>0}{$data.booking_date_end|date_format:$search_form_settings.date_format|escape}{/if}" id="alt_date_end{$search_form_settings.rand}">
			<script>{literal}
				$(function(){
					$('#date_end{/literal}{$search_form_settings.rand}{literal}').datepicker({dateFormat: '{/literal}{$search_form_settings.datepicker_date_format}{literal}', altFormat: '{/literal}{$search_form_settings.datepicker_alt_format}{literal}', altField: '#alt_date_end{/literal}{$search_form_settings.rand}{literal}', showOn: 'both'});
				});
			{/literal}</script>
		</div>
	</div>	
	<div class="r {if $operation_type ne 'rent' && $operation_type ne 'lease'}hide{/if}" id="booking_guests{$search_form_settings.rand}">
		<div class="f">{l i='field_booking_guests' gid='listings'}</div>
		<div class="v">
			{ld i='booking_guests' gid='listings' assign='booking_guests'}
			<select name="filters[booking_guests]">
				<option value="">{$booking_guests.header}</option>
				{foreach item=item key=key from=$booking_guests.option}
				<option value="{$key}" {if $key eq $data.booking_period.guests}{/if}>{$item}</option>
				{/foreach}
			</select>
		</div>
	</div>		
	<div class="r many pricebox">
		<div class="f">{l i='field_price_range' gid='listings'}:</div>
		<div class="v">
			<link rel="stylesheet" href="{$site_root}{$js_folder}jquery-ui/jquery-ui.custom.css" type="text/css" />
			<link rel="stylesheet" href="{$site_root}{$js_folder}slider/css/ui.slider.extras.css" type="text/css" />
			{js file='slider/jquery.ui.slider-'+$_LANG.rtl+'.js'}
			{js file='slider/selectToUISlider.jQuery.js'}
			{js file='number-format.js'}
			<script>{literal}
				var price_ranges = {};
				var currency_output = {/literal}{block name='currency_format_regexp_output' module='start'}{literal}
				{/literal}
				{foreach item=item key=key from=$price_range}
				{capture assign='min_price'}{strip}
					currency_output({$item.min_price})
				{/strip}{/capture}
				{capture assign='max_price'}{strip}
					currency_output({$item.max_price})
				{/strip}{/capture}
				price_ranges['{$key|escape}'] = {literal}{{/literal}data: [], min: {$min_price|strip_tags}, max: {$max_price|strip_tags}{literal}}{/literal};
				{foreach item=item2 key=key2 from=$item.data}
				{capture assign='price'}{strip}
					currency_output({$item2})
				{/strip}{/capture}
				price_ranges['{$key|escape}']['data'].push({literal}{{/literal}value: '{$item2}', text: {$price|strip_tags}{literal}}{/literal});
				{/foreach}
				price_ranges['{$key|escape}']['data'].push({literal}{{/literal}value: '{$item.max_price}', text: {$max_price|strip_tags}{literal}}{/literal});
				{/foreach}
				{literal}
				$(function(){	
					$('#price_min option').each(function(index, item){
						var item = $(item);
						item.html(currency_output(item.attr('value')));
					});
					
					$('#price_max option').each(function(index, item){
						var item = $(item);
						item.html(currency_output(item.attr('value')));
					});
					
					var price_min_selected_val = $('#price_min_selected_val');
					price_min_selected_val.html(currency_output(price_min_selected_val.html()));
					
					var price_max_selected_val = $('#price_max_selected_val');
					price_max_selected_val.html(currency_output(price_max_selected_val.html()));
								
					$('select#price_min, select#price_max').selectToUISlider({
						labels : 2,
						tooltip: false,
						tooltipSrc : 'text',
						labelSrc: 'text',
						isRTL: {/literal}{if $_LANG.rtl == 'rtl'}true{else}false{/if}{literal},
					});
					$('#operation_type').bind('change', function(){
						var type = $(this).val();
						
						$('.pricebox .ui-slider').slider('destroy').remove();
						
						var price_min = $('#price_min');
						price_min.empty();
						for(var i in price_ranges[type]['data']){
							$("<option />", {value: price_ranges[type]['data'][i].value, text: price_ranges[type]['data'][i].text}).appendTo(price_min);
						}
						
						var price_max = $('#price_max');
						price_max.empty();
						for(var i in price_ranges[type]['data']){
							$("<option />", {value: price_ranges[type]['data'][i].value, text: price_ranges[type]['data'][i].text, selected: true}).appendTo(price_max);
						}
						
						$('#price_min_selected_val').html(price_ranges[type].min);
						$('#price_max_selected_val').html(price_ranges[type].max);
						
						$('select#price_min, select#price_max').selectToUISlider({
							labels : 2,
							tooltip: false,
							tooltipSrc : 'text',
							labelSrc: 'text',
						});
					});
				});
			{/literal}</script>
			<div class="select-slider">	
				
				<select id="price_min" name="filters[price_min]" class="hide">
				
					{foreach item=item from=$price_range[$operation_type].data}
					<option value="{$item|escape}" {if $item eq $data.price_min}selected{/if}>{$item}</option>
					{/foreach}
					
					<option value="{$price_range[$operation_type].max_price|escape}" {if $price_range[$operation_type].max_price eq $data.price_min}selected{/if}>{$price_range[$operation_type].max_price}</option>
					
				</select>
									
				<select id="price_max" name="filters[price_max]" class="hide">
				
					{foreach item=item from=$price_range[$operation_type].data}
					<option value="{$item|escape}" {if $item eq $data.price_max}selected{/if}>{$item}</option>
					{/foreach}
					
					<option value="{$price_range[$operation_type].max_price|escape}" {if !$data.price_max}selected{/if}>{$price_range[$operation_type].max_price}</option>
					
				</select>
									
				<div class="vals">
					<div id="price_min_selected_val" class="fleft" dir="ltr">
						{$price_range[$operation_type].min_price}
					</div>
					<div id="price_max_selected_val" class="fright" dir="ltr">
						{$price_range[$operation_type].max_price}
					</div>
				</div>
				
				<div class="r price-input">
					<div class="f">Min</div>
					<div class="v"><input id="price_min" name="filters[price_min]" value="0" type="text" /></div>
				</div>
				<div class="r price-input">
					<div class="f">Max</div>
					<div class="v"><input id="price_max" name="filters[price_max]" type="text" /></div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
	<div id="quick_search_extend_form">{$quick_search_extend_form}</div>
	<div class="r {if $quick_search_extend_form}{/if}">
		<div id="with_photo_box{$search_form_settings.rand}" {if $operation_type eq 'buy' || $operation_type eq 'lease'}class="hide"{/if}>
			<input type="hidden" name="filters[with_photo]" value="0">
			<input type="checkbox" name="filters[with_photo]" value="1" id="with_photo{$search_form_settings.rand}" {if $data.with_photo}checked{/if}>
			<label for="with_photo{$search_form_settings.rand}">{l i='field_with_photo' gid='listings'}</label><br>
		</div>
		
		<input type="hidden" name="filters[by_open_house]" value="0">
		<input type="checkbox" name="filters[by_open_house]" value="1" id="open_house" {if $data.open_house}checked{/if}>
		<label for="open_house">{l i='field_open_house' gid='listings'}</label><br>
		
		<input type="hidden" name="filters[by_private]" value="0">
		<input type="checkbox" name="filters[by_private]" value="1" id="by_private" {if $data.by_private}checked{/if}>
		<label for="by_private">{l i='field_by_private' gid='listings'}</label>
	</div>
	<div class="r">
		<input type="submit" value="{l i='btn_search' gid='start' type='button'}" />
	</div>
	<input type="hidden" name="form" value="quick_search_form" />
	</form>
</div>

<!-- More refinements 
<div class="listings_form edit_block">
	<h2>{l i='header_extend_refine_search' gid='listings'}</h2>
	<form id="advanced_search_form" name="extend_search_listing_form" action="" method="POST">		
		<div id="advanced_search_extend_form">{$advanced_search_extend_form}</div>
		<div class="r {if $advanced_search_extend_form}separator{/if}">
			<div class="f">{l i='field_id' gid='listings'}:</div>
			<div class="v"><input type="text" name="filters[id]" value="{$data.id|escape}"></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_postal_code' gid='listings'}:</div>
			<div class="v"><input type="text" name="filters[zip]" value="{$data.zip|escape}"></div>
		</div>
		
		<div class="r">
			<div class="f">{l i='field_radius' gid='listings'}:</div>
			<div class="v">
				<select name="filters[radius]">
					<option value="">{l i='text_radius_select' gid='listings'}</option>
					{foreach item=item key=key from=$radius_data.option}
					<option value="{$key|escape}" {if $key eq $data.radius}selected{/if}>{$item}</option>
					{/foreach}
				</select>
			</div>
		</div>
		
		<div class="r">
			<div class="f">{l i='field_keyword' gid='listings'}:</div>
			<div class="v"><input type="text" name="filters[keyword]" value="{$data.keyword|escape}"></div>
		</div>
		<div class="r">
			<input type="submit" value="{l i='btn_search' gid='start' type='button'}">
		</div>
		<input type="hidden" name="form" value="advanced_search_form">
	</form>
</div>	
-->
<script>{literal}
	$(function(){
		new listingSearch({
			siteUrl: '{/literal}{$site_url}{literal}',
			{/literal}{if $view_mode}viewMode: '{$view_mode}',{/if}{literal}
			tIds: ['save_search_block'],
		});
	});
{/literal}</script>
