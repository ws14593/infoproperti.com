{include file="header.tpl" load_type='ui'}

<div class="rc">
	<div class="content-block">
		
		{if $use_rss}<span id="rss_link" class="btn-link fright" title="{l i='link_search_results_rss' gid='listings' type='button'}"><ins class="with-icon i-rss"></ins></span>{/if}
		
		<h1>{l i='header_listings_result' gid='listings'} - <span id="total_rows">{$page_data.total_rows}</span> {l i='header_listings_found' gid='listings'}</h1>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="search_listings_sections">
				{foreach item=tgid from=$operation_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_operation_type eq $tgid}active{/if}"><a href="{$menu_action_link|replace:'[operation_type]':$tgid}/default/DESC/1">{l i='operation_search_'+$tgid gid='listings'}</a></li>
				{/foreach}
			</ul>
			<a href="{$site_url+'listings/set_view_mode/list'}" class="btn-link fright" title="{l i='link_view_list' gid='listings' type='button'}"><ins class="with-icon i-list"></ins></a>
		</div>
		
		<div class="sorter line" id="sorter_block">
			{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
			{if $listings}<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>{/if}
		</div>
		
		{block name=show_default_map module=geomap id_user=$data.id_user gid='listing_search' settings=$map_settings markers=$markers width='754' height='400' map_id='listings_map_full_container'}
		
		<div id="listings_map">{$block}</div>

		<div id="pages_block_2">{if $listings}{pagination data=$page_data type='full'}{/if}</div>
		
		{js module=listings file='listings-map.js'}
		<script>{literal}
			$(function(){
				new listingsMap({
					siteUrl: '{/literal}{$site_url}{literal}',
					mapAjaxUrl: '{/literal}listings/ajax_index{literal}',
					sectionId: 'search_listings_sections',
					{/literal}{if $use_rss}useRss: true,{/if}{literal}
					operationType: '{/literal}{$current_operation_type}{literal}',
					order: '{/literal}{$order}{literal}',
					orderDirection: '{/literal}{$order_direction}{literal}',
					page: {/literal}{$page}{literal},
					tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
				});
			});
		{/literal}</script>
	</div>
</div>

<div class="lc">
	<div class="inside account_menu">
		{block name=listings_search_block module=listings}
		{if $use_poll_in_search}{block name=show_poll_place_block module=polls one_poll_place=0}{/if}
		{helper func_name=show_banner_place module=banners func_param='big-left-banner'}
		{helper func_name=show_banner_place module=banners func_param='left-banner'}
	</div>
</div>

<div class="clr"></div>

{include file="footer.tpl"}
