{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_saved_searches' gid='listings'}</h1>		
		<table class="searches_list list">
		<tr>
			<th>{l i='field_searches' gid='listings'}</th>
			<th class="w100">&nbsp;</th>
		</tr>
		{foreach item=item from=$searches}
		<tr class="item">
			<td>
				{l i='field_search' gid='listings'} <b>{$item.date_search}</b><br>
				{$item.name}
			</td>
			<td class="center">
				<a href="{$site_url}listings/load_saved_search/{$item.id}" class="btn-link fright"><ins class="with-icon i-load"></ins></a>
				<a href="{$site_url}listings/delete_saved_search/{$item.id}" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
			</td>
		</tr>
		{foreachelse}
		<tr><td colspan="2"><div class="item empty">{l i='no_saved_searches' gid='listings'}</div></td></tr>
		{/foreach}
		</table>
		{if $searches}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

