{if $listings}
{js file='dualslider/jquery.dualSlider.0.3.min.js'}
{js file='dualslider/jquery.timers-1.2.js'}

<script>var forms_{$slider_form_settings.rand} = {literal}{}{/literal};</script>
<div class="slider_{$view|escape}">
	<div class="slider_wrapper">
		<div class="slider_wrapper2">
			<div class="slider">
				<div class="carousel">		
					<div class="backgrounds_wrapper">
						<div class="backgrounds_wrapper2">
							<div class="backgrounds_wrapper3">
								<div class="backgrounds">
														
									<!-- comp slider -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/competition-slider.jpg') no-repeat;">
										<a href="http://infoproperti.com/competition/" style="width:100%;height:520px;display:block;" target="_blank"></a>
									</div>*/ ?>
									
									<!-- lebaran slider -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slide-lebaran-6.jpg') no-repeat;"></div>*/ ?>
														
									<!-- 17 agustus slider -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-17-agustus-revisi.jpg') no-repeat;"></div>
									<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-17-agustus-1.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider national sport day -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-hari-olahraga-nasional.jpg') no-repeat;"></div>*/ ?>
														
									<!-- slider pmi -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-pmi.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider batik -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/batik-slider.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider sumpah pemuda -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-sumpah-pemuda.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider hari guru nasional -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-hari-guru.jpg') no-repeat;"></div>*/ ?>

									<?php /* =========== Disabled. for static slider only ===================*/ ?>
									<?php /*disable temporarily {foreach item=item key=key from=$listings} */?>
									<?php /*<div class="item item_{$key|escape}" style="background: url('http://infoproperti.com/application/views/default/img/custom-slides/main-img-jakarta.jpg') no-repeat;"></div> */?>
									<?php /* disable temporarily {/foreach} */?>
									
									<?php /* =========== for Original slider only  ===================*/ ?>
									{foreach item=item from=$sliders}
										{if $item.slider_url != ""}
											<a href="{$item.slider_url}" target="_blank">
												<div class="item item_{$item.key}" style="background: url('{$item.media}') no-repeat;"></div>
											</a>
										{else}
											<div class="item item_{$item.key}" style="background: url('{$item.media}') no-repeat;"></div>
										{/if}
									{/foreach}

									<?php /*{foreach item=item key=key from=$listings}
									{if $item.slider_image != ""}	  #MOD# 
										<div class="item item_{$key|escape}" style="background: url('{$item.media.slider.thumbs[$view]}') no-repeat;"></div>
									{/if}  #MOD# 
									{/foreach}*/ ?>
						
								</div>
							</div>	
						</div>
					</div>
						
					<div class="gradient_wrapper">
						<div class="gradient_wrapper2">
							<div class="gradient">
								<div class="gradient-l"></div>
								<div class="gradient-r"></div>
							</div>
						</div>
					</div>
					
					{if $slider_form_page_data.count > 1}
					<div class="paging_wrapper">
						<div class="paging_wrapper2">
							<div class="paging">
								<a id="previous_item" class="previous" alt="{l i='nav_prev' gid='start' type='button'}" title="{l i='nav_prev' gid='start' type='button'}">{l i='nav_prev' gid='start'}</a>
								<a id="next_item" class="next" alt="{l i='nav_next' gid='start' type='button'}" title="{l i='nav_next' gid='start' type='button'}">{l i='nav_next' gid='start'}</a>
							</div>
						</div>
					</div>	
					{/if}
					
					<div class="panel"> <!-- disable for later use-->
					<?php /*
						<div class="details_wrapper">
							<div class="details">
								{foreach item=item key=key from=$listings}
								<div class="listing detail">
									<a href="{seolink module='users' method='view' data=$item.user}"><img src="{$item.user.media.user_logo.thumbs.small}" alt="{$item.user.output_name|escape}"></a>
									<a href="{seolink module='users' method='view' data=$item.user}">{$item.user.output_name|truncate:50}</a>							
									<a href="{seolink module='listings' method='view' data=$item}">{if $view eq '654_395'}{$item.output_name|truncate:50}{else}{$item.output_name|truncate:100}{/if}</a>
									<span>{$item.property_type_str} {$item.operation_type_str}</span>
									<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
								</div>
								{/foreach}
							
							</div>
							<div class="details_background"></div>
						</div>
						*/?>
					</div>
					
					
					<div class="slider_search_form">
					<h2 id="inner-search-title-top"><span>Temukan properti <i>ideal</i> anda</span></h2>
						{$slider_search_form}
						<div class="background"></div>
					</div>		
				</div>	
				
			</div>
		</div>
	</div>
</div>
<script>{literal}
	$(function(){
		$(".carousel").dualSlider({
			auto: {/literal}{if $slider_form_page_data.slider_auto}true{else}false{/if}{literal},
			autoDelay: {/literal}{if $slider_form_page_data.slider_auto}{$slider_form_page_data.rotation*1000}{else}false{/if}{literal},
			easingCarousel: "swing",
			easingDetails: "swing",
			durationCarousel: 300,
			durationDetails: 300,
			widthsliderimage: $(".carousel .backgrounds .item").width(),
			{/literal}{if $_LANG.rtl eq 'rtl'}rtl: true,{/if}{literal}
		});
		$('.gradient .gradient-l').bind('click', function(){
			{/literal}{if {$_LANG.rtl === 'rtl'}{literal}
			var next = $('#next_item');
			if(next.css('display') != 'none') next.trigger('click');
			{/literal}{else}{literal}
			var previous = $('#previous_item');
			if(previous.css('display') != 'none')  previous.trigger('click');
			{/literal}{/if}{literal}
		});
		$('.gradient .gradient-r').bind('click', function(){
			{/literal}{if {$_LANG.rtl === 'rtl'}{literal}
			var previous = $('#previous_item');
			if(previous.css('display') != 'none')  previous.trigger('click');
			{/literal}{else}{literal}
			var next = $('#next_item');
			if(next.css('display') != 'none') next.trigger('click');
			{/literal}{/if}{literal}
			
		});
		$('.slider.backgrounds .item').bind('click', function(){
			{/literal}{if {$_LANG.rtl === 'rtl'}{literal}
			var previous = $('#previous_item');
			if(previous.css('display') != 'none')  previous.trigger('click');
			{/literal}{else}{literal}
			var next = $('#next_item');
			if(next.css('display') != 'none') next.trigger('click');
			{/literal}{/if}{literal}
		});
	});
{/literal}</script>
{/if}
