{include file="header.tpl" load_type='ui'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/listings/add_kpr_button">Add KPR button</a></div></li>               
	</ul>
    <font style="color:white; font-size:16px; ">Recommended dimension for KPR button is 167 pixels wide and 20 pixels tall</font>
</div>
<form id="listings_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th>{l i='field_photo' gid='listings'}</th>
	<th class="w200">Bank</th>
	<th class="w150">Status</th>
	<th class="w150">&nbsp;</th>
</tr>
{foreach item=item from=$kpr_button}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td><img src="{$base_url}uploads/kpr_button/{$item.image}" alt="{$item.output_name|truncate:20|escape}" style="width:167px; height:20px;"></td>
	<td>{$item.name|truncate:23}</td>
	<td>
    	{if $item.status eq '1'}
        	Active
        {else}
        	Disabled
        {/if}
    </td>
	<td class="icons">
		{if $item.status}
		<a href="{$site_url}admin/listings/activate_kpr_button/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_listing' gid='listings' type='button'}" title="{l i='link_deactivate_listing' gid='listings' type='button'}"></a>
		{else}
		<a href="{$site_url}admin/listings/activate_kpr_button/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_listing' gid='listings' type='button'}" title="{l i='link_activate_listing' gid='listings' type='button'}"></a>
		{/if}
		<a href="{$site_url}admin/listings/edit_kpr_button/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_listing' gid='listings' type='button'}" title="{l i='link_edit_listing' gid='listings' type='button'}"></a>
		<a href="{$site_url}admin/listings/delete_kpr_button/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_listing' gid='listings' type='button'}" title="{l i='link_delete_listing' gid='listings' type='button'}"></a>
	</td>
</tr>
{foreachelse}
{assign var='colspan' value=6}
{depends module=reviews}{assign var='colspan' value=$colspan+1}{/depends}
<tr><td colspan="{$colspan}" class="center">{l i='no_listings' gid='listings'}</td></tr>
{/foreach}
</table>
</form>
{include file="footer.tpl"}
