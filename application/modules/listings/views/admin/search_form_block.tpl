	{if $form_data}
		{foreach item=item from=$form_data}
		{counter print=false assign=counter}
		{if $item.type eq 'section'}
			{foreach item=field key=key from=$item.section.fields}
			<div class="row">
				<div class="h">{$field.field_content.name}</div>
				<div class="v">{include file="search_field_block.tpl" module="listings" field=$field}</div>
			</div>
			{/foreach}
		{else}
			<div class="row">
				<div class="h">{$item.field_content.name}</div>
				<div class="v">{include file="search_field_block.tpl" module="listings" field=$item}</div>
			</div>
		{/if}
		{/foreach}
	{/if}
