	<div class="photo-info-area">
		<div class="photo-area">
		<img src="{$photo.media.thumbs.200_200}" hspace="3" onclick="javascript: gUpload.full_view('{$photo.media.thumbs.620_400}');" /><br>
		<b>{l i='photo_status' gid='listings'}: {if $photo.status}<font class="stat-active">{l i='photo_active' gid='listings'}</font>{else}<font class="stat-moder">{l i='photo_moderate' gid='listings'}</font>{/if}</b><br>
		{if $photo.comment}{$photo.comment|truncate:50:'...':true}<br>{/if}
		</div>
		<a href="#" onclick="javascript: gUpload.open_edit_form({$photo.id}); return false;"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='btn_edit' gid='start' type='button'}" title="{l i='btn_edit' gid='start' type='button'}"></a>
		<a href="#" onclick="javascript: gUpload.delete_photo({$photo.id}); return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='btn_delete' gid='start' type='button'}" title="{l i='btn_delete' gid='start' type='button'}"></a>
		<div class="clr"></div>
	</div>
