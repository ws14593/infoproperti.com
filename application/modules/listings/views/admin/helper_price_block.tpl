{if $data.price_negotiated}
	{l i='text_negotiated_price_'+$data.operation_type gid='listings'}
{else}
	{switch from=$data.operation_type}
		{case value='sale'}
			{if $data.price_reduced > 0}
				{if $template eq 'small'}
					{block name='currency_format_output' module='start' value=$data.price_reduced cur_gid=$data.gid_currency}
				{else}
					<div class="fleft">{block name='currency_format_output' module='start' value=$data.price_reduced cur_gid=$data.gid_currency}</div>&nbsp;
					<del>{block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}</del>
				{/if}
			{else}
				{block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}
			{/if}
		{case value='buy'}
			{if $data.price > 0}{l i='text_price_from' gid='listings'} {block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}{/if}
			{if $data.price_max > 0}{l i='text_price_to' gid='listings'} {block name='currency_format_output' module='start' value=$data.price_max cur_gid=$data.gid_currency}{/if}
		{case value='rent'}
			{if $data.price_reduced > 0}
				{if $template eq 'small'}
					{block name='currency_format_output' module='start' value=$data.price_reduced cur_gid=$data.gid_currency}
				{else}
					<div class="fleft">{block name='currency_format_output' module='start' value=$data.price_reduced cur_gid=$data.gid_currency}</div>&nbsp;
					<del>{block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}</del>
				{/if}
			{else}
				{block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}
			{/if}
			
			{$data.price_period_str}
		{case value='lease'}
			{if $data.price > 0}{l i='text_price_from' gid='listings'} {block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}{/if}
			{if $data.price_max > 0}{l i='text_price_to' gid='listings'} {block name='currency_format_output' module='start' value=$data.price_max cur_gid=$data.gid_currency}{/if}		
			
			{$data.price_period_str}
	{/switch}
{/if}
