{include file="header.tpl" load_type='ui'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/listings/add_kpr_button">Add KPR button</a></div></li>
		<li><div class="l"><a href="{$site_url}admin/listings/kpr_button">Back</a></div></li> 
	</ul>
	&nbsp;
</div>
{$button->id}
<form method="post" action="{if $button}{$site_url}admin/listings/update_kpr_button/{$button.id}{else}{$site_url}admin/listings/save_kpr_button{/if}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{if $button.id}Edit KPR button{else}Add KPR button{/if}</div>
		<div class="row">
			<div class="h">Bank</div>
			<div class="v">
            	<select name="bank_id">
                		<option value="empty">-- SELECT BANK --</option>
                	{foreach item=item from=$bank}
                    	<option value="{$item->bankID}" {if $bank_id eq $item->bankID}selected = 'selected'{elseif $button.bank_id eq $item->bankID} selected = 'selected'{/if} >{$item->name}</option>
                    {/foreach}
                </select>
            </div>
		</div>
		{if $data.user_id eq 0}
		<div class="row">
			<div class="h">Active</div>
			<div class="v"><input type="checkbox" value="1" name="status" {if $status}checked{elseif $button.status eq '1'}checked{/if}></div>
		</div>
		{/if}
		<div class="row">
			<div class="h">Image</div>
			<div class="v">
            	<input type="file" name="userfile" >
                {if $button}
                <input type="hidden" name="old_image" value="{$button.image}" />
				<input type="hidden" name="button_id" value="{$button.id}" />                
                {/if}
			</div>
		</div>


	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/listings/kpr_button">{l i='btn_cancel' gid='start'}</a>
</form>
{include file="footer.tpl"}
