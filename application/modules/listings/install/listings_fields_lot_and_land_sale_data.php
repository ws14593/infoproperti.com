<?php

$fe_sections = array(
	array("data" => array( "gid" => "info_3", "editor_type_gid" => "lot_and_land_sale", 'sorter' => 1)),
	array("data" => array( "gid" => "comment_3", "editor_type_gid" => "lot_and_land_sale", 'sorter' => 2)),
);

$fe_fields = array(
	array("data" => array( "gid" => "irrigated_3", "section_gid" => "info_3", "editor_type_gid" => "lot_and_land_sale", "field_type" => "checkbox", "fts" => "1", "settings_data" => '', "sorter" => "1", "options" => '')),
	array("data" => array( "gid" => "residence_3", "section_gid" => "info_3", "editor_type_gid" => "lot_and_land_sale", "field_type" => "checkbox", "fts" => "1", "settings_data" => '', "sorter" => "2", "options" => '')),
	array("data" => array( "gid" => "comments_3", "section_gid" => "comment_3", "editor_type_gid" => "lot_and_land_sale", "field_type" => "textarea", "fts" => "1", "settings_data" => '', "sorter" => "1", "options" => '')),
);

$fe_forms = array(
	array("data" => array( "gid" => "main_search_form_3", "editor_type_gid" => "lot_and_land_sale", "name" => "Index search form", "field_data" => 'a:2:{i:0;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"irrigated_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}i:1;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"residence_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}}')),
	array("data" => array( "gid" => "quick_search_form_3", "editor_type_gid" => "lot_and_land_sale", "name" => "Quick search form", "field_data" => 'a:2:{i:0;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"irrigated_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}i:1;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"residence_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}}')),
	array("data" => array( "gid" => "advanced_search_3", "editor_type_gid" => "lot_and_land_sale", "name" => "Advanced search form", "field_data" => '')),
	array("data" => array( "gid" => "admin_export_form_3", "editor_type_gid" => "lot_and_land_sale", "name" => "Admin export form", "field_data" => 'a:2:{i:0;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"irrigated_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}i:1;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"residence_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}}')),
	array("data" => array( "gid" => "user_export_form_3", "editor_type_gid" => "lot_and_land_sale", "name" => "User export form", "field_data" => 'a:2:{i:0;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"irrigated_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}i:1;a:2:{s:4:"type";s:5:"field";s:5:"field";a:3:{s:3:"gid";s:11:"residence_3";s:11:"section_gid";s:6:"info_3";s:4:"type";s:8:"checkbox";}}}')),
);	
