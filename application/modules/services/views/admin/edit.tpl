{include file="header.tpl"}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{if $data.id}{l i='admin_header_service_change' gid='services'}{else}{l i='admin_header_service_add' gid='services'}{/if}</div>
		<div class="row">
			<div class="h">{l i='field_gid' gid='services'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.gid}" name="gid"></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_name' gid='services'}:&nbsp;* </div>
			<div class="v">
				{foreach item=lang_item key=lang_id from=$languages}
				<input type="{if $lang_id eq $cur_lang}text{else}hidden{/if}" name="langs[{$lang_id}]" value="{if $validate_lang}{$validate_lang[$lang_id]}{else}{$data.name}{/if}" lang-editor="value" lang-editor-type="langs" lang-editor-lid="{$lang_id}">
				{/foreach}
				<a href="#" lang-editor="button" lang-editor-type="langs"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16"></a>
				{block name=lang_inline_editor module=start}
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_pay_type' gid='services'}:&nbsp;* </div>
			<div class="v"><select name="pay_type">
				{foreach item=item key=key from=$pay_type_lang.option}<option value="{$key}"{if $key eq $data.pay_type} selected{/if}>{$item}</option>{/foreach}
			</select></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_status' gid='services'}: </div>
			<div class="v"><input type="checkbox" value="1" {if $data.status}checked{/if} name="status"></div>
		</div>

		<div class="row">
			<div class="h">{l i='field_template' gid='services'}: </div>
			<div class="v">
			{if $template}
				{$template.name}<input type="hidden" name="template_gid" value="{$data.template_gid}">
			{else}
				<select name="template_gid" onchange="javascript: load_param_block(this.value);">
					{foreach item=item from=$templates}<option value="{$item.gid}">{$item.name}</option>{/foreach}
				</select>
			{/if}
			</div>
		</div>

		<div id="admin_params">
		{$template_block}
		</div>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/services">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
function showLangs(divId){
	$('#'+divId).slideToggle();
}

function load_param_block(id){
	$('#admin_params').load('{/literal}{$site_url}admin/services/ajax_get_template_admin_param_block/{literal}'+id);
	$("div.row:odd").addClass("zebra");
}

{/literal}</script>

{include file="footer.tpl"}
