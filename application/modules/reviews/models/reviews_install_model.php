<?php
/**
* Review install model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Reviews_install_model extends Model{
	
	/**
	 * Code igniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"feedbacks-items" => array(
					"action" => "none",
					"items" => array(
						"reviews_list_menu_item" => array("action" => "create", "link" => "admin/reviews/index", "status" => 1, "sorter" => 1),
					),
				),
				"system-items" => array(
					"action" => "none",
					"items" => array(
						"reviews_sett_menu_item" => array("action" => "create", "link" => "admin/reviews/types", "status" => 1, "sorter" => 5),
					),
				),
			),
		),
		"admin_reviews_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Reviews section",
			"items" => array(
				"rws_objects_item" => array("action" => "create", "link" => "admin/reviews/index", "status" => 1),
				"rws_types_item" => array("action" => "create", "link" => "admin/reviews/types", "status" => 1),
				"rws_settings_item" => array("action" => "create", "link" => "admin/reviews/settings", "status" => 1),
			),
		),
	);

	/**
	 * Ausers configuration
	 * @var array
	 */
	private $ausers = array(
		array("module" => "reviews", "method" => "index", "is_default" => 1),
		array("module" => "reviews", "method" => "types", "is_default" => 0),
		array("module" => "reviews", "method" => "settings", "is_default" => 0),
	);

	/**
	 * Notifications configuration
	 * @var array
	 */
	private $notifications = array(
		"templates" => array(
			array("gid"=>"user_reviews_object", "name"=>"Review object", "vars"=>array("type", "object_id", "review", 'poster', 'user'), "content_type"=>"text"),
			array("gid"=>"auser_reviews_object", "name"=>"Review object (for admin)", "vars"=>array("type", "object_id", "review", 'poster', 'user'), "content_type"=>"text"),
			array("gid"=>"user_reviews_reply", "name"=>"Reply on review", "vars"=>array("type", "object_id", "review", 'comment', 'responder', 'user'), "content_type"=>"text"),
			array("gid"=>"auser_reviews_reply", "name"=>"Reply on review (for admin)", "vars"=>array("type", "object_id", "review", 'comment', 'responder', 'user'), "content_type"=>"text"),
		),
		"notifications" => array(
			array("gid"=>"user_reviews_object", "template"=>"user_reviews_object", "send_type" => "simple"),
			array("gid"=>"auser_reviews_object", "template"=>"auser_reviews_object", "send_type" => "simple"),
			array("gid"=>"user_reviews_reply", "template"=>"user_reviews_reply", "send_type" => "simple"),
			array("gid"=>"auser_reviews_reply", "template"=>"auser_reviews_reply", "send_type" => "simple"),
		),
	);
	
	/**
	 * Spam configuration
	 * @var array
	 */
	private $spam = array(
		array("gid"=>"reviews_object", "form_type"=>"select_text", "send_mail"=>true, "status"=>true, "module"=>"reviews", "model"=>"Reviews_model", "callback"=>"spam_callback"),
	);
	
	/**
	 * Moderation types
	 * @var array
	 */
	private $moderation_types = array(
		array(
			"name" => "reviews",
			"mtype" => "-1",
			"module" => "reviews",
			"model" => "Reviews_model",
			"check_badwords" => "1",
			"method_get_list" => "",
			"method_set_status" => "",
			"method_delete_object" => "",
			"allow_to_decline" => "0",
			"template_list_row" => "",
		),
 	);

	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Check system requirements of module
	 */
	function _validate_requirements(){
		$result = array("data"=>array(), "result" => true);

		//check for Mbstring
		$good			= function_exists("mb_convert_encoding");
		$result["data"][] = array(
			"name" => "Mbstring extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;

		//check for iconv
		$good			= function_exists("iconv");
		$result["data"][] = array(
			"name" => "Iconv extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;
		return $result;
	}
	
	/**
	 * Install links to menu module
	 */
	public function install_menu(){
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("reviews", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}
	}
	
	/**
	 * Install notifications links
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"])) $template_data["vars"] = implode(",", $template_data["vars"]);
			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_gid] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);
		}

		foreach ((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				$template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				$templates_ids[$notification_data["template"]] = $template["id"];
			}
			
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("reviews", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
		
		$this->CI->Notifications_model->update_langs($this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_export($langs_ids=null){
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs($this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Uninstall notifications links
	 */
	public function deinstall_notifications(){
		//add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}
	
	/**
	 * Install spam links
	 */
	public function install_spam(){
		// add spam type
		$this->CI->load->model("spam/models/Spam_type_model");

		foreach((array)$this->spam as $spam_data){
			$validate_data = $this->CI->Spam_type_model->validate_type(null, $spam_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Spam_type_model->save_type(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import spam languages
	 * @param array $langs_ids
	 */
	public function install_spam_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$this->CI->load->model("spam/models/Spam_type_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("reviews", "spam", $langs_ids);
		if(!$langs_file){log_message("info", "Empty spam langs data");return false;}
	
		$this->CI->Spam_type_model->update_langs($this->spam, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export spam languages
	 * @param array $langs_ids
	 */
	public function install_spam_lang_export($langs_ids=null){
		$this->CI->load->model("spam/models/Spam_type_model");
		$langs = $this->CI->Spam_type_model->export_langs((array)$this->spam, $langs_ids);
		return array("spam" => $langs);
	}
	
	/**
	 * Uninstall spam links
	 */
	public function deinstall_spam(){
		//add spam type
		$this->CI->load->model("spam/models/Spam_type_model");

		foreach((array)$this->spam as $spam_data){
			$this->CI->Spam_type_model->delete_type($spam_data["gid"]);
		}
	}
	
	/**
	 * Install links to moderation module
	 */
	public function install_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$mtype['date_add'] = date("Y-m-d H:i:s");
			$this->CI->Moderation_type_model->save_type(null, $mtype);
		}
	}
	
	/**
	 * Untall moderation languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('reviews', 'moderation', $langs_ids);

		if(!$langs_file){log_message('info', 'Empty moderation langs data'); return false;}

		$this->CI->load->model('moderation/models/Moderation_type_model');
		$this->CI->Moderation_type_model->update_langs($this->moderation_types, $langs_file);
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('moderation/models/Moderation_type_model');
		return array('moderation' => $this->CI->Moderation_type_model->export_langs($this->moderation_types, $langs_ids));
	}
	
	/**
	 * Uninstall links to moderation module
	 */
	public function deinstall_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$type = $this->CI->Moderation_type_model->get_type_by_name($mtype["name"]);
			$this->CI->Moderation_type_model->delete_type($type['id']);
		}
	}
	
	/**
	 * Install moderators links
	 */
	public function install_ausers () {
		//install ausers permissions
		$this->CI->load->model("Ausers_model");

		foreach((array)$this->ausers as $method_data){
			//$validate_data = $this->CI->Ausers_model->validate_method($method_data, true);
			//if(!empty($validate_data["errors"])) continue;
			$this->CI->Ausers_model->save_method(null, $method_data);
		}
	}
	
	/**
	 * Import moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_update($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("reviews", "ausers", $langs_ids);
		if(!$langs_file){log_message("info", "Empty ausers langs data");return false;}

		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "reviews";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method["method"]])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method["method"]]);
			}
		}
	}

	/**
	 * Export moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_export($langs_ids){
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "reviews";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method["method"]] = $method["langs"];
		}
		return array('ausers' => $return);
	}
	
	/**
	 * Uninstall moderators links
	 */
	public function deinstall_ausers(){
		$this->CI->load->model("Ausers_model");
		$params = array();
		$params["where"]["module"] = "reviews";
		$this->CI->Ausers_model->delete_methods($params);
	}

	/**
	 * Install fields
	 */
	public function _prepare_installing(){
		$this->CI->load->model("Reviews_model");
		
		$fields = array();
		foreach ($this->CI->pg_language->languages as $id => $value){
			$fields["values_".$value["id"]] = array(
				"type" => "TEXT",
				"null" => TRUE,
			);
		}
		$this->CI->Reviews_model->lang_dedicate_module_callback_add($fields);
	}
	
	/**
	 * Install rating types
	 */
	function _arbitrary_installing(){

		///// add entries for lang data updates
		$lang_dm_data = array(
			"module" => "reviews",
			"model" => "Reviews_type_model",
			"method_add" => "lang_dedicate_module_callback_add",
			"method_delete" => "lang_dedicate_module_callback_delete"
		);
		$this->CI->pg_language->add_dedicate_modules_entry($lang_dm_data);

		return;
	}
	
	/**
	 * Install rating types languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_install($langs_ids){
		if(empty($langs_ids)) return false;		
		return true;
	}

	/**
	 * Export rating types languages
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		//// arbitrary
		$arbitrary_return = array();		
		return array("arbitrary" => $arbitrary_return);
	}

	/**
	 * Uninstall rating types
	 */
	function _arbitrary_deinstalling(){
		
		$this->CI->load->model("Reviews_model");
		
		/// delete entries in dedicate modules
		$lang_dm_data["where"] = array(
			"module" => "reviews",
			"model" => "Reviews_type_model",
		);
		$this->CI->pg_language->delete_dedicate_modules_entry($lang_dm_data);	
		
		$fields = array();
		foreach ($this->CI->pg_language->languages as $id => $value){
			$fields[] = "values_".$value["id"];
		}
		$this->CI->Reviews_model->lang_dedicate_module_callback_delete($fields);
	}
}
