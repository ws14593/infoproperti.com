<?php

$install_lang["notification_auser_reviews_object"] = "Новый отзыв (для администрации)";
$install_lang["notification_auser_reviews_reply"] = "Комментарий к отзыву (для администрации)";
$install_lang["notification_user_reviews_object"] = "Новый отзыв";
$install_lang["notification_user_reviews_reply"] = "Комментарий к отзыву";
$install_lang["tpl_auser_reviews_object_content"] = "Здравствуйте, администратор!\n\nПоявился новый отзыв на сайте [domain]. Для просмотра зайдите в панель администратора > Обратная связь > Отзывы и проверьте вкладки \"Пользователи\" и \"Объявления\".\n\nС уважением,\n[name_from]";
$install_lang["tpl_auser_reviews_object_subject"] = "[domain] | Новый отзыв";
$install_lang["tpl_auser_reviews_reply_content"] = "Здравствуйте, администратор!\n\nПоявился новый комментарий к отзыву.\n\Отзыв: [review]\n\Комментарий: [comment]\n\nС уважением,\n[name_from]";
$install_lang["tpl_auser_reviews_reply_subject"] = "[domain] | Комментарий к отзыву";
$install_lang["tpl_user_reviews_object_content"] = "Здравствуйте, [to],\n\nОставлен новый отзыв.\n\nАвтор отзыва: [poster]\n\nСообщение: [message]\n\nС уважением,\n[name_from]";
$install_lang["tpl_user_reviews_object_subject"] = "[domain] | Новый отзыв [type] (ID=[object_id])";
$install_lang["tpl_user_reviews_reply_content"] = "Здравствуйте, [user],\n\nПоявился новый комментарий к Вашему отзыву.\n\nВаш отзыв: [review]\nКомментарий: [comment]\n\nС уважением,\n[name_from]";
$install_lang["tpl_user_reviews_reply_subject"] = "[domain] | Комментарий к Вашему отзыву";


