{if $spam_types_count > 0}
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan=2>{l i='stat_header_reviews' gid='reviews'}</th>
	</tr>
	{foreach item=item from=$spam_types}
	{assign var=stat_header value='stat_header_spam_'+$item.gid}
	{counter print=false assign=counter}
	<tr {if $counter%2}class="zebra"{/if}>
		<td class="first"><a href="{$site_url}admin/spam/alerts/{$item.gid}">{l i=$stat_header gid='spam'}</a></td>
		<td class="w30"><a href="{$site_url}admin/spam/alerts/{$item.gid}">{$item.obj_count} ({$item.obj_need_approve})</a></td>
	</tr>
	{/foreach}
</table>
{/if}
