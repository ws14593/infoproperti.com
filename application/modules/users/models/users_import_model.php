<?php
/**
* Users Import Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Users_import_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Transfer configuration
	 */
	private $fields_for_transfer = array();
	
	/**
	 * Constructor
	 *
	 * return Users_import_model object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model("Users_model");
		
		$this->fields_for_transfer = array(
			array("field_name"=>"id", "field_type"=>"int", "name"=>l("field_id", "users"), "required"=>false),
			array("field_name"=>"date_created", "field_type"=>"text", "name"=>l("field_date_created", "users"), "required"=>false),
			array("field_name"=>"date_modified", "field_type"=>"text", "name"=>l("field_date_modified", "users"), "required"=>false),
			array("field_name"=>"email", "field_type"=>"text", "name"=>l("field_email", "users"), "required"=>true),
			array("field_name"=>"unique_name", "field_type"=>"text", "name"=>l("field_unique_name", "users"), "required"=>true),
			array("fk"=>"user_type_str", "field_name"=>"user_type", "field_type"=>"select", "options"=>array("header"=>l("field_user_type", "users")), "required"=>true),
			array("field_name"=>"password", "field_type"=>"text", "name"=>l("field_password", "users"), "required"=>true),
			array("fk"=>"status_output", "field_name"=>"status", "field_type"=>"select", "options"=>array("header"=>l("field_status", "users")), "required"=>false),
			array("field_name"=>"confirm", "field_type"=>"int", "name"=>l("field_confirm", "users"), "required"=>false),
			array("field_name"=>"confirm_code", "field_type"=>"text", "name"=>l("field_confirm_code", "users"), "required"=>false),
			array("field_name"=>"fname", "field_type"=>"text", "name"=>l("field_fname", "users"), "required"=>true),
array("field_name"=>"sname", "field_type"=>"text", "name"=>l("field_sname", "users"), "required"=>false),
			array("field_name"=>"company_name", "field_type"=>"text", "name"=>l("field_name", "users"), "required"=>false),
			array("fk"=>"language_output", "field_name"=>"lang_id", "field_type"=>"select", "options"=>array("header"=>l("field_language", "users")), "required"=>false),
			array("fk"=>"currency_output", "field_name"=>"id_currency", "field_type"=>"select", "options"=>array("header"=>l("field_currency", "users")), "required"=>false),
//			array("field_name"=>"user_open_id", "field_type"=>"text", "name"=>l("field_open_id", "users"), "required"=>false),
			array("field_name"=>"user_logo", "field_type"=>"file", "name"=>l("field_user_logo", "users"), "required"=>false),
			array("fk"=>"group_output", "field_name"=>"group_id", "field_type"=>"select", "options"=>array("header"=>l("field_group", "users")), "required"=>false),
			array("field_name"=>"account", "field_type"=>"text", "name"=>l("field_account", "users"), "required"=>false),
			array("fk"=>"country", "field_name"=>"id_country", "field_type"=>"select", "options"=>array("header"=>l("field_country", "users")), "required"=>false),
			array("fk"=>"region", "field_name"=>"id_region", "field_type"=>"select", "options"=>array("header"=>l("field_region", "users")), "required"=>false),
			array("fk"=>"city", "field_name"=>"id_city", "field_type"=>"select", "options"=>array("header"=>l("field_city", "users")), "required"=>false),
			array("field_name"=>"phone", "field_type"=>"text", "name"=>l("field_phone", "users"), "required"=>false),
			array("field_name"=>"contact_email", "field_type"=>"text", "name"=>l("field_contact_email", "users"), "required"=>false),
			array("field_name"=>"contact_phone", "field_type"=>"text", "name"=>l("field_contact_phone", "users"), "required"=>false),
			array("field_name"=>"contact_info", "field_type"=>"text", "name"=>l("field_contact_info", "users"), "required"=>false),
			array("field_name"=>"address", "field_type"=>"text", "name"=>l("field_address", "users"), "required"=>false),
			array("field_name"=>"postal_code", "field_type"=>"text", "name"=>l("field_postal_code", "users"), "required"=>false),
			array("field_name"=>"web_url", "field_type"=>"text", "name"=>l("field_web_url", "users"), "required"=>false),
			array("field_name"=>"featured_end_date", "field_type"=>"text", "name"=>l("field_featured_end_date", "users"), "required"=>false),
			array("field_name"=>"show_logo_end_date", "field_type"=>"text", "name"=>l("field_show_logo_end_date", "users"), "required"=>false),
			array("fk"=>"working_days_str", "field_name"=>"working_days", "field_type"=>"select", "options"=>array("header"=>l("field_working_days", "users")), "required"=>false),
			array("fk"=>"working_hours_begin_text", "field_name"=>"working_hours_begin", "field_type"=>"select", "options"=>array("header"=>l("field_working_hours_begin", "users")), "required"=>false),
			array("fk"=>"working_hours_end_text", "field_name"=>"working_hours_end", "field_type"=>"select", "options"=>array("header"=>l("field_working_hours_end", "users")), "required"=>false),
			array("fk"=>"lunch_time_begin_text", "field_name"=>"lunch_time_begin", "field_type"=>"select", "options"=>array("header"=>l("field_lunch_time_begin", "users")), "required"=>false),
			array("fk"=>"lunch_time_end_text", "field_name"=>"lunch_time_end", "field_type"=>"select", "options"=>array("header"=>l("field_lunch_time_end", "users")), "required"=>false),
			array("field_name"=>"lat", "field_type"=>"text", "name"=>l("field_lat", "users"), "required"=>false),
			array("field_name"=>"lon", "field_type"=>"text", "name"=>l("field_lon", "users"), "required"=>false),
			array("fk"=>"agent_company.output_name", "field_name"=>"agent_company", "field_type"=>"select", "options"=>array("header"=>l("field_agent_company", "users")), "required"=>false),
			array("field_name"=>"agent_status", "field_type"=>"int", "name"=>l("field_agent_status", "users"), "required"=>false),
//			array("field_name"=>"agent_count", "field_type"=>"int", "name"=>l("field_agent_count", "users"), "required"=>false),
//			array("field_name"=>"listings_for_sale_count", "field_type"=>"int", "name"=>l("field_listings_for_sale_count", "users"), "required"=>false),
//			array("field_name"=>"listings_for_rent_count", "field_type"=>"int", "name"=>l("field_listings_for_rent_count", "users"), "required"=>false),
			array("field_name"=>"twitter", "field_type"=>"text", "name"=>l("field_twitter", "users"), "required"=>false),
			array("field_name"=>"facebook", "field_type"=>"text", "name"=>l("field_facebook", "users"), "required"=>false),
			array("field_name"=>"vkontakte", "field_type"=>"text", "name"=>l("field_vkontakte", "users"), "required"=>false),
//			array("field_name"=>"views", "field_type"=>"int", "name"=>l("field_views", "users"), "required"=>false),
//			array("field_name"=>"banned", "field_type"=>"int", "name"=>l("field_banned", "users"), "required"=>false),
		);
	}
	
	/**
	 * Callback get custom fields for module export
	 * @param boolean $is_export fields for export
	 * @return array
	 */
	public function callback_get_fields($is_export=true){
		foreach($this->fields_for_transfer as $field){
			if(isset($property_types[$field["section_gid"]])){
				$section = $property_types[$field["section_gid"]]["name"]."-";
			}else{
				$section = "";
			}
			switch($field["field_type"]){
				case "select":
					switch($field['field_name']){
						case 'user_type':
							$custom_fields[] = array("name" => 'user_type', "type"=>"text", "label"=>$section.$field["options"]["header"]." (text, code)");
						break;
						case 'id_country':
							$custom_fields[] = array('name' => 'country_code', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, code)');
						break;
						case 'id_region':
							$custom_fields[] = array('name' => 'region_code', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => 'id_region', 'type'=>'int', 'label'=>$section.$field['options']['header'].' (int, id)');
						break;
						case 'lang_id':
							$custom_fields[] = array('name' => 'language_gid', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, GUID)');
						break;
						case 'id_currency':
							$custom_fields[] = array('name' => 'currency_gid', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, GUID)');
						break;
						case 'working_days':
							$custom_fields[] = array('name' => 'working_days_code', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, code)');
						break;
						default:
							$custom_fields[] = array("name" => $field["field_name"], "type"=>"int", "label"=>$section.$field["options"]["header"]." (int, id)");
						break;
					}
					$custom_fields[] = array("fk"=>"field_editor.".$field["field_name"]."_output", "name" => $field["field_name"], "type"=>"text", "label"=>$section.$field["options"]["header"]." (text)");
				break;
				case "textarea":
					$custom_fields[] = array("name" => $field["field_name"], "type"=>"text", "label"=>$section.$field["name"]." (text)");
				break;
				case "file":
					$custom_fields[] = array("name" => $field["field_name"], "type"=>"text", "label"=>$section.$field["name"]." (url)");
					$custom_fields[] = array("name" => $field["field_name"], "type"=>"file", "label"=>$section.$field["name"]." (file)");
				break;
				default:
					$custom_fields[] = array("name" => $field["field_name"], "type"=>$field["field_type"], "label"=>$section.$field["name"]." (".$field["field_type"].")");
				break;
			}
		}	
		return $custom_fields;		
	}

	/**
	 * Callback import data
	 * @param array $filter_data search data
	 * @param array $relations relations between fields
	 * @param integer $user_id user identifier
	 * @param boolean $use_moderate use moderation
	 * @return array
	 */
	public function callback_import_data($data, $relations, $user_id=0, $use_moderate=false){
		$return = array("errors"=>array(), "data"=>array());
	
		foreach($relations as $i=>$relation){
			if($relation["type"] == "text"){
				foreach($this->fields_for_transfer as $field){
					if($relation["link"] == $field["field_name"]){
						if(isset($field["fk"]) && isset($data[$field["field_name"]])){
							$data = $this->get_fk_values($field["fk"], $field["field_name"], $data);
						}
						break;
					}	
				}
			}
		}
	
		if(isset($data['country_code']) && $data['country_code']){
			$this->CI->load->model('Countries_model');			
			$country = $this->CI->Countries_model->get_country($data['country_code']);
			if($country){
				$data['id_country'] = $data['country_code'];
			}else{
				$data['id_country'] = '';
			}
		}
		
		if(isset($data['region_code']) && $data['region_code']){
			$this->CI->load->model('Countries_model');			
			$region = $this->CI->Countries_model->get_region_by_code($data['region_code'], $data['id_country']);
			if($region){
				if(!$data['id_country']) $data['id_country'] = $region['country_code'];
				$data['id_region'] = $region['id'];
			}else{
				$data['id_region'] = 0;
			}
		}
	
		if(isset($data['language_gid']) && $data['language_gid']){
			foreach($this->CI->pg_language->languages as $lid => $lang_data){
				if($lang_data['code'] == $data['language_gid']){
					$data['lang_id'] = $lid;
					break;
				}
			}
		}
		
		if(isset($data['currency_gid']) && $data['currency_gid']){
			if($this->CI->pg_module->is_module_installed('payments')){
				$this->CI->load->model('payments/models/Payment_currency_model');
				$currencies = $this->CI->Payment_currency_model->get_currency_list();
				foreach($currencies as $currency_data){
					if($currency_data['gid'] == $data['currency_gid']){
						$data['id_currency'] = $currency_data['id'];
						break;
					}
				}
			}
		}
		
		if(isset($data['working_days_code']) && $data['working_days_code']){
			$data['working_days'] = explode('|', $data['working_days_code']);
		}
		
		if(isset($data["id_city"]) && $data["id_city"] && (!isset($data["id_region"]) || !$data["id_region"])){
			$this->CI->load->model("Countries_model");
			
			$city = $this->CI->Countries_model->get_city($data["id_city"]);
			if($city){
				if(!isset($data["id_country"]) || !$data["id_country"]) $data["id_country"] = $city["country_code"];
				$data["id_region"] = $city["id_region"];
			}
		}
		
		if(isset($data["id_region"]) && $data["id_region"] && (!isset($data["id_country"]) || !$data["id_country"])){
			$this->CI->load->model("Countries_model");
			
			$region = $this->CI->Countries_model->get_region($data["id_region"]);
			if($region){
				$data["id_country"] = $region["country_code"];				
			}
		}
		
		if(isset($data["password"])){
			$data["repassword"] = $data["password"];
		}
		
		if(!isset($data["status"])) $data["status"] = 1;

		if(isset($data['user_logo'])){
			$user_logo = $data['user_logo'];
			unset($data['user_logo']);
		}else{
			$user_logo = null;
		}
		
		if(!$user_id) $user_id = null;
		
		if(!$user_id && isset($data['id'])){
			$this->CI->Users_model->set_format_settings('use_format', false);
			$user = $this->CI->Users_model->get_user_by_id($data['id']);
			$this->CI->Users_model->set_format_settings('use_format', true);
			if($user){
				$user_id = $data['id'];
				unset($data['id']);
			}
		}		
	
		$data = $this->validate_data($user_id, $data);

		$validate_data = $this->CI->Users_model->validate_user($user_id, $data);
		if(!empty($validate_data["errors"])){
			$return["errors"] = $validate_data["errors"];
		}else{
			if($user_id && !is_null($user_logo)){
				$this->CI->Users_model->delete_logo($user_id);
			}
	
			$user_id = $this->CI->Users_model->save_user($user_id, $validate_data["data"]);
			$return["data"]["id"] = $user_id;
			
			if(!empty($user_logo)){
				$user_logo = (array)$user_logo;
				$this->CI->Users_model->save_local_user_logo($user_id, array_shift($user_logo), $use_moderate);
			}
		}
	
		return $return;
	}
	
	/**
	 * Return foreign key value
	 * @param string $fk foreign key name
	 * @param string $field_name field_name
	 * @param array $data import data
	 * @return mixed
	 */
	public function get_fk_values($fk, $field_name, $data){
		$this->CI->load->model("Countries_model");
		
		switch($fk){
			case 'user_type_str':
				$user_types = $this->CI->Users_model->get_user_types();
				
				$replace = false;
				$default_lang = $this->CI->pg_language->current_lang_id;
				
				foreach($user_types as $user_type){
					if($user_type == $data['user_type']){
						$data['user_type'] = $user_type;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($user_types as $user_type){
						if(l($user_type, 'users') == $data['user_type']){
							$data['user_type'] = $user_type;
							$replace = true;
							break;
						}
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						foreach($user_types as $user_type){
							if(l($user_type, 'users', $lid) == $data['user_type']){
								$data['user_type'] = $user_type;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace) $data['user_type'] = $this->CI->Users_model->get_user_type_default();			
			break;
			case 'status_output':
				$replace = false;
				$default_lang = $this->CI->pg_language->current_lang_id;
				if(l('active_user', 'users') == $data['status']){
					$data['status'] = 1;
					$replace = true;
				}elseif(l('inactive_user', 'users') == $data['status']){
					$data['status'] = 0;
					$replace = true;
				}else{
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						if(l('active_user', 'users', $lid) == $data['status']){
							$data['status'] = 1;
							$replace = true;
						}elseif(l('inactive_user', 'users', $lid) == $data['status']){
							$data['status'] = 0;
							$replace = true;
						}
					}
				}
				if(!$replace) $data['status'] = 0;
			break;
			case 'language_output':
				foreach($this->CI->pg_language->languages as $lid => $lang_data){
					if($lang_data['name'] == $data['lang_id']){
						$data['lang_id'] = $lid;
						break;
					}
				}
			break;
			case 'currency_output':
				if($this->CI->pg_module->is_module_installed('payments')){
					$this->CI->load->model('payments/models/Payment_currency_model');
					$currencies = $this->CI->Payment_currency_model->get_currency_list();
					foreach($currencies as $currency_data){
						if($currency_data['name'] == $data['id_currency']){
							$data['id_currency'] = $currency_data['id'];
							break;
						}
					}
				}
			break;
			case 'group_output':
				$this->CI->load->model('users/models/Groups_model');
				$group = $this->CI->Groups_model->get_group_by_name($data["group_id"]);
				if($group){
					$data["group_id"] = $group["id"];
				}else{
					$data["group_id"] = 0;
				}
			break;
			case "country":
				$country = $this->CI->Countries_model->get_country_by_name($data["id_country"]);
				if($country){
					$data["id_country"] = $country["code"];
				}else{
					$data["id_country"] = 0;
				}
			break;
			case "region":
				$region = $this->CI->Countries_model->get_region_by_name($data["id_region"]);
				if($region){
					if(!$data["id_country"]) $data["id_country"] = $region["country_code"];
					$data["id_region"] = $region["id"];
				}else{
					$data["id_region"] = 0;
				}
			break;
			case "region_code":
				$region = $this->CI->Countries_model->get_region_by_code($data["id_region"]);
				if($region){
					if(!$data["id_country"]) $data["id_country"] = $region["country_code"];
					$data["id_region"] = $region["id"];
				}else{
					$data["id_region"] = 0;
				}
			break;
			case "city":
				$city = $this->CI->Countries_model->get_city_by_name($data["id_city"]);
				if($city){
					if(!$data["id_country"]) $data["id_country"] = $city["country_code"];
					if(!$data["id_region"]) $data["id_region"] = $city["id_region"];
					$data["id_city"] = $city["id"];
				}else{
					$data["id_city"] = 0;
				}
			break;
			case 'working_days_str':
				if(empty($data['working_days'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$weekdays = ld('weekday-names', 'start');
				$working_days = explode(', ', $data['working_days']);
				$data['working_days'] = array();
				foreach($working_days as $index=>$day){
					foreach($weekdays['option'] as $key=>$value){
						if($day == $value){
							$data['working_days'][] = $key;
							unset($working_days[$index]);
							break;
						}
					}
				}
				if(!empty($working_days)){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$weekdays = ld('weekday-names', 'start', $lid);
						foreach($working_days as $index=>$day){
							foreach($weekdays['option'] as $key=>$value){
								if($day == $value){
									$data['working_days'][] = $key;
									unset($working_days[$index]);
									break;
								}
							}
							if(empty($working_days)) break;
						}
						if(empty($working_days)) break;
					}
				}
			break;
			case 'working_hours_begin_text':
				if(empty($data['working_hours_begin'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$dayhours = ld('dayhour-names', 'start');
				foreach($dayhours['option'] as $key=>$value){
					if($value == $data['working_hours_begin']){
						$data['working_hours_begin'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$dayhours = ld('dayhour-names', 'start', $lid);
						foreach($dayhours['option'] as $key=>$value){
							if($value == $data['working_hours_begin']){
								$data['working_hours_begin'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['working_hours_begin'] = 0;
				}
			break;
			case 'working_hours_end_text':
				if(empty($data['working_hours_end'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$dayhours = ld('dayhour-names', 'start');
				foreach($dayhours['option'] as $key=>$value){
					if($value == $data['working_hours_end']){
						$data['working_hours_end'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$dayhours = ld('dayhour-names', 'start', $lid);
						foreach($dayhours['option'] as $key=>$value){
							if($value == $data['working_hours_end']){
								$data['working_hours_end'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['working_hours_end'] = 0;
				}
			break;
			case 'lunch_time_begin_text':
				if(empty($data['lunch_time_begin'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$dayhours = ld('dayhour-names', 'start');
				foreach($dayhours['option'] as $key=>$value){
					if($value == $data['lunch_time_begin']){
						$data['lunch_time_begin'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$dayhours = ld('dayhour-names', 'start', $lid);
						foreach($dayhours['option'] as $key=>$value){
							if($value == $data['lunch_time_begin']){
								$data['lunch_time_begin'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['lunch_time_begin'] = 0;
				}
			break;
			case 'lunch_time_end_text':
				if(empty($data['lunch_time_end'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$dayhours = ld('dayhour-names', 'start');
				foreach($dayhours['option'] as $key=>$value){
					if($value == $data['lunch_time_end']){
						$data['lunch_time_end'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$dayhours = ld('dayhour-names', 'start', $lid);
						foreach($dayhours['option'] as $key=>$value){
							if($value == $data['lunch_time_end']){
								$data['lunch_time_end'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['lunch_time_end'] = 0;
				}
			break;
			case 'agent_company.output_name':
				$users = $this->CI->Users_model->get_users_list_by_filters(array("name"=>$data["agent_company"]), 1, 1);
				if(!empty($users)){
					$data["agent_company"] = $users[0]["id"];
				}else{
					$data["agent_company"] = 0;
				}
			break;
			default:
				
			break;
		}
		return $data;
	}
	
	/**
	 * Fill default values
	 * @return array
	 */
	public function validate_data($user_id, $data){
		if($user_id) return $data;
		
		$is_default = false;
		
		if(!isset($data['user_type']) || empty($data['user_type'])){
			$user_types = $this->CI->Users_model->get_user_types();
			$data['user_type'] = !empty($user_types) ? current($user_types) : '';
			$is_default = true;
		}

		if(!isset($data['email']) || empty($data['email'])){
			$data['email'] = 'default@site.local';
			$is_default = true;
		}
	
		if(!isset($data['password']) || empty($data['password'])){
			$data['password'] = $data['repassword'] = substr(md5(date("Y-m-d H:i:s") . $user_data["email"]), 0, 6);
			$is_default = true;
		}
		
		if(!isset($data['phone']) || empty($data['phone'])){
			$this->CI->load->helper("start");
			$phone_format = get_phone_format();
			if($phone_format){
				$data['phone'] = $phone_format;
			}else{
				$data['phone'] = '111-111-111';
			}
			$is_default = true;
		}
		
		if(!isset($data['fname']) || empty($data['fname'])){
			$data['fname'] = 'User';
			$is_default = true;
		}
		
		if(!isset($data['sname']) || empty($data['sname'])){
			$data['sname'] = 'User';
			$is_default = true;
		}
		
		if($data['user_type'] == 'company' && (!isset($data['company_name']) || empty($data['company_name']))){
			$data['company_name'] = 'Company (by default)';
			$is_default = true;
		}
		
		if($is_default) $data['status'] = 0;
		
		return $data;
	}
	
	/**
	 * Return default user
	 * @return array
	 */
	public function get_default_user_id(){
		$user_id = $this->CI->pg_module->get_module_config("users", "import_default_user");
		if($user_id) $user = $this->Users_model->get_user_by_id($user_id);
		if($user){
			$user_id = $user['id'];
		}else{
			$data = array();
			$data = $this->validate_data(null, $data);
			$validate_data = $this->CI->Users_model->validate_user(null, $data);
			if(!empty($validate_data['errors'])) return 0;
			$user_id = $this->CI->Users_model->save_user(null, $validate_data['data']);
			$this->CI->pg_module->set_module_config("users", "import_default_user", $user_id);
		}
		return $user_id;
	}
}
