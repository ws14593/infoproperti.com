	<!-- user_info right panel -->
	<div class="user_info">
		<h2>{l i='header_provided_by' gid='listings'}</h2>
		{if $user.status}
		{capture assign='user_actions'}{strip}
			{block name='button_contact' module='mailbox' user_id=$user.id user_type=$user.user_type}
			{if $show_all_listings}{block name='user_listings_button' module='listings' user=$user}{/if}
			{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id is_owner=$is_user_owner}
			{block name='mark_as_spam_block' module='spam' object_id=$user.id type_gid='users_object' is_owner=$is_user_owner}
		{/strip}{/capture}
		{if $user_actions}
		<div class="actions noPrint">
			{$user_actions}
		</div>
		{/if}
		{/if}
		
		<div class="clr"></div>	
		<div class="image">
			{capture assign='user_logo'}
				<img src="{$user.media.user_logo.thumbs.big}" title="{$user.output_name|escape}" alt="{$user.output_name|truncate:30|escape}">
			{/capture}
			{if $user.status}
			<a href="{seolink module='users' method='view' data=$user}">{$user_logo}</a>{else}{$user_logo}{/if}
		</div>
		<h3>{if $user.status}<a href="{seolink module='users' method='view' data=$user}">{$user.output_name|truncate:30}</a>{else}{$user.output_name|truncate:30}{/if}, {ld_option i='user_type' gid='users' option=$user.user_type}</h3>
		{if $user.status}
		<div id="user_block">
			<div class="tabs tab-size-15 noPrint">
				<ul id="user_sections">
					<li id="ui_contacts" sgid="contacts" class="active"><a href="{$site_url}listings/user/{$user.id}/{$sgid}">{l i='filter_section_info' gid='listings'}</a></li>
					{if $user.user_type eq 'company'}<li id="ui_map_info" sgid="map_info" class="{if $section_gid eq 'map_info'}active{/if}"><a href="{$site_url}listings/user/{$user.id}/{$sgid}">{l i='filter_section_map' gid='listings'}</a></li>{/if}
					{if $show_contact_form}{depends module=contact}<li id="ui_contact" sgid="contact" class="{if $section_gid eq 'contact'}active{/if}"><a href="{$site_url}listings/user/{$user.id}/{$sgid}">{l i='filter_section_contact' gid='listings'}</a></li>{/depends}{/if}
				</ul>
			</div>
			<div id="content_ui_contacts" class="view-section print_block">
			    {block name=view_user_block module=users user=$user template='small'}
				<p><a href="{$site_url}contact_us/index/" class="underline bold">Click here</a>, if you are the owner of this venture for customization and full benefits of our services.</p>
			</div>
			<div id="content_ui_contact" class="view-section hide noPrint">{block name=show_contact_form module=contact user_id=$user.id}</div>
			{if $user.user_type eq 'company'}
			    <div id="content_ui_map_info" class="view-section{if $section_gid ne 'map'} hide{/if} noPrint">
				{block name=show_default_map module=geomap id_user=$user_id markers=$markers settings=$map_settings width='300' height='300' only_load_scripts=1}
			    </div>
			{/if}
		</div>
		
		{js module=users file='users-menu.js'}
		{js module=users_services file='available_view.js'}
		<script>{literal}
			var rMenu;
			$(function(){
				rMenu = new usersMenu({
					siteUrl: '{/literal}{$site_url}{literal}',
					idUser: '{/literal}{$user.id}{literal}',
					tabPrefix: 'ui',
					CurrentSection: 'ui_contacts',
					template: 'small',
					{/literal}{depends module=users_services}available_view: new available_view(),{/depends}{literal}
				});
			});
		{/literal}</script>		
		{/if}
	</div>	
