{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		{assign var='header_my_agents' value='header_my_agents_'+$action}
		<h1>{l i=$header_my_agents gid='users'}</h1>

		<div class="search-links">
			{l i='link_search_in_my_agents' gid='users'}
			<div class="edit_block">
				<form action="" method="post" enctype="multipart/form-data" id="agent_search_form">
					<div class="r"><input type="text" name="name" value="{$page_data.name|escape}" id="agent_search" autocomplete="off"></div>
				</form>
			</div>
		</div>

		<div id="search_my_agents_form" class="hide"></div>

		<div id="agents_block">{$block}</div>
		{js module=users file='users-list.js'}
		<script type='text/javascript'>{literal}
		$(function(){
			new usersList({
				siteUrl: '{/literal}{$site_url}{literal}',
				viewUrl: 'users/agents/{/literal}{$action}{literal}',
				viewAjaxUrl: 'users/ajax_my_agents/{/literal}{$action}{literal}',
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
				listBlockId: 'agents_block'
			});
		});
	{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}


