<div class="view-user">
{l i='no_information' gid='users' assign='no_info_str'}
{l i='not_access' gid='users' assign='not_access_str'}
{l i='please_buy' gid='users' assign='buy_str'}
{if $user.user_type == 'agent' && $user.agent_status}
    <div class="r">
		<div class="f">{l i='company' gid='users'}: </div>
		<div class="v"><a href="{seolink module='users' method='view' data=$user.company}">{$user.company.output_name}</a></div>
    </div>
{/if}

<div class="r">
    <div class="f">{l i='field_facebook' gid='users'}: </div>
    <div class="v">
		{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		{elseif !$user.facebook}<font class="gray_italic">{$no_info_str}</font>
		{elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		{else}{$user.facebook}
		{/if}
    </div>
</div>
<div class="r">
    <div class="f">{l i='field_twitter' gid='users'}: </div>
    <div class="v">
		{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		{elseif !$user.twitter}<font class="gray_italic">{$no_info_str}</font>
		{elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		{else}{$user.twitter}
		{/if}
    </div>
</div>

<?php /* disable for now
<div class="r">
    <div class="f"> </div>
    <div class="v">
		{if $user.no_access_contact}<font class="gray_italic"></font>
		{elseif !$user.vkontakte}<font class="gray_italic"></font>
		{elseif !$user.is_contact}<font class="gray_italic"></font>
		{else}
		{/if}
    </div>
</div>
*/ ?>
{switch from=$user.user_type}
	{case value='private'}
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">
		   	{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.contact_phone}{$user.phone}
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.contact_phone}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_email' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.contact_email}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.contact_email}
		    {/if}
		</div>
	</div>
    <?php /* movewd to view_block.tpl
    <div class="r">
        <div class="f">{l i='field_description' gid='users'}: </div>
        <div class="v">
            {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
            {elseif !$user.contact_info}<font class="gray_italic">{$no_info_str}</font>
            {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
            {elseif $template eq 'small'}{$user.contact_info|truncate:55}
            {else}{$user.contact_info}
            {/if}
        </div>
	</div>
	*/ ?>

	{case value='company'}
	<div class="r">
		<div class="f">{l i='field_region' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.location}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.location}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.contact_phone}{$user.phone}
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.contact_phone}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_email' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.contact_email}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.contact_email}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_address' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.address}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.address}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_postal_code' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.postal_code}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.postal_code}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_web_url' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.web_url}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.web_url}
		    {/if}
		</div>
	</div>
	<div class="r">
	    <div class="f">{l i='field_working_days' gid='users'}: </div>
	    <div class="v">
			{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.working_days_str}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.working_days_str}
		    {/if}
		</div>
	</div>
	<div class="r">
	    <div class="f">{l i='field_working_hours' gid='users'}: </div>
	    <div class="v">
			{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.working_hours_str}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.working_hours_str}
		    {/if}
		</div>
	</div>
	<div class="r">
	    <div class="f">{l i='field_lunch_time' gid='users'}: </div>
	    <div class="v">
			{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.lunch_time_str}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.lunch_time_str}
		    {/if}
		</div>
	</div>
	
	{case value='agent'}
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.contact_phone}{$user.phone}
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.contact_phone}
		    {/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_email' gid='users'}:</div>
		<div class="v">
		    {if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user.contact_email}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user.contact_email}
		    {/if}
		</div>
	</div>
{/switch}

{if !$user.is_contact && !$user.no_access_contact}
<div class="buy-box"><input type="button" value="{l i='btn_activate_contact' gid='services' type='button'}" name="contacts_btn"></div>
{/if}

</div>
