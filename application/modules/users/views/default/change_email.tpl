{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">

	<div class="content-block">

		<h1>{l i='text_change_email' gid='users'}</h1>
		
		<div class="content-value">
	
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f">{l i='field_current_email' gid='users'}: <b>{$data.email}</b></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_new_email' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="text" name="email" value="" autocomplite="off"></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="{l i='btn_send' gid='start'  type='button'}" name="btn_save"></div>
				</div>
			</form>
			
			</div>
			<div class="edit_block">{helper func_name=show_social_networking_link module=users_connections}</div>
		</div>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

