{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_menu'}

<div class="actions">
	<ul>
		{if $agent_status}
		<li id="delete_all"><div class="l"><a href="{$site_url}admin/users/agents_delete/{$company_id}">{l i='btn_delete' gid='start'}</a></div></li>
		{else}
		<li id="approve_all"><div class="l"><a href="{$site_url}admin/users/agents_request/{$company_id}/1">{l i='btn_approve' gid='start'}</a></div></li>
		<li id="decline_all"><div class="l"><a href="{$site_url}admin/users/agents_request/{$company_id}/0">{l i='btn_decline' gid='start'}</a></div></li>
		{/if}
	</ul>
	&nbsp;
</div>

<form id="agents_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w20"><input type="checkbox" id="grouping_all"></th>
		<th class="w100"><a href="{$sort_links.name}"{if $order eq 'name'} class="{$order_direction|lower}"{/if}>{l i='field_name' gid='users'}</a></th>
		<th class="w100"><a href="{$sort_links.email}"{if $order eq 'email'} class="{$order_direction|lower}"{/if}>{l i='field_email' gid='users'}</a></th>
		<th class="w100"><a href="{$sort_links.phone}"{if $order eq 'phone'} class="{$order_direction|lower}"{/if}>{l i='field_phone' gid='users'}</a></th>
		<th class="w100"><a href="{$sort_links.lsitings_count}"{if $order eq 'listings_count'} class="{$order_direction|lower}"{/if}>{l i='field_listings_count gid='users'}</a></th>
		<th class="w50"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_date_created' gid='users'}</a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	{foreach item=item from=$agents}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>				
			<td class="center"><input type="checkbox" name="ids[]" class="grouping" value="{$item.id}" /></td>
			<td>{$item.output_name|truncate:100}</td>
			<td>{$item.email|truncate:50}</td>
			<td>{$item.phone}</td>
			<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
			<td class="icons">
				{if $agent_status}
				<a href="{$site_url}admin/users/agents_delete/{$company_id}/{$item.id}" onclick="javascript: if(!confirm('{l i='note_agent_delete' gid='users' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_agent_delete' gid='users' type='button'}" title="{l i='link_agent_delete' gid='users' type='button'}"></a>
				{else}
				<a href="{$site_url}admin/users/agents_approve/{$company_id}/1/{$item.id}"><img src="{$site_root}{$img_folder}icon-approve.png" width="16" height="16" border="0" alt="{l i='link_agent_approve' gid='users' type='button'}" title="{l i='link_agent_approve' gid='users' type='button'}"></a>
				<a href="{$site_url}admin/users/agents_decline/{$company_id}/0/{$item.id}"><img src="{$site_root}{$img_folder}icon-decline.png" width="16" height="16" border="0" alt="{l i='link_agent_decline' gid='users' type='button'}" title="{l i='link_agent_decline' gid='users' type='button'}"></a>
				{/if}
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="6" class="center">{l i='no_agents' gid='users'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}

<script type="text/javascript">{literal}
var reload_link = "{/literal}{$site_url}admin/users/agents/{$company_id}/{$agent_status}{literal}";
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input.grouping').attr('checked', 'checked');
		}else{
			$('input.grouping').removeAttr('checked');
		}
	});
		
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input[type=checkbox].grouping').attr('checked', 'checked');
		}else{
			$('input[type=checkbox].grouping').removeAttr('checked');
		}
	});
	{/literal}{if $agent_status}{literal}
	$(#delete_all').bind('click', function(){
		if(!$('input[type=checkbox].grouping').is(':checked')) return false; 
		if(!confirm('{/literal}{l i='note_agents_delete_all' gid='users' type='js'}{literal}')) return false;
		$('#agents_form').attr('action', $(this).find('a').attr('href')).submit();		
		return false;
	});
	{/literal}{else}{literal}
	$(#approve_all,#decline_all').bind('click', function(){
		if(!$('input[type=checkbox].grouping').is(':checked')) return false; 
		$('#agents_form').attr('action', $(this).find('a').attr('href')).submit();		
		return false;
	});
	{/literal}{/if}{literal}

});
function reload_this_page(value){
	var link = reload_link + value + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}
