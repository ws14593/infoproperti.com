{include file="header.tpl"}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/users/deactivated_alerts_delete/{$data.id}" onclick="javascript: if(!confirm('{l i='note_deactivated_alerts_delete' gid='users' type='js'}')) return false;">{l i='btn_delete' gid='start'}</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_deactivated_alerts_show' gid='users'}</div>
	<div class="row">
		<div class="h">{l i='field_deactivated_alert_name' gid='users'}: </div>
		<div class="v">{$data.name}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_deactivated_alert_email' gid='users'}: </div>
		<div class="v">{$data.email}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_deactivated_alert_phone' gid='users'}: </div>
		<div class="v">{$data.phone}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_deactivated_alert_reason' gid='users'}: </div>
		<div class="v">{$data.reason}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_deactivated_message' gid='users'}: </div>
		<div class="v">{$data.message}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_deactivated_alert_date_add' gid='users'}: </div>
		<div class="v">{$data.date_add|date_format:$date_format}</div>
	</div>
</div>
<a class="cancel" href="{$site_url}admin/users/deactivated_alerts/{$filter}">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
<script>{literal}
	$(function(){
		$("div.row:odd").addClass("zebra");
	});
{/literal}</script>
{include file="footer.tpl"}
