	{ld i='user_type' gid='users' assign='user_types'}
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first">{l i='stat_header_users' gid='users'}</th>
		{foreach item=item from=$user_types.option}
		<th class="center">{$item}</th>
		{/foreach}
	</tr>
	{if $stat_users.index_method}
	<tr>
		<td class="first w150"><a href="{$site_url}admin/users/index/">{l i='stat_header_all' gid='users'}</a></td>
		{foreach item=item key=key from=$user_types.option}
		<td class="w150 center"><a href="{$site_url}admin/users/index/{$key}/">{$stat_users[$key].all}</a></td>
		{/foreach}
	</tr>
	<tr class="zebra">
		<td class="first w150"><a href="{$site_url}admin/users/index/active">{l i='stat_header_active' gid='users'}</a></td>
		{foreach item=item key=key from=$user_types.option}
		<td class="w150 center"><a href="{$site_url}admin/users/index/{$key}/active">{$stat_users[$key].active}</a></td>
		{/foreach}
	</tr>
	<tr>
		<td class="first w150"><a href="{$site_url}admin/users/index/not_active">{l i='stat_header_blocked' gid='users'}</a></td>
		{foreach item=item key=key from=$user_types.option}
		<td class="w150 center"><a href="{$site_url}admin/users/index/{$key}/not_active">{$stat_users[$key].blocked}</a></td>
		{/foreach}
	</tr>
	<tr class="zebra">
		<td class="first w150"><a href="{$site_url}admin/users/index/not_confirm">{l i='stat_header_unconfirmed' gid='users'}</a></td>
		{foreach item=item key=key from=$user_types.option}
		<td class="w150 center"><a href="{$site_url}admin/users/index/{$key}/not_confirm">{$stat_users[$key].unconfirm}</a></td>
		{/foreach}
	</tr>
	{/if}
	</table>
	<br>
