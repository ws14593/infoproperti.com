{include file="header.tpl" load_type='editable|ui'}
{js file='admin-multilevel-sorter.js'}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_settings_menu'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/users/deactivated_reason_edit/{$current_lang_id}">{l i='btn_deactivated_reason_create' gid='users'}</a></div></li>
		<li><div class="l"><a href="#" onclick="javascript: mlSorter.update_sorting(); return false;">{l i='btn_deactivated_reason_resort' gid='users'}</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="menu-level3">
	<ul>
		{foreach item=item key=lang_id from=$langs}
		<li class="{if $lang_id eq $current_lang_id}active{/if}"><a href="{$site_url}admin/users/deactivated_reasons/{$lang_id}">{$item.name}</a></li>
		{/foreach}
	</ul>
	&nbsp;
</div>
<div class="filter-form" id="ds_items">
	<ul name="parent_0" class="sort connected" id="clsr0ul">
		{foreach item=item key=key from=$reference.option}
		<li id="item_{$key}">
			<div class="icons">
				<a href="{$site_url}admin/users/deactivated_reason_edit/{$current_lang_id}/{$key}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" alt="{l i='link_reasons_edit' gid='users' type='button'}" title="{l i='link_reasons_edit' gid='users' type='button'}"></a>
				<a href='#' onclick="if (confirm('{l i='note_reasons_delete' gid='spam' type='js'}')) mlSorter.deleteItem('{$key}');return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" alt="{l i='link_reasons_delete' gid='users' type='button'}" title="{l i='link_reasons_delete' gid='users' type='button'}"></a>
			</div>
			<div class="editable" id="{$key}">{$item|default:'&nbsp;'}</div>
		</li>
		{/foreach}
	</ul>
</div>
<script>{literal}
	var mlSorter;
	$(function(){
		mlSorter = new multilevelSorter({
			siteUrl: '{/literal}{$site_url}{literal}',
			itemsBlockID: 'pages',
			urlSaveSort: 'admin/users/ajax_deactivated_reason_save_sorter/',
			urlDeleteItem: 'admin/users/ajax_deactivated_reason_delete/',
		});
	});
{/literal}</script>
{include file="footer.tpl"}
