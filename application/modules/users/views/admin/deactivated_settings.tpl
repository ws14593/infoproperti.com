{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_settings_menu'}
<div class="actions">&nbsp;</div>
<form method="post" action="{$site_url}admin/users/deactivated_settings" name="save_form">
	<div class="edit-form n150">		
		<div class="row header">{l i='admin_header_deactivated_settings' gid='users'}</div>
		<div class="row">
			<div class="h"><label for="deactivated_send_alert">{l i='field_deactivated_send_alert' gid='users'}</label>: </div>
			<div class="v">
				<input type="hidden" name="deactivated_send_alert" value="0" /> 
				<input type="checkbox" name="deactivated_send_alert" value="1" id="deactivated_send_alert" {if $data.deactivated_send_alert}checked="checked"{/if} /> 
			</div>
		</div>		
		<div class="row zebra">
			<div class="h"><label for="deactivated_send_mail">{l i='field_deactivated_send_mail' gid='users'}</label>: </div>
			<div class="v">
				<input type="hidden" name="deactivated_send_mail" value="0" /> 
				<input type="checkbox" name="deactivated_send_mail" value="1" id="deactivated_send_mail" {if $data.deactivated_send_mail}checked="checked"{/if} /> 
				&nbsp;&nbsp;
				{l i='field_deactivated_email' gid='users'}
				<input type="text" name="deactivated_email" value="{$data.deactivated_email|escape}" id="deactivated_email_row" {if !$data.deactivated_send_mail}disabled{/if} /> 
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$('#deactivated_send_mail').bind('change', function(){
		if(this.checked){
			$('#deactivated_email_row').removeAttr('disabled');
		}else{
			$('#deactivated_email_row').attr('disabled', 'disabled');
		}
	});
});
{/literal}</script>
{include file="footer.tpl"}
