function usersMap(optionArr){
	this.properties = {
		siteUrl: '',
		mapAjaxUrl: 'users/ajax_users',
		
		order: 'date_created',
		orderDirection: 'DESC',
		page: 1,
		
		mapBlockId: 'users_map',
		sectionId: 'users_sections',
		errorObj: new Errors(),
		tIds: []
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
	}
	
	this.init_links = function(){
		$('#' + _self.properties.sectionId + ' li').bind('click', function(){
			var id = $(this).attr('id');
			$('#' + _self.properties.sectionId + ' li').removeClass('active');
			$('#'+id).addClass('active');
			_self.properties.userType = $('#'+id).attr('sgid');
			_self.properties.page = 1;
			_self.loading_map();
			return false;
		});
		
		if(_self.properties.tIds.length){
			for(var index in _self.properties.tIds){
				var id = _self.properties.tIds[index];
				$('#'+id+' select').live('change', function(){
					_self.properties.order = $(this).val();
					_self.loading_map();
					return false;
				});
				$('#'+id+' input[name=sorter_btn]').live('click', function(){
					if(_self.properties.orderDirection == 'ASC'){
						_self.properties.orderDirection = 'DESC';
					}else{
						_self.properties.orderDirection = 'ASC';
					}
					_self.loading_map();
					return false;
				});
				$('#'+id+'>.pages a[data-page]').live('click', function(){
					_self.properties.page = $(this).attr('data-page');
					_self.loading_map();
					return false;
				});				
			}
		}
	}
	
	this.loading_map = function(url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.mapAjaxUrl + '/';
			if(_self.properties.userType) url += _self.properties.userType + '/';
			if(_self.properties.order) url += _self.properties.order + '/';
			if(_self.properties.orderDirection) url += _self.properties.orderDirection + '/';
			url +=  _self.properties.page;
		}
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			success: function(data){
				$('#'+_self.properties.mapBlockId).append(data);
			}
		});
	}
	
	this.loading_post_map = function(post_data, url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.viewAjaxUrl;
		}
		$.ajax({
			url: url, 
			type: 'POST',
			data: post_data,
			cache: false,
			success: function(data){			
				$('#'+_self.properties.mapBlockId).append(data);
			}
		});
	}
		
	_self.Init(optionArr);
}
