DROP TABLE IF EXISTS `[prefix]groups`;
DROP TABLE IF EXISTS `[prefix]users`;
DROP TABLE IF EXISTS `[prefix]user_connections`;
DROP TABLE IF EXISTS `[prefix]user_account_list`;
DROP TABLE IF EXISTS `[prefix]user_services`;
DROP TABLE IF EXISTS `[prefix]user_deactivated_alerts`;
DROP TABLE IF EXISTS `[prefix]user_profile_visitors`;
