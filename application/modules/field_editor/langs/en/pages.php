<?php

$install_lang["admin_header_field_add"] = "Add new field";
$install_lang["admin_header_field_change"] = "Change field data";
$install_lang["admin_header_fields_list"] = "References management";
$install_lang["admin_header_form_add"] = "Add new form";
$install_lang["admin_header_form_change"] = "Change form data";
$install_lang["admin_header_forms_list"] = "References management";
$install_lang["admin_header_section_add"] = "Add new section";
$install_lang["admin_header_section_change"] = "Change section references";
$install_lang["admin_header_section_list"] = "References management";
$install_lang["error_editor_type_empty"] = "Editor type is empty";
$install_lang["error_empty_search_name"] = "Search name is empty";
$install_lang["error_field_code_exists"] = "Field with such code already exists";
$install_lang["error_field_code_incorrect"] = "Invalid field code";
$install_lang["error_field_length_less_than"] = "Field length less then [length]";
$install_lang["error_field_length_more_than"] = "Field length more then [length]";
$install_lang["error_field_name_empty"] = "Field name is empty";
$install_lang["error_field_option_name_empty"] = "Option name is empty";
$install_lang["error_field_type_empty"] = "Field type is empty";
$install_lang["error_form_code_incorrect"] = "Keyword is empty";
$install_lang["error_form_editor_type_incorrect"] = "Form content type is required";
$install_lang["error_form_name_incorrect"] = "Name is required";
$install_lang["error_section_code_exists"] = "Section with such code already exists";
$install_lang["error_section_code_incorrect"] = "Invalid section code";
$install_lang["error_section_empty"] = "Section keyword is empty";
$install_lang["error_section_name_empty"] = "Section name is empty";
$install_lang["field_checkbox_by_default"] = "By default";
$install_lang["field_comment"] = "Comment";
$install_lang["field_field_name"] = "Field";
$install_lang["field_field_type"] = "Field type";
$install_lang["field_form_type"] = "Form content type";
$install_lang["field_fts"] = "Use in fulltext search";
$install_lang["field_gid"] = "Keyword";
$install_lang["field_name"] = "Name";
$install_lang["field_section_data"] = "Section";
$install_lang["field_section_name"] = "Section";
$install_lang["field_select_empty_option"] = "Add '...' option";
$install_lang["field_select_options"] = "Options";
$install_lang["field_select_search_type"] = "Choose type";
$install_lang["field_select_search_type_many"] = "Multiple choice";
$install_lang["field_select_search_type_one"] = "Single choice";
$install_lang["field_select_view_type"] = "Field output type";
$install_lang["field_select_view_type_checkbox"] = "Checkboxes";
$install_lang["field_select_view_type_header"] = "View type";
$install_lang["field_select_view_type_multi"] = "Multi-select";
$install_lang["field_select_view_type_radio"] = "Radio buttons";
$install_lang["field_select_view_type_select"] = "Select";
$install_lang["field_select_view_type_slider"] = "Slider";
$install_lang["field_text_by_default"] = "Default value";
$install_lang["field_text_format"] = "Value output format";
$install_lang["field_text_max_char"] = "Max length";
$install_lang["field_text_min_char"] = "Min length";
$install_lang["field_text_search_type"] = "Search type";
$install_lang["field_text_template"] = "Value template";
$install_lang["field_text_view_type_exact"] = "Exact search";
$install_lang["field_text_view_type_header"] = "Search settings";
$install_lang["field_text_view_type_like"] = "Occurrence of the text";
$install_lang["field_text_view_type_range"] = "Range of numbers";
$install_lang["field_textarea_by_default"] = "Default value";
$install_lang["field_textarea_max_char"] = "Max length (0-unlimited)";
$install_lang["field_textarea_min_char"] = "Min length";
$install_lang["filter_section"] = "Type of section";
$install_lang["form_name"] = "Form";
$install_lang["header_add_form_field"] = "Add field into form";
$install_lang["header_add_form_section"] = "Add section into form";
$install_lang["header_add_select_option"] = "Add option";
$install_lang["header_change_field_settings"] = "Change field settings";
$install_lang["header_change_select_option"] = "Change option";
$install_lang["header_edit_form_section"] = "Edit section";
$install_lang["link_add_field"] = "Add new field";
$install_lang["link_add_form"] = "Add new form";
$install_lang["link_add_form_field"] = "Add field into form";
$install_lang["link_add_form_section"] = "Add section into form";
$install_lang["link_add_new_option"] = "Add new option";
$install_lang["link_add_section"] = "Add new section";
$install_lang["link_default_option"] = "Set default option";
$install_lang["link_delete_field"] = "Delete field";
$install_lang["link_delete_form"] = "Delete form";
$install_lang["link_delete_option"] = "Delete option";
$install_lang["link_delete_section"] = "Delete section";
$install_lang["link_edit_field"] = "Edit field";
$install_lang["link_edit_form"] = "Edit form";
$install_lang["link_edit_form_fields"] = "Edit form fields";
$install_lang["link_edit_option"] = "Edit option";
$install_lang["link_edit_section"] = "Edit section";
$install_lang["link_save_sorting"] = "Save a sequence";
$install_lang["link_sorting_mode"] = "Sorting mode";
$install_lang["link_view_mode"] = "View mode";
$install_lang["no_fields"] = "No fields yet";
$install_lang["no_forms"] = "No forms yet";
$install_lang["no_sections"] = "No sections yet";
$install_lang["note_delete_form"] = "Are you sure you want to delete the form?";
$install_lang["note_delete_section"] = "Are you sure you want to delete the section? All its fields will be deleted too.";
$install_lang["others_languages"] = "Other languages";
$install_lang["select_view_type_checkbox"] = "Checkboxes";
$install_lang["select_view_type_mselect"] = "Multi-select";
$install_lang["select_view_type_radio"] = "Radio buttons";
$install_lang["select_view_type_select"] = "Dropdown";
$install_lang["success_delete_field"] = "Field is deleted";
$install_lang["success_delete_form"] = "Form is deleted";
$install_lang["success_delete_section"] = "Section is deleted";
$install_lang["success_update_form_data"] = "Form is successfully updated";
$install_lang["success_update_option_data"] = "Option is successfully saved";
$install_lang["success_update_section_data"] = "Section is successfully saved";
$install_lang["text_add_form_or"] = "or";
$install_lang["text_add_form_select_another_s"] = "Choose another section";
$install_lang["text_add_form_select_field"] = "Choose a field";
$install_lang["text_add_form_select_section"] = "Choose a section";
$install_lang["text_choose_block"] = "Choose a section";
$install_lang["text_format_none"] = "None";
//$install_lang["text_format_price"] = "Price";
//$install_lang["text_format_year"] = "Year";
//$install_lang["text_format_area"] = "Area"; #modification#
$install_lang["text_template_email"] = "Email";
$install_lang["text_template_floatval"] = "Float";
$install_lang["text_template_intval"] = "Int";
$install_lang["text_template_price"] = "Price";
$install_lang["text_template_string"] = "Literal";
$install_lang["text_template_url"] = "Url";

