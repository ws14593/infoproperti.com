					<ul id="multiselect_options">
					{foreach item=item key=key from=$reference_data.option}
					<li id="option_{$key}">{$item}
						<div class="icons">
							<a href="#" class="edit_link"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_option' gid='field_editor'}" title="{l i='link_edit_option' gid='field_editor'}"></a>
							<a href="#" class="delete_link"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_option' gid='field_editor'}" title="{l i='link_delete_option' gid='field_editor'}"></a>
						</div>
					</li>
					{/foreach}
					</ul>
