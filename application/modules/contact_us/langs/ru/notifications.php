<?php

$install_lang["notification_contact_us_form"] = "Свяжитесь с нами";
$install_lang["tpl_contact_us_form_content"] = "Заполнена форма связи:\n\n\n\n___________________________________\n\n\n\nПричина: [reason];\n\nИмя: [user_name];\n\nEmail: [user_email];\n\nТема: [subject];\n\nСообщение:\n\n[message] \n\n___________________________________";
$install_lang["tpl_contact_us_form_subject"] = "[domain] | Свяжитесь с нами";

