<?php

$install_lang["admin_languages_menu_languages_ds_item"] = "Источники данных";
$install_lang["admin_languages_menu_languages_ds_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_list_item"] = "Языки";
$install_lang["admin_languages_menu_languages_list_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_pages_item"] = "Страницы";
$install_lang["admin_languages_menu_languages_pages_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item"] = "Языки";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item_tooltip"] = "Добавление новых языков, редактирование языков сайта";

