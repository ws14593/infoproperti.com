<?php 
/**
 * CSV import driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Import_csv_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Allowed file extensions
	 * @var string
	 */
	public $allowed_types = "csv|zip";
	
	/**
	 * Import file extension
	 * @var string
	 */
	public $extension = "csv";
	
	/**
	 * Rows limit
	 * @var integer
	 */
	public $rows_per_request = 1;
	
	/**
	 * Two step
	 * @param boolean
	 */
	public $two_step = false;
	
	/**
	 * Driver settings
	 * @var array
	 */
	public $settings = array(
		array("name"=>"use_first_row", "type"=>"checkbox", "default"=>"1"),
		array("name"=>"delimiter", "type"=>"symbol", "default"=>";"),
		array("name"=>"enclosure", "type"=>"symbol", "default"=>"\""),
	);
	
	/**
	 * Constructor
	 * @return Import_csv_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Analyze uploaded file
	 * @param string $filename import file name
	 * @param array $data import data
	 * @param array $settings driver settings
	 */
	public function analyze($filename, $settings){
		$return = array("errors"=>array(), "data"=>array());
				
		$rows = 0;
		$relations = array();
		
		$handle = fopen($filename, "rb");
	
		if(!$settings["delimiter"]) $settings["delimiter"] = ";";
		if(!$settings["enclosure"]) $settings["enclosure"] = "\"";
		
		$first_row = fgetcsv($handle, 0, $settings["delimiter"], $settings["enclosure"]);

		if($settings["use_first_row"]){
			foreach($first_row as $i=>$name){
				if(!$name) continue;
				$relations[$i] = array("name"=>$name, "type"=>"", "link"=>"");
			}
		}else{
			$count = count($first_row);
			for($i=0;$i<$count; $i++){
				$relations[$i] = array("name"=>"", "type"=>"", "link"=>"");
			}
			$rows++;
		}
					
		$return["data"]["position"] = ftell($handle);
				
		$str = "";
		while(!feof($handle)){			
			$str = "";
			while($word=fgets($handle, 4096)){
				if($word) $str = $word;
				if(strpos($str, PHP_EOL) !== false) break;
			}
			if(!$str) continue;
			$rows++;
		}
		
		fclose($handle);
	
		$return["data"]["relations"] = $relations; 
		$return["data"]["total"] = $rows;

		return $return;
	}
	
	/**
	 * Parse uploaded file
	 * @param string $filename import file name
	 * @param array $settings driver settings
	 * @param array $relations field relations
	 * @param integer $position start position
	 */
	public function parse($filename, $settings, $relations, $position, $repeat=false){
		
		$return = array("data"=>array(), "position"=>0, "status"=>1, "processed"=>0);
	
		$handle = fopen($filename, "rb");
		
		if(!$handle) return $return;
		
		fseek($handle, $position);
		
		$row = 0;
		
		while(!feof($handle)){
			if(++$row > $this->rows_per_request){
				$return["status"] = 0;
				break;
			}
			
			$data = fgetcsv($handle, 0, $settings["delimiter"], $settings["enclosure"]);
			if(!empty($data)){		
				$return["data"][] = $data;
				$return["processed"]++;
			}		
		}
		
		$return["position"] = ftell($handle);
		
		fclose($handle);
		
		return $return;
	}
}
