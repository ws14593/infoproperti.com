<?php
/**
* Import admin side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Admin_Import extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Admin_Import
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
		$this->Menu_model->set_menu_active_item("admin_menu", "exp-import-items");
		$this->system_messages->set_data("header", l("admin_header_import", "import"));
	}
	
	/**
	 * Render index action
	 * @param string $order sorting field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function index(){
		$this->data();
	}
	
	/**
	 * Render data list action
	 * @param string $order sorting field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function data($order="date_created", $order_direction="DESC", $page=1){
		$this->load->model("Import_model");
		$this->load->model("import/models/Import_driver_model");
		$this->load->model("import/models/Import_module_model");
		
		$filters = array();
		
		$current_settings = isset($_SESSION["import_list"]) ? $_SESSION["import_list"] : array();
				
		$current_settings["filters"] = $filters;
				
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if (!isset($current_settings["page"]))
			$current_settings["page"] = 1;		
		
		if (!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$data_count = $this->Import_model->get_data_count($filters);

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $data_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["import_list"] = $current_settings;

		$sort_links = array(
			"date_created" => site_url() . "admin/import/index/date_created/".(($order != "date_created" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);		
		$this->template_lite->assign("sort_links", $sort_links);

		if($data_count > 0){
			$data = $this->Import_model->get_data_list($filters, $page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign("data", $data);
		}
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url()."admin/import/index/".$driver_gid."/".$module_id."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $data_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->template_lite->assign("module_id", $module_id);
		
		$this->Menu_model->set_menu_active_item("admin_import_menu", "import_data_item");
		$this->template_lite->view("list");
	}
	
	/**
	 * Upload data for importing
	 */
	public function upload(){
		$this->load->model("Import_model");
		$this->load->model("import/models/Import_driver_model");
	
		$data = array();
	
		if($this->input->post("btn_save")){
			
			$data = $this->input->post("data");
			
			$validate_data = $this->Import_model->validate_data(null, $data);			
			$validate_upload_data = $this->Import_model->validate_upload("import_file", $data);
			$validate_data["errors"] = array_merge($validate_data["errors"], $validate_upload_data["errors"]);
		
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$data_id = $this->Import_model->save_data(null, $validate_data["data"]);				
				$upload_data = $this->Import_model->upload("import_file", $data_id);				
				if(!empty($upload_data["errors"])){
					$validate_data = $upload_data;
				}else{
					$validate_data = $this->Import_model->validate_data($data_id, $upload_data["data"]);
				}
			
				if(!empty($validate_data["errors"])){
					$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
					$this->Import_model->delete_file($data_id);
					$this->Import_model->delete_data($data_id);					
				}else{
					$this->Import_model->save_data($data_id, $validate_data["data"]);
					$this->system_messages->add_message("success", l("success_file_uploaded", "import"));
					$url = site_url()."admin/import/relations/".$data_id;
					redirect($url);
				}				
			}
		}
		
		$this->template_lite->assign('data', $data);
		
		$drivers = $this->Import_driver_model->get_drivers();
		foreach($drivers as $key=>$driver){
			if(!$driver["status"]) unset($drivers[$key]);
		}
		$this->template_lite->assign("drivers", $drivers);	
		
		$this->load->model("import/models/Import_module_model");
		$modules = $this->Import_module_model->get_modules();
		$this->template_lite->assign("modules", $modules);	
		
		if(!empty($drivers)){
			$current_driver = current($drivers);
			if(isset($data['gid_driver'])){
				foreach($drivers as $driver){
					if($driver['gid'] = $data['gid_driver']){
						$current_driver = $driver;
						break;
					}
				}
			}
			$model_name = "import_".$current_driver["gid"]."_model";
			$this->load->model("import/models/drivers/".$model_name, $model_name);		
			$this->template_lite->assign("driver_gid", $current_driver["gid"]);
			$this->template_lite->assign("settings", $this->{$model_name}->settings);	
			$settings =  $this->template_lite->fetch("upload_form_settings", "admin", "import");
			$this->template_lite->assign("settings", $settings);
		}				
		$this->template_lite->view("upload_form");
	}
	
	/**
	 * Upload data for importing by ajax
	 * @param string $section_gid section guid
	 */
	public function ajax_upload_form(){
		$driver_gid = $this->input->post("driver_gid", true);
		$model_name = "import_".$driver_gid."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);		
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->template_lite->assign("settings", $this->{$model_name}->settings);
		echo $this->template_lite->fetch("upload_form_settings", "admin", "import");
		exit;
	}
	
	/**
	 * Edit data action
	 * @param integer $data_id data identifier
	 */
	public function edit($data_id){
		
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");

		$data = $this->Import_model->get_data_by_id($data_id, true);
		foreach($data["relations"] as $index=>$relation){
			if(!$relation["link"]) unset($data["relations"][$index]);
		}
		$this->template_lite->assign("data", $data);

		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		$this->template_lite->assign("two_step", $this->{$model_name}->two_step);
				
		/*if(!$this->{$model_name}->two_step){
			if($data["total"] > $data["processed"]) $this->parse($data_id);
			$data["processed"] = $data["total"];
		}*/
		
		$this->template_lite->assign("relations", count($data["relations"]));
		$this->template_lite->assign("processed", $data["total"]-$data["processed"]);
		$this->template_lite->assign("imported", !$this->{$model_name}->two_step ? $data["total"]-($data['imported'] + $data['failed']) : $data["processed"]);
		
		$this->system_messages->set_data("header", l("admin_header_data_edit", "import"));
		$this->template_lite->view("edit");
	}
	
	/**
	 * Set fields relations
	 * @param integer $import_id
	 */
	public function relations($data_id){
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		
		if($this->input->post("btn_save")){
			$module_fields = $this->Import_module_model->get_module_fields($data["id_object"]);
			$links = $this->input->post("link", true);
			foreach($data["relations"] as $index => $relation){
				if(isset($links[$index]) && $links[$index] != -1 && !empty($module_fields[$links[$index]]["name"])){
					$data["relations"][$index]["type"] = $module_fields[$links[$index]]["type"];
					$data["relations"][$index]["link"] = $module_fields[$links[$index]]["name"];
					$data["relations"][$index]["label"] = $module_fields[$links[$index]]["label"];
				}else{
					$data["relations"][$index]["type"] = "";
					$data["relations"][$index]["link"] = "";
					$data["relations"][$index]["label"] = "";
				}
			}
			$save_data["relations"] = $data["relations"];
			$validate_data = $this->Import_model->validate_data($data_id, $save_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$this->Import_model->save_data($data_id, $validate_data["data"]);
				$this->system_messages->add_message("success", l("success_relation_updated", "import"));
				$url = site_url()."admin/import/edit/".$data_id;
				redirect($url);			
			}
		}
		
		$this->template_lite->assign("data", $data);
		
		$module_fields = $this->Import_module_model->get_module_fields($data["id_object"]);
		$this->template_lite->assign("module_fields", $module_fields);
		
		$this->system_messages->set_data("header", l("admin_header_relations", "import"));
		$this->template_lite->view("relations");
	}
	
	/**
	 * Remove data action
	 * @param integer $data_id data identifier
	 */
	public function delete($ids=null){
		if(!$ids) $ids = $this->input->post("ids");
		if(!empty($ids)){
			$this->load->model("Import_model");
			foreach((array)$ids as $id){
				$this->Import_model->delete_file($id, true);
				$this->Import_model->delete_data($id);
			}
			$this->system_messages->add_message("success", l("success_data_deleted", "import"));
		}
		$url = site_url()."admin/import";
		redirect($url);
	}
	
	/**
	 * Parse imported data action
	 * @param integer $data_id data identifier
	 */
	public function parse($data_id){
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		
		try{
			$result = $this->Import_model->parse_data($data_id);
		}catch(Exception $e){
			$result['errors'][] = $e->getMessage();
		}
		
		if($this->{$model_name}->two_step){
			if($result){
				$this->system_messages->add_message("success", l("success_data_parsed", "import"));			
			}
			$url = site_url()."admin/import/edit/".$data_id;
			redirect($url);
		}
		
		return $result;
	}
	
	/**
	 * Parse imported data action by ajax
	 * @param integer $data_id data identifier
	 */
	public function ajax_parse($data_id){
		$response = array("errors"=>array(), "processed"=>0);
		
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		
		try{
			$result = $this->Import_model->parse_data($data_id);
		}catch(Exception $e){
			$result["errors"][] = $e->getMessage();
		}
	
		
		if(!empty($result["errors"])){
			$response["errors"] = implode("<br>", $result["errors"]);
		}else{
			$response["processed"] = $results["data"]["processed"];
		}
		echo json_encode($response);
		exit;
	}
	
	/**
	 * Parse imported data action
	 * @param integer $data_id data identifier
	 */
	public function make($data_id){
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);		
		
		if(!$this->{$model_name}->two_step && $data["total"]-$data["processed"]){
			$results = $this->parse($data_id);
			if(!empty($results['errors'])){
				$this->system_messages->add_message("error", implode("<br>", $results["errors"]));
			}
		}
		
		$results = $this->Import_model->make_import_data($data_id);
		if(!empty($results["errors"])){
			$this->system_messages->add_message("error", implode("<br>", $results["errors"]));
		}else{
			if($results["data"]["failed"]){
				$this->system_messages->add_message("error",
					l("error_data_imported", "import").": ".$results["data"]["imported"]."<br>".
					l("error_data_failed", "import").": ".$results["data"]["failed"]
				);
			}else{
				$this->system_messages->add_message("success", l("success_data_imported", "import").": ".$results["data"]["imported"]);
			}
		}	
				
		$url = site_url()."admin/import/edit/".$data_id;	
		redirect($url);
	}
	
	/**
	 * Parse imported data action by ajax
	 * @param integer $import_id data identifier
	 */
	public function ajax_make($import_id){
		$this->load->model("Import_model");
		
		$response = array("errors"=>"", "imported"=>0);
		
		$import_data = $this->Import_model->get_data_by_id($import_id, true);
		if(!$import_data){
			$response["error"] = l("error_empty_data", "import");
			echo json_encode($response);
			exit;
		}
		
		$model_name = "Import_".$import_data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);		
		
		if(!$this->{$model_name}->two_step && $import_data["total"] > ($import_data["processed"] + $import_data["imported"] + $import_data["failed"])){
			$results = $this->parse($import_id);
			if(!empty($results['errors'])){
				$response["error"] = implode('<br>', $results['errors']);
				echo json_encode($response);
				exit;
			}
			$import_data["processed"] = $results["data"]["processed"];
		}
		
		$results = $this->Import_model->make_import_data($import_id);
		if(!empty($results['errors'])){
			$response['error'] = implode('<br>', $results['errors']);
		}else{
			$response["imported"] = $results["data"]["imported"];
			$response["failed"] = $results["data"]["failed"];
			$response["total"] = $import_data["total"] - ($results["data"]["imported"] + $results["data"]["failed"]);
			if(!$response["continue"]){
				if($response["failed"]){
					$response["error"] =
						l("error_data_imported", "import").": ".$results["data"]["imported"]."<br>".
						l("error_data_failed", "import").": ".$results["data"]["failed"];
				}else{
					$response["success"] = l("success_data_imported", "import").": ".$results["data"]["imported"];
				}
			}
		}
		echo json_encode($response);
		exit;
	}
	
	/**
	 * Render driver list action
	 */
	public function drivers(){
		$this->load->model("import/models/Import_driver_model");
		$drivers = $this->Import_driver_model->get_drivers();
		$this->template_lite->assign("drivers", $drivers);
		$this->Menu_model->set_menu_active_item("admin_import_menu", "import_drivers_items");
		$this->template_lite->view("drivers_list");
	}

	/**
	 * Activate driver
	 * @param string $dirver_gid driver guid
	 * @param integer $status
	 */
	public function activate_driver($driver_gid, $status=1){
		$this->load->model("import/models/Import_driver_model");
		if($status){
			$this->Import_driver_model->activate_driver($driver_gid);
		}else{
			$this->Import_driver_model->deactivate_driver($driver_gid);
		}		
		redirect(site_url()."admin/import/drivers");
	}
}
