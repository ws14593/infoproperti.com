{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_import_menu'}
<div class="actions">
	<ul>
		<li id="add"><div class="l"><a href="{$site_url}admin/import/upload/">{l i='btn_add' gid='start'}</a></div></li>
	</ul>
	&nbsp;
</div>

<form id="import_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w100">{l i='field_data_driver' gid='import'}</th>
		<th class="w100">{l i='field_data_module' gid='import'}</th>
		<th class="w50">{l i='field_data_total' gid='import'}</th>
		<th class="w50"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_data_date_created' gid='import'}</a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	{foreach item=item from=$data}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>				
			<td>{$item.driver.output_name|truncate:50}</td>
			<td>{$item.module.output_name|truncate:50}</td>
			<td class="center">{$item.total}</td>
			<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
			<td class="icons">
				<a href="{$site_url}admin/import/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_data' gid='import' type='button'}" title="{l i='link_edit_data' gid='import' type='button'}"></a>
				<a href="{$site_url}admin/import/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_data' gid='import' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_data' gid='import' type='button'}" title="{l i='link_delete_data' gid='import' type='button'}"></a>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="5" class="first center">{l i='no_data' gid='import'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}

<script>{literal}
	var reload_link = "{/literal}{$site_url}admin/import/index{literal}";
	var filter = '{/literal}{$filter}{literal}';
	var order = '{/literal}{$order}{literal}';
	var loading_content;
	var order_direction = '{/literal}{$order_direction}{literal}';
	function reload_this_page(value){
		var link = reload_link + filter + '/' + value + '/' + order + '/' + order_direction;
		location.href=link;
	}
{/literal}</script>

{include file="footer.tpl"}
